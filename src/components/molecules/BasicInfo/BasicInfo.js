import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { Core, Icons, Styles, Lab } from "@ebs/styleguide";
import { useSelector, useDispatch, useStore } from "react-redux";
import Countries from "utils/other/CountryService";
import RecipientTypeList from "utils/other/RecipientTypeService";
import ShipmentStatusList from "utils/other/ShipmentStatusService";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { useForm, Controller } from "react-hook-form";
import { createShipment, updateShipment } from "store/modules/ShipmentsReducer";
import { useHistory } from "react-router-dom";
const {
  Box,
  Grid,
  Paper,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  FormLabel,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Radio,
  RadioGroup,
  Button,
} = Core;
const { useTheme } = Styles;

const { Autocomplete } = Lab;
import { set } from "lodash";



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const BasicInfoMolecule = React.forwardRef(
  ({ refresh, type, programList }, ref) => {
    const theme = useTheme();
    const history = useHistory();
    const classes = {
      root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(4),
        textAlign: "left",
        color: theme.palette.text.secondary,
      },
      container: {
        padding: theme.spacing(2),
      },
    };
    const requestorNameInputValue = useRef();
    const [shipmentTypeRadio, setShipmentTypeRadio] = useState("within CG");
    const [materialTypeRadio, setMaterialTypeRadio] = useState("propagative");
    const [countryList, setCountryList] = useState([]);
    const [recipientTypeList, setRecipientTypeList] = useState([]);
    const [shipmentStatusList, setShipmentStatusList] = useState([]);
    const [allowEdit, setAllowEdit] = useState(false);
    const [selectedRequestorCountry, setSelectedRequestorCountry] =
      useState("");
    const [selectedProgram, setSelectedProgram] = useState("");

    const [requestorNameValue, setRequestorNameValue] = useState("");
    const [requestorEmailValue, setRequestorEmailValue] = useState("");
    const [requestorAddressValue, setRequestorAddressValue] = useState("");
    const [requestorInstitutionValue, setRequestorInstitutionValue] =
      useState("");
    const [requestorCountryValue, setRequestorCountryValue] = useState("");

    const dispatch = useDispatch();
    const { getState } = useStore();
    const {
      shipmentDbId,
      mode,
      shipment,
      loading,
      isFormEditable,
      shipmentProgram,
    } = useSelector(({ shipmentsReducer, isFormEditable }) => shipmentsReducer);
    const { person, personProgram, isSHU, personProgramList } = useSelector(
      ({ personReducer }) => personReducer
    );
    const [currentSelectedProgram, setCurrentSelectedProgram] = useState([
      { id: "", label: "" },
    ]);

    //form-control
    const {
      control,
      register,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm({
      mode: "onChange",
      defaultValues: {
        shipmentStatus: "",
        shuReferenceNumber: shipment?.shuReferenceNumber,
        receivedDate: "",
        shipmentRemarks: "",
        authorizedSignatory: "",
        documentSignedDate: "",
        documentGeneratedDate: "",
        airwayBillNumber: "",
        shippedDate: "",
        dispatchRemarks: "",
        trackingRemarks: "",
        acknowledgmentRemarks: "",
        programId: "",
        shipmentProgram: shipmentProgram,
        shipmentName: "",
        shipmentPurpose: "",
        senderName: person.personName,
        senderEmail: person.email,
        senderInstitution: "The International Rice Research Institute (IRRI)",
        senderAddress: `University of the Philippines Campus at Los Baños Municipality of Los Baños, Province of Laguna, Republic of the Philippines`,
        senderCountry: getCountryElement("Philippines"),
        requestorName: "",
        requestorEmail: "",
        requestorAddress: "",
        requestorCountry: "",
        requestorInstitution: "",
        recipientName: "",
        recipientEmail: "",
        recipientAddress: "",
        recipientCountry: "",
        recipientInstitution: "",
        recipientType: "",
        shipmentTypeName: "",
        materialTypeRadioName: "",
        remarks: "",
        shipmentType: "",
        materialType: "",
      },
    });

    useEffect(() => {
      setCountryList(Countries.getCountries());
      setRecipientTypeList(RecipientTypeList.getRecipientType());
      setShipmentStatusList(ShipmentStatusList.getShipmentStatus());
      setValue("shipmentName", shipment?.shipmentName);
      setValue("shipmentCode", shipment?.shipmentCode);
      setValue("shipmentPurpose", shipment?.shipmentPurpose);
      setValue("shipmentType", shipment?.shipmentType);
      setValue("materialType", shipment?.materialType);
      setValue(
        "shipmentStatus",
        getShipmentStatusElement(shipment?.shipmentStatus)
      );

      setValue("requestorName", shipment?.requestorName);
      setValue("requestorEmail", shipment?.requestorEmail);
      setValue("requestorInstitution", shipment?.requestorInstitution);
      setValue("requestorAddress", shipment?.requestorAddress);

      setValue("senderName", shipment?.senderName);
      setValue("senderEmail", shipment?.senderEmail);
      setValue("senderInstitution", shipment?.senderInstitution);
      setValue("senderAddress", shipment?.senderAddress);
      setValue("senderCountry", getCountryElement(shipment?.senderCountry));

      setValue("recipientName", shipment?.recipientName);
      setValue("recipientEmail", shipment?.recipientEmail);
      setValue("recipientInstitution", shipment?.recipientInstitution);
      setValue("recipientAddress", shipment?.recipientAddress);
      setValue(
        "recipientCountry",
        getCountryElement(shipment?.recipientCountry)
      );
      setValue(
        "recipientType",
        getRecipientTypeElement(shipment?.recipientType)
      );

      //SHU Form setvalues
      setValue("shuReferenceNumber", shipment?.shuReferenceNumber);
      setValue(
        "requestorCountry",
        getCountryElement(shipment?.requestorCountry)
      );
      setValue("shipmentRemarks", shipment?.shipmentRemarks);
      setValue("documentSignedDate", shipment?.documentSignedDate);
      setValue("documentGeneratedDate", shipment?.documentGeneratedDate);

      setValue("trackingRemarks", shipment?.trackingRemarks);
      setValue("authorizedSignatory", shipment?.authorizedSignatory);
      setValue("shippedDate", shipment?.shippedDate);
      setValue("airwayBillNumber", shipment?.airwayBillNumber);
      setValue("dispatchRemarks", shipment?.dispatchRemarks);
      setValue("acknowledgmentRemarks", shipment?.acknowledgmentRemarks);
      setValue("remarks", shipment?.remarks);

      if (shipment?.shipmentType === "external to CG") {
        setShipmentTypeRadio("external to CG");
      } else {
        setShipmentTypeRadio("within CG");
      }

      if (shipment?.materialType === "non-propagative") {
        setMaterialTypeRadio("non-propagative");
      } else {
        setMaterialTypeRadio("propagative");
      }

      setValue("programId", getPersonProgramElement(shipment?.programId));
      setValue("shipmentProgram", shipmentProgram);
    }, [setValue, shipment, refresh]);

    useEffect(() => {
      if (isFormEditable) {
        setAllowEdit("visible");
      } else {
        setAllowEdit(false);
      }
    }, [isFormEditable, allowEdit]);

    useEffect(() => {
      if (mode === "put" && shipment) {
        setValue("shipmentName", shipment?.shipmentName);
        setValue("shipmentCode", shipment?.shipmentCode);
        setValue("shipmentPurpose", shipment?.shipmentPurpose);
        setValue("shipmentType", shipment?.shipmentType);
        setValue("materialType", shipment?.materialType);
        setValue(
          "shipmentStatus",
          getShipmentStatusElement(shipment?.shipmentStatus)
        );

        setValue("requestorName", shipment?.requestorName);
        setValue("requestorEmail", shipment?.requestorEmail);
        setValue("requestorInstitution", shipment?.requestorInstitution);
        setValue("requestorAddress", shipment?.requestorAddress);

        setValue("senderName", shipment?.senderName);
        setValue("senderEmail", shipment?.senderEmail);
        setValue("senderInstitution", shipment?.senderInstitution);
        setValue("senderAddress", shipment?.senderAddress);
        setValue("senderCountry", getCountryElement(shipment?.senderCountry));

        setValue("recipientName", shipment?.recipientName);
        setValue("recipientEmail", shipment?.recipientEmail);
        setValue("recipientInstitution", shipment?.recipientInstitution);
        setValue("recipientAddress", shipment?.recipientAddress);
        setValue(
          "recipientCountry",
          getCountryElement(shipment?.recipientCountry)
        );
        setValue(
          "recipientType",
          getRecipientTypeElement(shipment?.recipientType)
        );

        //SHU Form setvalues
        setValue("shuReferenceNumber", shipment?.shuReferenceNumber);
        setValue(
          "requestorCountry",
          getCountryElement(shipment?.requestorCountry)
        );
        setValue("shipmentRemarks", shipment?.shipmentRemarks);
        setValue("documentSignedDate", shipment?.documentSignedDate);
        setValue("receivedDate", shipment?.receivedDate);
        setValue("documentGeneratedDate", shipment?.documentGeneratedDate);

        setValue("trackingRemarks", shipment?.trackingRemarks);
        setValue("authorizedSignatory", shipment?.authorizedSignatory);
        setValue("shippedDate", shipment?.shippedDate);
        setValue("airwayBillNumber", shipment?.airwayBillNumber);
        setValue("dispatchRemarks", shipment?.dispatchRemarks);
        setValue("acknowledgmentRemarks", shipment?.acknowledgmentRemarks);
        setValue("remarks", shipment?.remarks);

        if (shipment?.shipmentType === "within CG") {
          setShipmentTypeRadio("within CG");
        } else {
          setShipmentTypeRadio("external to CG");
        }

        if (shipment?.materialType === "propagative") {
          setMaterialTypeRadio("propagative");
        } else {
          setMaterialTypeRadio("non-propagative");
        }
        setValue("programId", getPersonProgramElement(shipment?.programId));
        setValue("shipmentProgram", shipmentProgram);
      }
    }, [shipment, refresh]);

    const onSubmit = (data, event) => {
      event.preventDefault();

      if (data.senderName == undefined) {
        data.senderName = person.personName;
      }

      if (data.senderAddress == undefined) {
        data.senderAddress =
          "University of the Philippines Campus at Los Baños Municipality of Los Baños, Province of Laguna, Republic of the Philippines";
      }

      if (data.senderEmail == undefined) {
        data.senderEmail = person.email;
      }
      if (data.senderCountry == undefined) {
        data.senderCountry = getCountryElement("Philippines");
      }
      if (data.senderInstitution == undefined) {
        data.senderInstitution = "The International Rice Research Institute (IRRI)";
      }

      data.requestorCountry = data.requestorCountry?.label;
      data.senderCountry = data.senderCountry?.label;
      data.recipientCountry = data.recipientCountry?.label;
      data.recipientType = data.recipientType?.label;
      data.shipmentType = shipmentTypeRadio;
      data.materialType = materialTypeRadio;

      if (
        isSHU ||
        data.shipmentStatus != null ||
        data.shipmentStatus != undefined
      ) {
        data.shipmentStatus = data.shipmentStatus.label;
      }

      if (data.airwayBillNumber == undefined && data.shipmentStatus == "Done") {
        data.shipmentStatus = shipment?.shipmentStatus;
      }

      if (!isSHU) {
        data.programDbId = data.programId["id"].toString();
      }

      (mode === "post" &&
        createShipment({
          programDbId: data.programDbId,
          shipmentName: data.shipmentName,
          shuReferenceNumber: data.shuReferenceNumber,
          shipmentPurpose: data.shipmentPurpose,
          shipmentCode: data.shipmentCode,
          shipmentType: data.shipmentType,
          materialType: data.materialType,
          requestorName: data.requestorName,
          requestorEmail: data.requestorEmail,
          requestorAddress: data.requestorAddress,
          requestorInstitution: data.requestorInstitution,
          requestorCountry: data.requestorCountry,
          senderName: data.senderName,
          senderEmail: data.senderEmail,
          senderAddress: data.senderAddress,
          senderInstitution: data.senderInstitution,
          senderCountry: data.senderCountry,
          recipientName: data.recipientName,
          recipientEmail: data.recipientEmail,
          recipientAddress: data.recipientAddress,
          recipientInstitution: data.recipientInstitution,
          recipientCountry: data.recipientCountry,
          recipientType: data.recipientType,
          creatorId: person.personDbId,
          modifierId: person.personDbId,
          remarks: data.remarks,
        })(dispatch, getState)) ||
        updateShipment(data, shipmentDbId)(dispatch, getState);
    };

    function handleClose() {
      // setOpen(false);
      history.push("shipment");
    }

    function handleClickShipmentType(event) {
      if (event.target.value === "within CG") {
        setShipmentTypeRadio("within CG");
      } else {
        setShipmentTypeRadio("external to CG");
      }
    }

    function handleClickMaterialType(event) {
      if (event.target.value === "propagative") {
        setMaterialTypeRadio("propagative");
      } else {
        setMaterialTypeRadio("non-propagative");
      }
    }

    function getCountryElement(label) {
      let countryObj = Countries.getCountries().find((c) => c.label === label);
      return countryObj;
    }

    function getRecipientTypeElement(label) {
      let recipientTypeObj = RecipientTypeList.getRecipientType().find(
        (r) => r.label === label
      );
      return recipientTypeObj;
    }
    function getShipmentStatusElement(label) {
      let shipmentStatusObj = ShipmentStatusList.getShipmentStatus().find(
        (s) => s.label === label
      );
      return shipmentStatusObj;
    }

    function getPersonProgramElement(id) {
      try {
        let personProgramObj = null;
        if (id == null) {
          id = personProgram["programDbId"];
        }

        if (Object.prototype.toString.call(id).indexOf("Object") > -1) {
          id = id["id"];
        }

        personProgramObj = personProgramList.find((item) => item.id === id);
        setCurrentSelectedProgram(personProgramObj);

        return personProgramObj;
      } catch (error) {}
    }

    useEffect(() => {
      if (refresh) {
        window.onbeforeunload = function () {
          return true;
        };
      }

      return () => {
        window.onbeforeunload = null;
      };
    }, [refresh]);
    // useEffect(() => {
    //   window.addEventListener("beforeunload", alertUser);
    //   return () => {
    //     console.log("hello");
    //     window.removeEventListener("beforeunload", alertUser);
    //   };
    // }, []);
    // const alertUser = (e) => {
    //   e.preventDefault();
    //   e.returnValue = "";
    // };

    function handleChangeSetRecipient(event) {
      event.preventDefault();
      if (event.target.checked == true) {
        setValue("recipientName", requestorNameValue);
        setValue("recipientEmail", requestorEmailValue);
        setValue("recipientAddress", requestorAddressValue);
        setValue("recipientInstitution", requestorInstitutionValue);
        setValue("recipientCountry", getCountryElement(requestorCountryValue));
      } else {
      }
    }

    return (
      /* 
     @prop data-testid: Id to use inside BasicInfo.test.js file.
     */
      <div className={classes.root}>
        <form onSubmit={handleSubmit(onSubmit)} data-testid="basicInfoForm">
          {isFormEditable && isSHU && (
            <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-end"
            >
              <Grid item spacing={2}>
                <Button
                  onClick={handleClose}
                  // onClick={() => window.location.reload()}
                  variant="contained"
                  color="secondary"
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white btn-cancel"
                >
                  <FormattedMessage id="none" defaultMessage="Cancel" />
                </Button>
              </Grid>

              <Grid item spacing={2}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white btn-success"
                >
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </Grid>
            </Grid>
          )}

          {!isFormEditable && (
            <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-end"
            >
              <Grid item spacing={2}>
                <Button
                  onClick={handleClose}
                  // onClick={() => window.location.reload()}
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                  variant="contained"
                  color="secondary"
                >
                  <FormattedMessage id="none" defaultMessage="Back" />
                </Button>
              </Grid>
            </Grid>
          )}

          {/* {shipment?.shipmentName} */}
          {/* SHU Form Input - START*/}
          {isSHU && (
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid container spacing={2}>
                    <Grid item xs={2}>
                      <Controller
                        control={control}
                        name="shipmentStatus"
                        render={({ field: { onChange, value } }) => (
                          <Autocomplete
                            onChange={(event, options) => {
                              setValue("shipmentStatus", options.label);
                              onChange(options);
                            }}
                            value={value}
                            options={shipmentStatusList}
                            classes={{
                              option: classes.option,
                            }}
                            autoHighlight
                            getOptionLabel={(item) =>
                              item.label ? item.label : ""
                            }
                            getOptionSelected={(option, value) =>
                              value === undefined ||
                              value === "" ||
                              option.label === value.label
                            }
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                label="Shipment Status"
                                variant="outlined"
                                error={!!errors.item}
                                helperText={errors.item && "item required"}
                                required
                              />
                            )}
                          />
                        )}
                      />
                    </Grid>

                    <Grid item xs={2}>
                      <Controller
                        name="receivedDate"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            type="date"
                            fullWidth="true"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Received Date"
                              />
                            }
                            variant="outlined"
                            data-testid={"receivedDate"}
                          />
                        )}
                        defaultValue={shipment?.receivedDate}
                      />
                    </Grid>

                    <Grid item xs={6}>
                      <Controller
                        name="shipmentRemarks"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            multiline
                            row={3}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Shipment Remarks"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            data-testid={"shipmentRemarks"}
                          />
                        )}
                        defaultValue={shipment?.shipmentRemarks}
                      />
                    </Grid>
                  </Grid>

                  <Grid container spacing={2}>
                    <Grid item xs={4}>
                      <Controller
                        name="authorizedSignatory"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Authorized Signatory"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            data-testid={"authorizedSignatory"}
                          />
                        )}
                        defaultValue={shipment?.authorizedSignatory}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Controller
                        name="documentSignedDate"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            type="date"
                            fullWidth="true"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Document Signed Date"
                              />
                            }
                            variant="outlined"
                            data-testid={"documentSignedDate"}
                          />
                        )}
                        defaultValue={shipment?.documentSignedDate}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Controller
                        name="documentGeneratedDate"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            type="date"
                            fullWidth="true"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Document Generated Date"
                              />
                            }
                            variant="outlined"
                            data-testid={"documentGeneratedDate"}
                          />
                        )}
                        defaultValue={shipment?.documentGeneratedDate}
                      />
                    </Grid>
                  </Grid>

                  <Grid container spacing={2}>
                    <Grid item xs={2}>
                      <Controller
                        name="airwayBillNumber"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Airbill Number"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            data-testid={"airwayBillNumber"}
                          />
                        )}
                        defaultValue={shipment?.airwayBillNumber}
                      />
                    </Grid>
                    <Grid item xs={2}>
                      <Controller
                        name="shippedDate"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            type="date"
                            fullWidth="true"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Shipped Date"
                              />
                            }
                            variant="outlined"
                            data-testid={"shippedDate"}
                          />
                        )}
                        defaultValue={shipment?.shippedDate}
                      />
                    </Grid>
                    <Grid item xs={8}>
                      <Controller
                        name="dispatchRemarks"
                        control={control}
                        // ref={register}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            multiline
                            rows={2}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Dispatch Remarks"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            data-testid={"dispatchRemarks"}
                          />
                        )}
                        defaultValue={shipment?.dispatchRemarks}
                      />
                    </Grid>
                  </Grid>

                  <Grid container spacing={2}>
                    <Grid item xs={4}>
                      <Controller
                        name="trackingRemarks"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            multiline
                            rows={4}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Tracking Remarks"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            data-testid={"trackingRemarks"}
                          />
                        )}
                        defaultValue={shipment?.trackingRemarks}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      <Controller
                        name="acknowledgmentRemarks"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Acknowledgement Remarks"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            multiline
                            rows={4}
                            data-testid={"acknowledgmentRemarks"}
                          />
                        )}
                        defaultValue={shipment?.acknowledgmentRemarks}
                      />
                    </Grid>
                    <Grid item xs={4}>
                      {/* <Controller
                name="dispatchRemarks_"
                control={control}
                // ref={register}
                render={({ field }) => (
                  <TextField
                    {...field}
                    // required
                   fullWidth="true"
                    label={
                      <FormattedMessage
                        id="none"
                        defaultMessage="Dispatch Remarks"
                      />
                    }
                    variant="outlined"
                    multiline
                    rows={4}
                    data-testid={"dispatchRemarks"}
                  />
                )}
                defaultValue={shipment?.dispatchRemarks}
              /> */}
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          )}
          {/* SHU Form Input - END*/}
          <Grid container spacing={1}>
            {/* Client Form Input -START*/}

            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <Grid container direction="row" spacing={2}>
                  {isFormEditable && !isSHU && (
                    <Grid item xs={2}>
                      <Controller
                        control={control}
                        name="programId"
                        render={({ field: { onChange, value } }) => (
                          <Autocomplete
                            onChange={(event, options) => {
                              setValue("programId", options.label);
                              onChange(options);
                            }}
                            value={value}
                            options={personProgramList}
                            classes={{
                              option: classes.option,
                            }}
                            autoHighlight
                            getOptionLabel={(item) =>
                              item.label ? item.label : ""
                            }
                            getOptionSelected={(option, value) =>
                              value === undefined ||
                              value === "" ||
                              option.label === value.label
                            }
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                label="Program"
                                variant="outlined"
                                error={!!errors.item}
                                size="normal"
                                fullWidth
                                helperText={errors.item && "item required"}
                              />
                            )}
                          />
                        )}
                      />
                    </Grid>
                  )}
                  {!isFormEditable && (
                    <Grid item xs={2}>
                      <Controller
                        name="shipmentProgram"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            value={shipmentProgram}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Program"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"shipmentProgram"}
                          />
                        )}
                        defaultValue={shipment?.programId}
                      />
                    </Grid>
                  )}
                  {isFormEditable && isSHU && (
                    <Grid item xs={2}>
                      <Controller
                        name="shipmentProgram"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            value={shipmentProgram}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Program"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"shipmentProgram"}
                          />
                        )}
                        defaultValue={shipment?.programId}
                      />
                    </Grid>
                  )}

                  <Grid item xs={2}>
                    <Controller
                      name="shipmentName"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          fullWidth="true"
                          required
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Shipment Name"
                            />
                          }
                          variant="outlined"
                          InputLabelProps={{
                            shrink: true,
                          }}
                          InputProps={{
                            readOnly: !isFormEditable,
                          }}
                          data-testid={"shipmentName"}
                        />
                      )}
                      defaultValue={shipment?.shipmentName}
                    />
                  </Grid>
                  <Grid item xs={2}>
                    <Controller
                      name="shuReferenceNumber"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          fullWidth="true"
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="SHU Reference Number"
                            />
                          }
                          variant="outlined"
                          InputLabelProps={{
                            shrink: true,
                          }}
                          data-testid={"shuReferenceNumber"}
                        />
                      )}
                      defaultValue={
                        shipment?.shuReferenceNumber
                          ? shipment?.shuReferenceNumber
                          : ""
                      }
                    />
                  </Grid>
                  <Grid item xs={2}>
                    <Controller
                      name="shipmentPurpose"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          fullWidth="true"
                          multiline
                          row={3}
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Shipment Purpose"
                            />
                          }
                          variant="outlined"
                          InputLabelProps={{
                            shrink: true,
                          }}
                          InputProps={{
                            readOnly: !isFormEditable,
                          }}
                          data-testid={"shipmentPurpose"}
                        />
                      )}
                      defaultValue={shipment?.shipmentPurpose}
                    />
                  </Grid>
                  <Grid item xs={2}>
                    {/* <Controller
                    name="shipmentTransactionType"
                    control={control}
                    // ref={register}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        // required
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="Transaction Type"
                          />
                        }
                        data-testid={"shipmentTransactionType"}
                      />
                    )}
                    defaultValue={shipment?.shipmentTransactionType}
                  /> */}
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Shipment Type</FormLabel>
                      <RadioGroup
                        aria-label="shipmentTypeLabel"
                        name="shipmentTypeName"
                        value={shipmentTypeRadio}
                      >
                        <FormControlLabel
                          value="within CG"
                          control={<Radio onClick={handleClickShipmentType} />}
                          label="Within CG"
                        />
                        <FormControlLabel
                          value="external to CG"
                          control={<Radio onClick={handleClickShipmentType} />}
                          label="External to CG"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item xs={2}>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Material Type</FormLabel>
                      <RadioGroup
                        aria-label="materialTypeLabel"
                        name="materialTypeRadioName"
                        value={materialTypeRadio}
                        width="100"
                      >
                        <FormControlLabel
                          value="propagative"
                          control={<Radio onClick={handleClickMaterialType} />}
                          label="Propagative"
                        />
                        <FormControlLabel
                          value="non-propagative"
                          control={<Radio onClick={handleClickMaterialType} />}
                          label="Non-propagative"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>

            {/* Requestor,Sender and Recipient */}

            <Grid item xs={12} spacing={3}>
              <Paper className={classes.paper}>
                <Grid container direction="row" spacing={2}>
                  {/* Sender */}
                  <Grid container item xs={4} direction="row" spacing={1}>
                    <Grid item xs={12} spacing={1}>
                      <Typography variant="h8" color="primary">
                        <FormattedMessage id="none" defaultMessage="Sender" />
                      </Typography>
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="senderName"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Sender Name"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"senderName"}
                          />
                        )}
                        defaultValue={shipment?.senderName}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="senderEmail"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            error={errors["senderEmail"]}
                            helperText={
                              errors["senderEmail"] && (
                                <Typography className="text-sm font-ebs text-red-600">
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Email not valid"
                                  />
                                </Typography>
                              )
                            }
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Sender Email"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"senderEmail"}
                          />
                        )}
                        // rules={{
                        //   validate: (value) => {
                        //     const emailExpresion = new RegExp(
                        //       "^[^@]+@[^@]+.[a-zA-Z]{2,}$"
                        //     );
                        //     return emailExpresion.test(value)
                        //       ? true
                        //       : "⚠ Invalid email";
                        //   },
                        // }}
                        defaultValue={shipment?.senderEmail}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="senderAddress"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            multiline
                            row={3}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Sender Address"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"senderAddress"}
                          />
                        )}
                        defaultValue={shipment?.senderAddress}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="senderInstitution"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Sender Institution"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"senderInstitution"}
                          />
                        )}
                        defaultValue={shipment?.senderInstitution}
                      />
                    </Grid>
                    {isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="senderCountry"
                          // rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setValue("senderCountry", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={countryList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Sender Country"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                />
                              )}
                            />
                          )}
                        />
                      </Grid>
                    )}
                    {!isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="senderCountry"
                          // rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setValue("senderCountry", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={countryList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Sender Country"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                  InputLabelProps={{
                                    shrink: true,
                                  }}
                                  InputProps={{
                                    readOnly: !isFormEditable,
                                  }}
                                />
                              )}
                            />
                          )}
                        />
                      </Grid>
                    )}
                  </Grid>

                  {/* Requestor */}
                  <Grid container item xs={4} direction="row" spacing={1}>
                    <Grid item xs={12} spacing={1}>
                      <Typography variant="h8" color="primary">
                        <FormattedMessage
                          id="none"
                          defaultMessage="Requestor"
                        />
                      </Typography>
                    </Grid>

                    <Grid item xs={12} spacing={1}>
                      {" "}
                      <Controller
                        name="requestorName"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            required
                            onChange={(e) => {
                              setRequestorNameValue(e.target.value);
                              setValue("requestorName", e.target.value);
                            }}
                            ref={requestorNameInputValue}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Requestor Name"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"requestorName"}
                          />
                        )}
                        defaultValue={shipment?.requestorName}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      {" "}
                      <Controller
                        name="requestorEmail"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            required
                            error={errors["requestorEmail"]}
                            onChange={(e) => {
                              setRequestorEmailValue(e.target.value);
                              setValue("requestorEmail", e.target.value);
                            }}
                            helperText={
                              errors["recipientEmail"] && (
                                <Typography className="text-sm font-ebs text-red-600">
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Email not valid"
                                  />
                                </Typography>
                              )
                            }
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Requestor Email"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"requestorEmail"}
                          />
                        )}
                        rules={{
                          validate: (value) => {
                            if (value) {
                              const emailExpresion = new RegExp(
                                "^[^@]+@[^@]+.[a-zA-Z]{2,}$"
                              );
                              return emailExpresion.test(value)
                                ? true
                                : "⚠ Invalid email";
                            }
                          },
                        }}
                        defaultValue={shipment?.requestorEmail}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="requestorAddress"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            multiline
                            required
                            rows={3}
                            onChange={(e) => {
                              setRequestorAddressValue(e.target.value);
                              setValue("requestorAddress", e.target.value);
                            }}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Requestor Address"
                              />
                            }
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            variant="outlined"
                            data-testid={"requestorAddress"}
                          />
                        )}
                        defaultValue={shipment?.requestorAddress}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="requestorInstitution"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            required
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Requestor Institution"
                              />
                            }
                            variant="outlined"
                            onChange={(e) => {
                              setRequestorInstitutionValue(e.target.value);
                              setValue("requestorInstitution", e.target.value);
                            }}
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"requestorInstitution"}
                          />
                        )}
                        defaultValue={shipment?.requestorInstitution}
                      />
                    </Grid>
                    {isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="requestorCountry"
                          rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setRequestorCountryValue(options.label);
                                setValue("requestorCountry", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={countryList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Requestor Country"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                />
                              )}
                            />
                          )}
                        />
                        <Grid item xs={12} spacing={1}>
                          <FormGroup aria-label="position" row>
                            <FormControlLabel
                              value="setRecipient"
                              control={<Checkbox color="primary" />}
                              onChange={handleChangeSetRecipient}
                              label="Set Requestor Information to Recipient"
                              labelPlacement="Set Requestor Information to Recipient"
                            />
                          </FormGroup>
                        </Grid>
                      </Grid>
                    )}
                    {!isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="requestorCountry"
                          rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setValue("requestorCountry", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={countryList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Requestor Country"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                  InputLabelProps={{
                                    shrink: true,
                                  }}
                                  InputProps={{
                                    readOnly: !isFormEditable,
                                  }}
                                />
                              )}
                            />
                          )}
                        />
                      </Grid>
                    )}

                    <Grid item></Grid>
                  </Grid>

                  {/* Receiver */}
                  <Grid container item xs={4} direction="row" spacing={1}>
                    <Grid item xs={12} spacing={1}>
                      <Typography variant="h8" color="primary">
                        <FormattedMessage
                          id="none"
                          defaultMessage="Recipient"
                        />
                      </Typography>
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      {" "}
                      <Controller
                        name="recipientName"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            required
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Recipient Name"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"recipientName"}
                          />
                        )}
                        defaultValue={shipment?.recipientName}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      {" "}
                      <Controller
                        name="recipientEmail"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            error={errors["recipientEmail"]}
                            required
                            helperText={
                              errors["recipientEmail"] && (
                                <Typography className="text-sm font-ebs text-red-600">
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Email not valid"
                                  />
                                </Typography>
                              )
                            }
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Recipient Email"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"recipientEmail"}
                          />
                        )}
                        rules={{
                          validate: (value) => {
                            if (value) {
                              const emailExpresion = new RegExp(
                                "^[^@]+@[^@]+.[a-zA-Z]{2,}$"
                              );
                              return emailExpresion.test(value)
                                ? true
                                : "⚠ Invalid email";
                            }
                          },
                        }}
                        defaultValue={shipment?.recipientEmail}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="recipientAddress"
                        control={control}
                        // ref={register}

                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            multiline
                            required
                            rows={3}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Recipient Address"
                              />
                            }
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            variant="outlined"
                            data-testid={"recipientAddress"}
                          />
                        )}
                        defaultValue={shipment?.recipientAddress}
                      />
                    </Grid>
                    <Grid item xs={12} spacing={1}>
                      <Controller
                        name="recipientInstitution"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            fullWidth="true"
                            required
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Recipient Institution"
                              />
                            }
                            variant="outlined"
                            InputLabelProps={{
                              shrink: true,
                            }}
                            InputProps={{
                              readOnly: !isFormEditable,
                            }}
                            data-testid={"recipientInstitution"}
                          />
                        )}
                        defaultValue={shipment?.recipientInstitution}
                      />
                    </Grid>

                    {isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="recipientCountry"
                          rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setValue("recipientCountry", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={countryList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Recipient Country"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                />
                              )}
                            />
                          )}
                        />
                      </Grid>
                    )}

                    {!isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="recipientCountry"
                          rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setValue("recipientCountry", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={countryList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Recipient Country"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                  InputLabelProps={{
                                    shrink: true,
                                  }}
                                  InputProps={{
                                    readOnly: !isFormEditable,
                                  }}
                                />
                              )}
                            />
                          )}
                        />
                      </Grid>
                    )}

                    {isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="recipientType"
                          rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setValue("recipientType", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={recipientTypeList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Recipient Type"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                />
                              )}
                            />
                          )}
                        />
                      </Grid>
                    )}
                    {!isFormEditable && (
                      <Grid item xs={12} spacing={1}>
                        <Controller
                          control={control}
                          name="recipientType"
                          rules={{ required: true }}
                          render={({ field: { onChange, value } }) => (
                            <Autocomplete
                              onChange={(event, options) => {
                                setValue("recipientType", options.label);
                                onChange(options);
                              }}
                              value={value}
                              options={recipientTypeList}
                              classes={{
                                option: classes.option,
                              }}
                              autoHighlight
                              getOptionLabel={(item) =>
                                item.label ? item.label : ""
                              }
                              getOptionSelected={(option, value) =>
                                value === undefined ||
                                value === "" ||
                                option.label === value.label
                              }
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Recipient Type"
                                  variant="outlined"
                                  error={!!errors.item}
                                  helperText={errors.item && "item required"}
                                  required
                                  InputLabelProps={{
                                    shrink: true,
                                  }}
                                  InputProps={{
                                    readOnly: !isFormEditable,
                                  }}
                                />
                              )}
                            />
                          )}
                        />
                      </Grid>
                    )}
                  </Grid>
                </Grid>

                <Grid container item xs={12} direction="row" spacing={2}>
                  <Grid item xs={12}>
                    <Controller
                      name="remarks"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          fullWidth="true"
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Remarks"
                            />
                          }
                          variant="outlined"
                          multiline
                          rows={2}
                          data-testid={"remarks"}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          InputProps={{
                            readOnly: !isFormEditable,
                          }}
                        />
                      )}
                      defaultValue={shipment?.remarks}
                    />
                  </Grid>
                </Grid>
              </Paper>
            </Grid>

            {/* Client Form Input - END*/}
          </Grid>
          {/* */}
          {isFormEditable && !isSHU && (
            <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-end"
            >
              <Grid item spacing={2}>
                <Button
                  onClick={handleClose}
                  // onClick={() => window.location.reload()}
                  variant="contained"
                  color="secondary"
                  // className="btn btn-cancel"
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white btn-cancel"
                >
                  <FormattedMessage id="none" defaultMessage="Cancel" />
                </Button>
              </Grid>

              <Grid item spacing={2}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  // className="btn btn-success"
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white btn-success"
                >
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </Grid>
            </Grid>
          )}
        </form>
      </div>
    );
  }
);
// Type and required properties
BasicInfoMolecule.propTypes = {
  // refresh: PropTypes.func.isRequired,
  type: PropTypes.oneOf(["post", "put"]).isRequired,
};
// Default properties
BasicInfoMolecule.defaultProps = { type: "put" };

export default BasicInfoMolecule;
