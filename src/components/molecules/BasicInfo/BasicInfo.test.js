  import BasicInfo from './BasicInfo';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('BasicInfo is in the DOM', () => {
  render(<BasicInfo></BasicInfo>)
  expect(screen.getByTestId('BasicInfoTestId')).toBeInTheDocument();
})
