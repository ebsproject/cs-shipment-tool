  import ShipmentItemBrowser from './ShipmentItemBrowser';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowser is in the DOM', () => {
  render(<ShipmentItemBrowser></ShipmentItemBrowser>)
  expect(screen.getByTestId('ShipmentItemBrowserTestId')).toBeInTheDocument();
})
