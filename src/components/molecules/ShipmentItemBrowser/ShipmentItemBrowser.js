import React, { useState, useEffect, useMemo } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import { Core } from "@ebs/styleguide";
const { Box, Typography, Select, MenuItem, Chip, Label, LinearProgress } = Core;
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { EbsGrid } from "@ebs/components";
import { getShipmentsData } from "store/modules/ShipmentsReducer";
import ToolBarButtonImport from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonImport";
import ToolBarButtonBulkUpdate from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonBulkUpdate";
import ToolBarButtonBulkDelete from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonBulkDelete";
import ToolBarButtonViewMlsAncetors from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonViewMlsAncestors";
import ToolBarButtonOrderItemNumber from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonOrderItemNumber";
import ToolBarButtonMtaUpdate from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonMtaUpdate";
import ToolBarButtonDownload from "components/atom/ShipmentItemBrowser/ShipmentItemDownload";
import RowButtonMessage from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonMessage";
import RowButtonEdit from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonEdit";
import RowButtonDelete from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonDelete";
import RowButtonFailed from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonFailed";
import RowButtonPassed from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonPassed";
import RowTextFieldPackageQty from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserTextFieldPackageQty";
import RowTextFieldVolume from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserTextFieldVolume";
import RowSelectVolumeUnit from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserSelectVolumeUnit";
import RowTextFieldRsht from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserTextFieldRshtNumber";
import RowSelectFieldItemStatus from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserSelectItemStatus";
import RefreshButton from "components/atom/ShipmentBrowser/ShipmentBrowserButtonRefresh";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserMolecules = React.forwardRef(({ refresh }, ref) => {
  const [data, setData] = useState("");
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const dispatch = useDispatch();
  const { shipmentDbId, shipment, isFormEditable } = useSelector(
    ({ shipmentsReducer }) => shipmentsReducer
  );
  const { isSHU } = useSelector(({ personReducer }) => personReducer);
  const { getState } = useStore();
  const [status, setStatus] = useState("Done");

  function setColor(value) {
    if (value == "passed") {
      return "primary";
    } else if (value == "failed") {
      return "secondary";
    } else {
      return "#f50057";
    }
  }

  const columns = [
    {
      Header: "id",
      accessor: "shipmentItemDbId",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Item Number" />
        </Typography>
      ),
      csvHeader: "Item Number",
      accessor: "shipmentItemNumber",
      width: 120,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="RSHT" />
        </Typography>
      ),
      Cell: ({ row }) => {
        return isFormEditable ? (
          <RowTextFieldRsht value={row} refresh={refresh} />
        ) : (
          <Typography>
            <FormattedMessage
              id="none"
              defaultMessage={row.values["testCode"]}
            />
          </Typography>
        );
      },
      csvHeader: "RSHT",
      accessor: "testCode",
      width: 150,
      // hidden: clientColumnEditShow["package_Count"],
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Volume" />
        </Typography>
      ),
      Cell: ({ row }) => {
        return isFormEditable && !isSHU ? (
          <RowTextFieldVolume value={row} refresh={refresh} />
        ) : (
          <Typography>
            <FormattedMessage
              id="none"
              defaultMessage={row.values["shipmentItemWeight"].toString()}
            />
          </Typography>
        );
      },
      csvHeader: "Volume",
      accessor: "shipmentItemWeight",
      width: 150,
      // hidden: clientColumnEditShow["package_Count"],
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Unit" />
        </Typography>
      ),
      Cell: ({ row }) => {
        return isFormEditable && !isSHU ? (
          <RowSelectVolumeUnit value={row} refresh={refresh} />
        ) : (
          <Typography>
            <FormattedMessage
              id="none"
              defaultMessage={row.values["packageUnit"]}
            />
          </Typography>
        );
      },

      csvHeader: "Package Unit",
      accessor: "packageUnit",
      width: 100,
      // hidden: clientColumnEditShow["unit"],
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Package Count" />
        </Typography>
      ),
      Cell: ({ row }) => {
        return isFormEditable && !isSHU ? (
          <RowTextFieldPackageQty value={row} refresh={refresh} />
        ) : (
          <Typography>
            <FormattedMessage
              id="none"
              defaultMessage={row.values["packageCount"].toString()}
            />
          </Typography>
        );
      },
      csvHeader: "Package Qty",
      accessor: "packageCount",
      width: 150,
      // hidden: clientColumnEditShow["package_Count"],
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Item Status" />
        </Typography>
      ),
      Cell: ({ value }) => {
        // return <RowSelectFieldItemStatus value={value} />;
        return (
          <Chip
            size="medium"
            color={setColor(value)}
            // value == "passed" ? "primary" : "secondary"

            label={value}
          />
        );
      },
      csvHeader: "Item Status",
      accessor: "shipmentItemStatus",
      width: 150,
      // hidden: clientColumnEditShow["package_Count"],
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Sample Id" />
        </Typography>
      ),
      csvHeader: "Sample Id",
      accessor: "sampleId",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Designation" />
        </Typography>
      ),
      csvHeader: "Designation",
      accessor: "germplasmDesignation",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="MTA" />
        </Typography>
      ),
      csvHeader: "MTA Status",
      accessor: "mtaStatus",
      width: 100,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Seed Name" />
        </Typography>
      ),
      csvHeader: "Seed Name",
      accessor: "seedName",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Other Names" />
        </Typography>
      ),
      csvHeader: "Other Names",
      accessor: "othernames",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Parentage" />
        </Typography>
      ),
      csvHeader: "Parentage",
      accessor: "germplasmParentage",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Generation" />
        </Typography>
      ),
      csvHeader: "Germplasm Generation",
      accessor: "germplasmGeneration",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Genetic Stock" />
        </Typography>
      ),
      csvHeader: "Genetic Stock",
      accessor: "geneticStock",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Origin" />
        </Typography>
      ),
      csvHeader: "Origin",
      accessor: "origin",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Species" />
        </Typography>
      ),
      csvHeader: "Species",
      accessor: "species",
      width: 150,
    },

    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Package Code" />
        </Typography>
      ),
      csvHeader: "Package Code",
      accessor: "packageCode",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Package Label" />
        </Typography>
      ),
      csvHeader: "Package Label",
      accessor: "packageLabel",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Source Study" />
        </Typography>
      ),
      csvHeader: "Source Study",
      accessor: "sourceStudy",
      width: 150,
    },

    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Note" />
        </Typography>
      ),
      hidden: true,
      csvHeader: "Note",
      accessor: "notes",
      width: 150,
    },

    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Package Qty" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <RowTextFieldVolume value={value} />;
    //   },
    //   csvHeader: "Packages Qty",
    //   accessor: "number_of_sample",
    //   width: 100,
    //   hidden: clientColumnEditShow["package_qty"],
    // },
    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage id="none" defaultMessage="RSHT Number" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <RowTextFieldRsht value={value} />;
    //   },
    //   csvHeader: "RSHT",
    //   accessor: "rsht_number",
    //   width: 100,
    //   hidden: shuColumnEditShow["rsht"],
    // },
    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Status" />
    //     </Typography>
    //   ),

    //   csvHeader: "status",
    //   accessor: "status",
    //   width: 100,
    //   hidden: shuColumnEditShow["rsht"],
    // },
  ];

  useEffect(() => {}, [data]);

  async function fetch({ page, sort, filters }) {
    var bodyFormData = new FormData();
    bodyFormData.append("shipmentDbId", JSON.stringify(shipmentDbId));
    var sortData = "shipmentItemNumber:ASC";

    if (filters.length > 0) {
      filters.forEach(function (item, index) {
        bodyFormData.append(item.col, item.val);
      });
    }

    if (sort.length > 0) {
      if (sort[0].mod == "DES") {
        sortData = sort[0].col + ":DESC";
      } else {
        sortData = sort[0].col + ":ASC";
      }
    }

    return new Promise((resolve, reject) => {
      try {
        cbClient
          .post(
            "shipment-items-search?page=" +
              page.number +
              "&limit=" +
              page.size +
              "&sort=" +
              sortData,
            bodyFormData
          )
          .then((response) => {
            setData(response.data.result.data);
            resolve({
              pages: response.data.metadata.pagination.totalPages,
              data: response.data.result.data,
              elements: response.data.metadata.pagination.totalCount,
            });
          })
          .catch(({ message }) => {
            reject(message);
          });
      } catch (error) {
        console.log(error);
      }
    });
  }

  const toolbarActions = (selection, refresh) => {
    return (
      <div display="flex">
        <Box display="flex" justifyContent="center" flexDirection="row">
          {isFormEditable && !isSHU && (
            <Box>{<ToolBarButtonImport refresh={refresh} />}</Box>
          )}
          <Box>&nbsp;</Box>
          {isFormEditable && !isSHU && (
            <Box>
              <ToolBarButtonBulkUpdate
                refresh={refresh}
                selection={selection}
                dataItem={data}
              />
            </Box>
          )}
          <Box>&nbsp;</Box>
          {isFormEditable && (
            <Box>
              <ToolBarButtonBulkDelete
                refresh={refresh}
                selection={selection}
                dataItem={data}
              />
            </Box>
          )}
          <Box>&nbsp;</Box>
          {isFormEditable && !isSHU && (
            <Box>
              <ToolBarButtonOrderItemNumber
                refresh={refresh}
                selection={selection}
                dataItem={data}
              />
            </Box>
          )}
          <Box>&nbsp;</Box>
          <Box>
            <ToolBarButtonViewMlsAncetors
              refresh={refresh}
              selection={selection}
            />
          </Box>
          <Box>&nbsp;</Box>
          {/* {isFormEditable && !isSHU && ( */}
          <Box>
            <ToolBarButtonMtaUpdate
              refresh={refresh}
              selection={selection}
              dataItem={data}
            />
          </Box>
          {/* )} */}
          <Box>&nbsp;</Box>
          {/* <Box>
            <ToolBarButtonDownload refresh={refresh} selection={selection} />
          </Box>
          <Box>&nbsp;</Box> */}

          {/* <Box>{<RefreshButton refresh={refresh} />}</Box> */}
        </Box>
      </div>
    );
  };

  const rowActions = (rowData, refresh) => {
    let isDelete = false;
    let isPassed = false;
    let isFailed = false;
    let isEdit = false;
    let isMessage = true;

    if (isFormEditable) {
      isDelete = true;
      isEdit = true;

      if (
        rowData.notes !== undefined &&
        rowData.notes !== null &&
        rowData.notes !== ""
      ) {
        isMessage = true;
      } else {
        isMessage = false;
      }

      if (rowData.shipmentItemStatus == "passed") {
        isPassed = false;
        isFailed = true;
      } else {
        isPassed = true;
        isFailed = false;
        isMessage = true;
      }
    }
    if (!isSHU) {
      isPassed = false;
      isFailed = false;
    } else {
      isEdit = false;
    }

    return (
      <Box display="flex" flexDirection="row">
        {isEdit && (
          <Box>
            <RowButtonEdit rowData={rowData} refresh={refresh} />
          </Box>
        )}

        {isDelete && (
          <Box>
            <RowButtonDelete
              shipmentItemDbId={rowData.shipmentItemDbId}
              refresh={refresh}
            />
          </Box>
        )}

        {isFailed && (
          <Box>
            <RowButtonFailed rowData={rowData} refresh={refresh} />
          </Box>
        )}

        {isPassed && (
          <Box>
            <RowButtonPassed rowData={rowData} refresh={refresh} />
          </Box>
        )}

        {isMessage && (
          <Box>
            <RowButtonMessage rowData={rowData} refresh={refresh} />
          </Box>
        )}
      </Box>
    );
  };

  // if (isFormEditable) {
  return (
    /* 
     @prop data-testid: Id to use inside shipmentitembrowser.test.js file.
     */
    <div>
      <EbsGrid
        id="shipmentItemBrowserId"
        toolbar={true}
        columns={columns}
        title={
          <Typography variant="h8" color="primary">
            <FormattedMessage id="none" defaultMessage="Shipment Items" />
          </Typography>
        }
        fetch={fetch}
        toolbaractions={toolbarActions}
        select="multi"
        rowactions={rowActions}
        raWidth={150}
        height="65vh"
        csvfilename={shipment.shipmentName}
        globalFilter={false}
      />
    </div>
  );
  // } else {
  //   <LinearProgress />;
  // }
});
// Type and required properties
ShipmentItemBrowserMolecules.propTypes = {};
// Default properties
ShipmentItemBrowserMolecules.defaultProps = {};

export default ShipmentItemBrowserMolecules;
