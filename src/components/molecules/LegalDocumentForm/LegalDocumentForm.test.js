  import LegalDocumentForm from './LegalDocumentForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('LegalDocumentForm is in the DOM', () => {
  render(<LegalDocumentForm></LegalDocumentForm>)
  expect(screen.getByTestId('LegalDocumentFormTestId')).toBeInTheDocument();
})
