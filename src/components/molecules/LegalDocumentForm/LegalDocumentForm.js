import React, { useState, useEffect } from "react";
import { getDomainContext } from "@ebs/layout";
const { context, sgContext } = getDomainContext("cs"); //"cs", "sm", "ba", "cb"
import PropTypes from "prop-types";
import { Core, Icons, Styles } from "@ebs/styleguide";
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import Docxtemplater from "docxtemplater";
import PizZip from "pizzip";
import PizZipUtils from "pizzip/utils/index.js";
import { saveAs } from "file-saver";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { useForm, Controller } from "react-hook-form";
import {
  createShipment,
  updateShipment,
  findShipment,
} from "store/modules/ShipmentsReducer";
const {
  Grid,
  Paper,
  Button,
  TextField,
  Typography,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Select,
  MenuItem,
} = Core;
const { useTheme } = Styles;

const getDocumentType = [
  { id: "shrink", name: "Shrink Wrap" },
  { id: "signed", name: "Signed" },
];



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const LegalDocumentFormMolecule = React.forwardRef(({}, ref) => {
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
  } = useForm({
    defaultValues: {
      mtaDocToGenerate: null,
      smtaIdMlsId: "",
      smtaIdPud1: "",
      smtaIdPud2: "",
    },
  });
  const dispatch = useDispatch();
  const theme = useTheme();
  const classes ={
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "left",
      color: theme.palette.text.secondary,
    },
    div: {
      padding: theme.spacing(1),
    },
  };
  const { getState } = useStore();
  const { shipmentDbId, mode, shipment, loading } = useSelector(
    ({ shipmentsReducer }) => shipmentsReducer
  );
  const { person, personProgram, isSHU } = useSelector(
    ({ personReducer }) => personReducer
  );

  const [smtaTypeRadio, setSmtaTypeRadio] = React.useState(null);
  const [smtaOptionValue, setSmtaOptionValue] = React.useState(null);
  const [docTypeOptionValue, setDocTypeOptionValue] = React.useState("shrink");
  const [smtaValue, setSmtaValue] = React.useState("");
  const [smtaIdMls, setSmtaIdMls] = React.useState("");
  const [smtaIdPud1, setSmtaIdPud1] = React.useState("");
  const [smtaIdPud2, setSmtaIdPud2] = React.useState("");
  const [otherMta, setOtherMta] = React.useState("");
  const [smtaIdMlsCurrent, setSmtaIdMlsCurrent] = React.useState("");
  const [smtaIdPud1Current, setSmtaIdPud1Current] = React.useState("");
  const [smtaIdPud2Current, setSmtaIdPud2Current] = React.useState("");
  const [otherMtaCurrent, setOtherMtaCurrent] = React.useState("");
  const [smtaIdMlsExist, setSmtaIdMlsExist] = React.useState("");
  const [smtaIdPud1Exist, setSmtaIdPud1Exist] = React.useState("");
  const [smtaIdPud2Exist, setSmtaIdPud2Exist] = React.useState("");
  const [otherMtaExist, setOtherMtaExist] = React.useState("");
  const [selectedSmtaId, setSelectedSmtaId] = React.useState("");
  const [selectedDocStatus, setSelectedDocStatus] = React.useState();
  const [pud1OptionHidden, setPud1OptionHidden] = React.useState(true);
  const [fileName, setFileName] = useState();
  const [isOtherMtaSelected, setIsOtherMtaSelected] = useState(false);
  const [disableGenerate, setDisableGenerate] = useState(true);

  function loadFile(url, callback) {
    PizZipUtils.getBinaryContent(url, callback);
  }

  function handleClick(event) {
    if (event.target.value === smtaTypeRadio) {
      setSmtaTypeRadio("");
    } else {
      setSmtaTypeRadio(event.target.value);
      setSmtaValue("");
    }
    if (event.target.value == "otherMta") {
      setIsOtherMtaSelected(true);
    } else {
      setIsOtherMtaSelected(false);
    }
  }

  function handleSmtaMlsChanges(e) {
    setValue("smtaIdMls", e.target.value);
    setSmtaIdMls(e.target.value);
  }

  function handleSmtaPud1Changes(e) {
    setValue("smtaIdPud1", e.target.value);
    setSmtaIdPud1(e.target.value);
  }

  function handleSmtaPud2Changes(e) {
    setValue("smtaIdPud2", e.target.value);
    setSmtaIdPud2(e.target.value);
  }

  function handleOtherMtaChanges(e) {
    setValue("otherMta", e.target.value);
    setOtherMta(e.target.value);
  }

  function handleChangeDocType(newValue) {
    setDocTypeOptionValue(newValue.target.value);
  }

  function handleChangeRadioGroupSMTA(newValue) {
    setDocTypeOptionValue("shrink");
    setSmtaOptionValue(newValue.target.value);

    if (newValue.target.value == "pud1") {
      setPud1OptionHidden(false);
    } else {
      setPud1OptionHidden(true);
    }
    setSelectedDocStatus(newValue.target.value);
  }

  function handleClickSmtaRadio(event) {
    if (event.target.value === smtaValue) {
      setSmtaValue("");
    } else {
      setSmtaValue(event.target.value);
      setSmtaTypeRadio("");
    }

    if (event.target.value == "pud1") {
      setSelectedSmtaId(smtaIdPud1Current);
    } else if (event.target.value == "mls") {
      setSelectedSmtaId(smtaIdMlsCurrent);
    } else if (event.target.value == "pud2") {
      setSelectedSmtaId(smtaIdPud2Current);
    }
  }

  useEffect(() => {}, [selectedSmtaId]);

  function generateDocument(data, smtaId) {
    // console.log("data?.materials", data?.materials);
    // console.log("data?.mlsancestors", data?.mlsancestors);
    // console.log("smtaId:", smtaId);
    // console.log("shuReferenceNumber:", data?.shuReferenceNumber);
    // console.log("signatory:", data?.authorizedSignatory);
    // console.log("country:", data?.recipientCountry);
    // console.log(" address: ", data?.recipientAddress);

    let templateUrl = "";
    let fileName = "";
    let docUrl = "";
    // const basePathUrl =
    //   context +
    //   "/mfe/shipment/" +
    //   process.env.REACT_APP_SHIPMENT_RELEASE_VERSION +
    //   "/template/";

    const basePathUrl = "https://cdn.ebsproject.org/templates/ebs-sh/";

    if (smtaOptionValue == "mls" && docTypeOptionValue == "shrink") {
      templateUrl = basePathUrl + "SMTA_MLS_Shrink-Wrap.docx";
      fileName = smtaId + "_SMTA_MLS_Shrink-Wrap.docx";
    } else if (smtaOptionValue == "mls" && docTypeOptionValue == "signed") {
      templateUrl = basePathUrl + "SMTA_MLS_Signed.docx";
      fileName = smtaId + "_SMTA_MLS_Signed.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "shrink") {
      templateUrl = basePathUrl + "SMTA_UD_Shrink-Wrap.docx";
      fileName = smtaId + "_SMTA_UD_Shrink-Wrap.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "signed") {
      templateUrl = basePathUrl + "SMTA_UD_Signed.docx";
      fileName = smtaId + "_SMTA_UD_Signed.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "hrdc") {
      templateUrl = basePathUrl + "HRDC_OMTA.docx";
      fileName = smtaId + "_HRDC_OMTA.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "irriomta") {
      templateUrl = basePathUrl + "IRRI_OMTA.docx";
      fileName = smtaId + "_IRRI_OMTA.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "jircas") {
      templateUrl = basePathUrl + "JIRCAS_OMTA.docx";
      fileName = smtaId + "_JIRCAS_OMTA.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "narvi") {
      templateUrl = basePathUrl + "NARVI_OMTA.docx";
      fileName = "NARVI_OMTA.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "vrap") {
      templateUrl = basePathUrl + "VRAP_OMTA.docx";
      fileName = smtaId + "_VRAP_OMTA.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "inger") {
      templateUrl = basePathUrl + "INGER_OMTA.docx";
      fileName = smtaId + "_INGER_OMTA.docx";
    } else if (smtaOptionValue == "pud1" && docTypeOptionValue == "asean") {
      templateUrl = basePathUrl + "ASEAN_OMTA.docx";
      fileName = smtaId + "_ASEAN_OMTA.docx";
    } else if (smtaOptionValue == "pud2" && docTypeOptionValue == "shrink") {
      templateUrl = basePathUrl + "SMTA_UD_Shrink-Wrap.docx";
      fileName = smtaId + "_SMTA_UD_Shrink-Wrap.docx";
    } else if (smtaOptionValue == "pud2" && docTypeOptionValue == "signed") {
      templateUrl = basePathUrl + "SMTA_UD_Signed.docx";
      fileName = smtaId + "_SMTA_UD_Signed.docx";
    }

    loadFile(templateUrl, function (error, content) {
      if (error) {
        throw error;
      }
      var zip = new PizZip(content);
      var doc = new Docxtemplater(zip, {
        paragraphLoop: true,
        linebreaks: true,
      });

      var options = { month: "long", day: "numeric", year: "numeric" };
      var today = new Date();
      var cDtd = today.toLocaleDateString("default", options);
      var newDate = cDtd.split(" ");
      var cDate =
        newDate[1].substring(0, newDate[1].length - 1) +
        " " +
        newDate[0] +
        " " +
        newDate[2];

      if (data?.materials.includes("IRGC")) {
        docUrl = "https://www.genesys-pgr.org/";
      }
      doc.setData({
        smtaId: smtaId,
        shuReferenceNumber: data?.shuReferenceNumber,
        signatory: data?.authorizedSignatory,
        recipient: data?.recipientName,
        institution: data?.recipientInstitution,
        country: data?.recipientCountry,
        address: data?.recipientAddress,
        materials: data?.materials,
        mlsAncestors: data?.mls_ancestors,
        currentDate: cDate,
        url: docUrl,
      });

      try {
        doc.render();
      } catch (error) {
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a properties object containing all suberrors).
        function replaceErrors(key, value) {
          if (value instanceof Error) {
            return Object.getOwnPropertyNames(value).reduce(function (
              error,
              key
            ) {
              error[key] = value[key];
              return error;
            },
            {});
          }
          return value;
        }

        if (error.properties && error.properties.errors instanceof Array) {
          const errorMessages = error.properties.errors
            .map(function (error) {
              return error.properties.explanation;
            })
            .join("\n");
          console.log("errorMessages", errorMessages);
        }
        throw error;
      }
      var out = doc.getZip().generate({
        type: "blob",
        mimeType:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      }); //Output the document using Data-URI
      saveAs(out, fileName);
    });
  }

  const onSubmit = (data) => {
    try {
      cbClient
        .get(`/shipments/${shipmentDbId}/smta-report?smtaId=${selectedSmtaId}`)
        .then(function (response) {
          generateDocument(response.data.result.data[0], selectedSmtaId);
        })
        .catch(function (error) {
          console.log(error);
        });
    } catch ({ message }) {
    } finally {
    }
  };

  useEffect(() => {
    if (isSHU) {
      setDisableGenerate(true);
      try {
        cbClient
          .get(`/shipments/${shipmentDbId}/items-smta`)
          .then(function (response) {
            response.data.result.data.forEach(function (data) {
              if (data.smtaStatus != null) {
                if (
                  data.smtaStatus.toLowerCase() == "fao" ||
                  data.smtaStatus.toLowerCase() == "mls" ||
                  data.smtaStatus.toLowerCase() == "rel1" ||
                  data.smtaStatus.toLowerCase() == "smta"
                ) {
                  setSmtaIdMlsExist(true);
                  setValue("smtaIdMlsId", data.smtaId);
                  setSmtaIdMlsCurrent(data.smtaId);
                  if (data.smtaId != null) {
                    setDisableGenerate(false);
                  }
                } else if (
                  data.smtaStatus.toLowerCase() == "pud" ||
                  data.smtaStatus.toLowerCase() == "pud1"
                ) {
                  setSmtaIdPud1Exist(true);
                  setValue("smtaIdPud1", data.smtaId);
                  setSmtaIdPud1Current(data.smtaId);
                  if (data.smtaId != null) {
                    setDisableGenerate(false);
                  }
                } else if (data.smtaStatus.toLowerCase() == "pud2") {
                  setSmtaIdPud2Exist(true);
                  setValue("smtaIdPud2", data.smtaId);
                  setSmtaIdPud2Current(data.smtaId);
                  if (data.smtaId != null) {
                    setDisableGenerate(false);
                  }
                }
              }
            });
            return;
          })
          .catch(function (error) {
            // console.log(error);
          });
      } catch ({ message }) {
      } finally {
      }
    }
  }, [smtaIdMlsExist, smtaIdPud1Exist, smtaIdPud2Exist]);

  useEffect(() => {
    setDisableGenerate(true);
    const delayDebounceFn = setTimeout(() => {
      if (smtaIdMls.length > 0 && smtaIdMlsCurrent != smtaIdMls) {
        try {
          var bodyFormData = new FormData();
          bodyFormData.append("smtaId", smtaIdMls);
          bodyFormData.append("smtaType", "mls");

          cbClient
            .post(`/shipments/${shipmentDbId}/assign-smta`, bodyFormData)
            .then(function (response) {
              setSmtaIdMlsCurrent(smtaIdMls);
              setDisableGenerate(false);
              return;
            })
            .catch(function (error) {
              console.log(error);
            });
        } catch ({ message }) {
        } finally {
        }
      }
    }, 3000);

    return () => clearTimeout(delayDebounceFn);
  }, [smtaIdMls]);

  useEffect(() => {
    setDisableGenerate(true);
    const delayDebounceFn = setTimeout(() => {
      if (smtaIdPud1.length > 0 && smtaIdPud1Current != smtaIdPud1) {
        try {
          var bodyFormData = new FormData();
          bodyFormData.append("smtaId", smtaIdPud1);
          bodyFormData.append("smtaType", "pud1");

          cbClient
            .post(`/shipments/${shipmentDbId}/assign-smta`, bodyFormData)
            .then(function (response) {
              setSmtaIdPud1Current(smtaIdPud1);
              setDisableGenerate(false);
              return;
            })
            .catch(function (error) {
              console.log(error);
            });
        } catch ({ message }) {
        } finally {
        }
      }
    }, 3000);

    return () => clearTimeout(delayDebounceFn);
  }, [smtaIdPud1]);

  useEffect(() => {
    setDisableGenerate(true);
    const delayDebounceFn = setTimeout(() => {
      if (smtaIdPud2.length > 0 && smtaIdPud2Current != smtaIdPud2) {
        try {
          var bodyFormData = new FormData();
          bodyFormData.append("smtaId", smtaIdPud2);
          bodyFormData.append("smtaType", "pud");

          cbClient
            .post(`/shipments/${shipmentDbId}/assign-smta`, bodyFormData)
            .then(function (response) {
              setSmtaIdPud2Current(smtaIdPud2);
              setDisableGenerate(false);
              return;
            })
            .catch(function (error) {
              console.log(error);
            });
        } catch ({ message }) {
        } finally {
        }
      }
    }, 3000);
    return () => clearTimeout(delayDebounceFn);
  }, [smtaIdPud2]);

  useEffect(() => {
    setDisableGenerate(true);
    const delayDebounceFn = setTimeout(() => {
      if (otherMta.length > 0 && otherMtaCurrent != otherMta) {
        try {
          var bodyFormData = new FormData();
          bodyFormData.append("smtaId", otherMta);
          bodyFormData.append("smtaType", "other");

          cbClient
            .post(`/shipments/${shipmentDbId}/assign-smta`, bodyFormData)
            .then(function (response) {
              setOtherMtaCurrent(otherMta);
              setDisableGenerate(false);
              return;
            })
            .catch(function (error) {
              console.log(error);
            });
        } catch ({ message }) {
        } finally {
        }
      }
      //end axios request
    }, 3000);

    return () => clearTimeout(delayDebounceFn);
  }, [otherMta]);

  return (
    /* 
     @prop data-testid: Id to use inside LegalDocumentForm.test.js file.
     */
    <div className={classes.root}>
      {isSHU && (
        <Paper className={classes.paper}>
          <form
            onSubmit={handleSubmit(onSubmit)}
            data-testid="documentLegalForm"
          >
            {/* {shipment?.shipmentName} */}
            <FormControl component="fieldset">
              <RadioGroup
                aria-label="mtaDocToGenerate"
                name="mtaDocToGenerate"
                value={smtaValue}
                onChange={handleChangeRadioGroupSMTA}
              >
                <div className={classes.div}>
                  {smtaIdMlsExist && (
                    <Grid container spacing={3} direction="row">
                      <Grid item xs={2}>
                        <FormControlLabel
                          align="right"
                          value="mls"
                          control={<Radio onClick={handleClickSmtaRadio} />}
                        />
                      </Grid>

                      <Grid item xs={4}>
                        <Typography variant="h6" color="primary" align="right">
                          <FormattedMessage id="none" defaultMessage="MLS" />
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Controller
                          name="smtaIdMlsId"
                          control={control}
                          render={({ field }) => (
                            <TextField
                              {...field}
                              variant="outlined"
                              label={
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="SMTA ID"
                                />
                              }
                              data-testid={"smtaIdMlsId"}
                              onChange={handleSmtaMlsChanges}
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          )}
                          defaultValue={smtaIdMlsCurrent}
                        />
                      </Grid>
                    </Grid>
                  )}
                </div>

                <div className={classes.div}>
                  {smtaIdPud1Exist && (
                    <Grid container spacing={3} direction="row">
                      <Grid item xs={2}>
                        <FormControlLabel
                          value="pud1"
                          control={<Radio onClick={handleClickSmtaRadio} />}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Typography variant="h6" color="primary" align="right">
                          <FormattedMessage
                            id="none"
                            defaultMessage="PUD and PUD1"
                          />
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Controller
                          name="smtaIdPud1"
                          control={control}
                          render={({ field }) => (
                            <TextField
                              {...field}
                              variant="outlined"
                              label={
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="SMTA ID"
                                />
                              }
                              data-testid={"smtaIdPud1"}
                              onChange={handleSmtaPud1Changes}
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          )}
                          defaultValue={smtaIdPud1Current}
                        />
                      </Grid>
                    </Grid>
                  )}
                </div>

                <div className={classes.div}>
                  {smtaIdPud2Exist && (
                    <Grid container spacing={3} direction="row">
                      <Grid item xs={2}>
                        <FormControlLabel
                          value="pud2"
                          control={<Radio onClick={handleClickSmtaRadio} />}
                        />
                      </Grid>
                      <Grid item xs={4} spacing={2}>
                        <Typography variant="h6" color="primary" align="right">
                          <FormattedMessage id="none" defaultMessage="PUD2" />
                        </Typography>
                      </Grid>
                      <Grid item xs={6} spacing={2}>
                        <Controller
                          name="smtaIdPud2"
                          control={control}
                          render={({ field }) => (
                            <TextField
                              {...field}
                              variant="outlined"
                              label={
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="SMTA ID"
                                />
                              }
                              data-testid={"smtaIdPud2"}
                              onChange={handleSmtaPud2Changes}
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          )}
                          defaultValue={smtaIdPud2Current}
                        />
                      </Grid>
                    </Grid>
                  )}
                </div>
              </RadioGroup>
            </FormControl>

            <div className={classes.div}>
              <Grid container spacing={3} direction="col">
                <Grid item xs={4} justifyContent="flex-end">
                  Document Type
                </Grid>
                <Grid item xs={4}>
                  <Select
                    id="documentType"
                    value={docTypeOptionValue}
                    label="Document Type"
                    onChange={handleChangeDocType}
                  >
                    <MenuItem value=""></MenuItem>
                    <MenuItem value="shrink">Shrink Wrap</MenuItem>
                    <MenuItem value="signed">Signed</MenuItem>

                    <MenuItem hidden={pud1OptionHidden} value="hrdc">
                      HRDC_OMTA
                    </MenuItem>
                    <MenuItem hidden={pud1OptionHidden} value="jircas">
                      JIRCAS_OMTA
                    </MenuItem>
                    <MenuItem hidden={pud1OptionHidden} value="narvi">
                      NARVI_OMTA
                    </MenuItem>
                    <MenuItem hidden={pud1OptionHidden} value="vrap">
                      VRAP_OMTA
                    </MenuItem>
                    <MenuItem hidden={pud1OptionHidden} value="inger">
                      IRRI-INGER-OMTA
                    </MenuItem>
                    <MenuItem hidden={pud1OptionHidden} value="asean">
                      ASEAN RiceNet-OMTA
                    </MenuItem>
                    <MenuItem hidden={pud1OptionHidden} value="irriomta">
                      IRRI-OMTA
                    </MenuItem>
                  </Select>
                </Grid>

                <Grid item xs={4}>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={disableGenerate}
                    className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                  >
                    Generate
                  </Button>
                </Grid>
              </Grid>
            </div>

            <div>
              <Grid container spacing={3} direction="row">
                <Grid item xs={6} justifyContent="flex-end">
                  <FormControl component="fieldset">
                    <RadioGroup
                      aria-label="otherDoc"
                      name="otherDocType"
                      value={smtaTypeRadio}
                    >
                      <FormControlLabel
                        value="noMta"
                        control={<Radio onClick={handleClick} />}
                        label="No MTA"
                      />
                      <div>
                        <FormControlLabel
                          value="otherMta"
                          control={<Radio onClick={handleClick} />}
                          label="Other MTA"
                        />
                      </div>
                    </RadioGroup>
                  </FormControl>
                </Grid>
                <Grid item xs={6}></Grid>
              </Grid>
            </div>

            <Grid container>
              {isOtherMtaSelected && (
                <Controller
                  name="otherMta"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      fullWidth="true"
                      variant="outlined"
                      label={
                        <FormattedMessage
                          id="none"
                          defaultMessage="Other MTA"
                        />
                      }
                      data-testid={"otherMta"}
                      onChange={handleOtherMtaChanges}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  )}
                  defaultValue={otherMtaCurrent}
                />
              )}
            </Grid>
          </form>
        </Paper>
      )}
    </div>
  );
});
// Type and required properties
LegalDocumentFormMolecule.propTypes = {};
// Default properties
LegalDocumentFormMolecule.defaultProps = {};

export default LegalDocumentFormMolecule;
