  import MlsAncestorsBrowser from './MlsAncestorsBrowser';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('MlsAncestorsBrowser is in the DOM', () => {
  render(<MlsAncestorsBrowser></MlsAncestorsBrowser>)
  expect(screen.getByTestId('MlsAncestorsBrowserTestId')).toBeInTheDocument();
})
