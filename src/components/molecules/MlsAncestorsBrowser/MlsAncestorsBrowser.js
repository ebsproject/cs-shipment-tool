import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import { Core } from "@ebs/styleguide";
const { Box, Typography } = Core;
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { EbsGrid } from "@ebs/components";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MlsAncestorsBrowserMolecules = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const { shipmentDbId, shipment } = useSelector(
    ({ shipmentsReducer }) => shipmentsReducer
  );
  const { getState } = useStore();

  const columns = [
    {
      Header: "id",
      accessor: "id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Item Number" />
        </Typography>
      ),
      csvHeader: "Shipment Item Number",
      accessor: "shipment_item_number",
      width: 100,
    },
    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Sample Id" />
    //     </Typography>
    //   ),
    //   csvHeader: "Sample Id",
    //   accessor: "sample_id",
    //   width: 150,
    // },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Designation" />
        </Typography>
      ),
      csvHeader: "Designation",
      accessor: "mlsancestors",
      width: 700,
    },
    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Origin" />
    //     </Typography>
    //   ),
    //   csvHeader: "Origin",
    //   accessor: "origin",
    //   width: 150,
    // },
  ];

  async function fetch({ page, sort, filters }) {
    var bodyFormData = new FormData();
    var sortData = "";
    if (filters.length > 0) {
      filters.forEach(function (item, index) {
        bodyFormData.append(item.id, item.value);
      });
    }
    if (sort.length > 0) {
      if (sort[0].mod == "DES") {
        sortData = sort[0].col + ":DESC";
      } else {
        sortData = sort[0].col + ":ASC";
      }
    } else {
      sortData = "shipment_item_number:ASC";
    }

    return new Promise((resolve, reject) => {
      try {
        cbClient
          .post(
            `shipments/${shipmentDbId}/mls-ancestors-search?page=` +
              page.number +
              "&limit=" +
              page.size +
              "&sort=" +
              sortData,
            bodyFormData
          )
          .then((response) => {
            resolve({
              pages: response.data.metadata.pagination.totalPages,
              data: response.data.result.data,
              elements: response.data.metadata.pagination.totalCount,
            });
          })
          .catch(({ message }) => {
            reject(message);
          });
      } catch (error) {
        console.log(error);
      }
    });
  }

  const toolbarActions = (selection, refresh) => {
    return (
      <Box display="flex" justifyContent="center">
        {/* <Box>{<CreateButton refresh={refresh} />}</Box> */}
      </Box>
    );
  };

  return (
    /* 
     @prop data-testid: Id to use inside mlsancestorsbrowser.test.js file.
     */

    <div>
      <EbsGrid
        // toolbar={true}

        id="MlsAncestorBrowserId"
        columns={columns}
        // title={
        //   <Typography variant="h8" color="primary">
        //     <FormattedMessage id="none" defaultMessage="Shipment Items" />
        //   </Typography>
        // }
        fetch={fetch}
        // toolbaractions={toolbarActions}
        // select="multi"
        // rowactions={rowActions}
        height="65vh"
        csvfilename={shipment.shipmentName + "-mls_ancestors"}
        // indexing
        globalFilter={false}
      />
    </div>
  );
});
// Type and required properties
MlsAncestorsBrowserMolecules.propTypes = {};
// Default properties
MlsAncestorsBrowserMolecules.defaultProps = {};

export default MlsAncestorsBrowserMolecules;
