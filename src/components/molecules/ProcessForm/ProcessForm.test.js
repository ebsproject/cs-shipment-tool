  import ProcessForm from './ProcessForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ProcessForm is in the DOM', () => {
  render(<ProcessForm></ProcessForm>)
  expect(screen.getByTestId('ProcessFormTestId')).toBeInTheDocument();
})
