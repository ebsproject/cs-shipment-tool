import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Styles, Icons } from "@ebs/styleguide";
const { Box, Typography, Grid, Paper, Accordion, AccordionSummary, AccordionDetails } = Core;
const { useTheme } = Styles;
const  { ExpandMoreIcon } = Icons;

import { useForm, Form } from "components/atom/ShipmentFormControls/useForm";
import Controls from "components/atom/ShipmentFormControls/Controls";

const initialFValues = {
  id: 0,
  shu_reference_no: "",
  tracking_status: "",
  shipping_status_remarks: "",
  process_status: "",
  dispatch_date: "",
  airbill_no: "",
  dispatch_remark: "",
  acknowledgement: "",
};

const getShipmentTrackingStatusOptions = () => [
  { id: "processing", title: "Processing" },
  { id: "dispatched", title: "Dispatched" },
];

const getShipmentProcessStatusOptions = () => [
  { id: "put_on_hold", title: "Put on Hold" },
  { id: "reject", title: "Reject" },
  { id: "finalized", title: "Finalized" },
];



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ProcessFormMolecule = React.forwardRef(({}, ref) => {
  const theme = useTheme();
  const classes = {
    root: {
      // flexGrow: 1,
      width: "100%",
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "left",
      color: theme.palette.text.secondary,
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
  };

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("shu_reference_no" in fieldValues)
      temp.fullName = fieldValues.shu_reference_no
        ? ""
        : "This field is required.";
    // if ("email" in fieldValues)
    //   temp.email = /$^|.+@.+..+/.test(fieldValues.email)
    //     ? ""
    //     : "Email is not valid.";
    // if ("mobile" in fieldValues)
    //   temp.mobile =
    //     fieldValues.mobile.length > 9 ? "" : "Minimum 10 numbers required.";
    // if ("departmentId" in fieldValues)
    //   temp.departmentId =
    //     fieldValues.departmentId.length != 0
    //       ? ""
    //       : "This field is required.";
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useForm(initialFValues, true, validate);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      // todo call api to store records
      resetForm();
    }
  };

  return (
    /* 
     @prop data-testid: Id to use inside ProcessForm.test.js file.
     */

    <div className={classes.root}>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        ></AccordionSummary>
        <AccordionDetails>
          <Form onSubmit={handleSubmit}>
            <Paper className={classes.paper}>
              <Grid container direction="row">
                <Grid item xs={6}>
                  <Controls.Input
                    name="shu_reference_no"
                    label="Shipment Reference No"
                    value={values.shu_reference_no}
                    onChange={handleInputChange}
                    error={errors.shu_reference_no}
                  />
                  <Controls.Select
                    name="tracking_status"
                    label="Tracking Status"
                    value={values.tracking_status}
                    onChange={handleInputChange}
                    error={errors.tracking_status}
                    options={getShipmentTrackingStatusOptions()}
                  />
                  <Controls.InputMultiLine
                    name="shipping_status_remarks"
                    label="Shipping Status "
                    value={values.shipping_status_remarks}
                    onChange={handleInputChange}
                    error={errors.shipping_status_remarks}
                  />
                  <Controls.RadioGroup
                    name="process_status"
                    label="Processing Status"
                    value={values.process_status}
                    onChange={handleInputChange}
                    items={getShipmentProcessStatusOptions()}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Controls.DatePicker
                    name="dispatch_date"
                    label="Dispatched Date"
                    value={values.dispatch_date}
                    onChange={handleInputChange}
                    error={errors.dispatch_date}
                  />

                  <Controls.Input
                    name="airbill_no"
                    label="Airbill Number"
                    value={values.airbill_no}
                    onChange={handleInputChange}
                    error={errors.airbill_no}
                  />

                  <Controls.InputMultiLine
                    name="dispatch_remark"
                    label="Dispatched Remarks"
                    value={values.dispatch_remark}
                    onChange={handleInputChange}
                    error={errors.dispatch_remark}
                  />
                  <Controls.Input
                    name="acknowledgement"
                    label="Acknowledgement"
                    value={values.acknowledgement}
                    onChange={handleInputChange}
                    error={errors.acknowledgement}
                  />
                </Grid>
              </Grid>
            </Paper>

            <Paper className={classes.paper}>
              <div align="right">
                <Controls.Button type="submit" text="Save" />
                <Controls.Button
                  text="Reset"
                  color="default"
                  onClick={resetForm}
                />
              </div>
            </Paper>
          </Form>
        </AccordionDetails>
      </Accordion>
    </div>
  );
});
// Type and required properties
ProcessFormMolecule.propTypes = {};
// Default properties
ProcessFormMolecule.defaultProps = {};

export default ProcessFormMolecule;
