  import DocumentUpload from './DocumentUpload';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('DocumentUpload is in the DOM', () => {
  render(<DocumentUpload></DocumentUpload>)
  expect(screen.getByTestId('DocumentUploadTestId')).toBeInTheDocument();
})
