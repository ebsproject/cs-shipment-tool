import React, {
  useState,
  useEffect,
  memo,
  Fragment,
  useCallback,
  useReducer,
  useRef,
} from "react";
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient, poClient } from "utils/axios";
import PropTypes from "prop-types";
import { Core, Icons, Styles } from "@ebs/styleguide";
import { EbsGrid } from "@ebs/components";
import ToolBarUploadFile from "components/atom/ShipmentDocumentBrowser/ShipmentDocumentBrowserUploadFile";
import RowButtonDelete from "components/atom/ShipmentDocumentBrowser/ShipmentDocumentBrowserButtonDelete";
import RowButtonDownload from "components/atom/ShipmentDocumentBrowser/ShipmentDocumentBrowserButtonDownload";

// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { useForm, Controller } from "react-hook-form";
import UploadService from "utils/other/FileUploadService";
import DocumentUploadBrowser from "components/molecules/DocumentUploadBrowser";
import {
  createShipmentFileUploadMetadata,
  uploadShipmentFile,
  setShipmentFileUploadSuccess,
} from "store/modules/ShipmentsReducer";

const {
  Box,
  Grid,
  Paper,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  Button
} = Core;






//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DocumentUploadMolecule = React.forwardRef(({ refresh }, ref) => {
  

  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
  } = useForm();
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { shipmentDbId, mode, shipment, loading, isFileUploadSuccess } =
    useSelector(({ shipmentsReducer }) => shipmentsReducer);
  const { person, personProgram } = useSelector(
    ({ personReducer }) => personReducer
  );

  const [selectedFiles, setSelectedFiles] = useState(null);
  const [currentFile, setCurrentFile] = useState(null);
  const [progress, setProgress] = useState(0);
  const [message, setMessage] = useState("");
  const [fileInfos, setFileInfos] = useState([]);
  const [shipmentFileDbId, setShipmentFileDbId] = useState(null);
  const [createUploadId, setCreateUploadId] = useState(0);
  const inputRef = useRef();

  const columns = [
    {
      Header: "id",
      accessor: "shipmentFileDbId",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "path",
      accessor: "filePath",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="File Name" />
        </Typography>
      ),
      csvHeader: "File Name",
      accessor: "fileName",
      width: 350,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Uploaded By" />
        </Typography>
      ),
      csvHeader: "Uploaded By",
      accessor: "creator",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Date" />
        </Typography>
      ),
      csvHeader: "Date Uploaded",
      accessor: "creationTimestamp",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="File Type" />
        </Typography>
      ),
      csvHeader: "File Type",
      accessor: "fileType",
      hidden: true,
      width: 100,
    },
  ];

  async function fetch({ page, sort, filters }) {
    var sortData = "fileName:desc";

    var bodyFormData = new FormData();
    bodyFormData.append("shipmentDbId", JSON.stringify(shipmentDbId));

    if (filters.length > 0) {
      filters.forEach(function (item, index) {
        bodyFormData.append(item.col, item.val);
      });
    }
    if (sort.length > 0) {
      if (sort[0].mod == "DES") {
        sortData = sort[0].col + ":DESC";
      } else {
        sortData = sort[0].col + ":ASC";
      }
    }

    return new Promise((resolve, reject) => {
      try {
        cbClient
          .post(
            "shipment-files-search?page=" +
              page.number +
              "&limit=" +
              page.size +
              "&sort=" +
              sortData,
            bodyFormData
          )
          .then((response) => {
            resolve({
              pages: response.data.metadata.pagination.totalPages,
              data: response.data.result.data,
              elements: response.data.metadata.pagination.totalCount,
            });
          })
          .catch(({ message }) => {
            reject(message);
          });
      } catch (error) {
        console.log(error);
      }
    });
  }

  const toolbarActions = (selection, refresh) => {
    return (
      <div>
        <Box display="flex" justifyContent="center">
          <ToolBarUploadFile refresh={refresh} />
        </Box>
      </div>
    );
  };

  const rowActions = (rowData, refresh) => {
    let isSubmitDeleteVisible = false;

    if (rowData.status == "DRAFT") {
      isSubmitDeleteVisible = true;
    }

    return (
      <Box display="flex" flexDirection="row">
        <Box>
          <RowButtonDownload rowData={rowData} refresh={refresh} />
        </Box>
        <Box>
          <RowButtonDelete
            shipmentFileDbId={rowData.shipmentFileDbId}
            refresh={refresh}
          />
        </Box>
      </Box>
    );
  };

  return (
    /* 
     @prop data-testid: Id to use inside DocumentUpload.test.js file.
     */
    <div>
      <Paper >
        <div className="alert alert-light" role="alert">
          {message}
        </div>

        <EbsGrid
          id="DocumentUploadId"
          toolbar={true}
          columns={columns}
          title={
            <Typography variant="h8" color="primary">
              <FormattedMessage id="none" defaultMessage="Uploaded Files" />
            </Typography>
          }
          fetch={fetch}
          toolbaractions={toolbarActions}
          rowactions={rowActions}
          height="65vh"
          csvfilename="uploaded_document_list"
          indexing
          globalFilter={false}
        />
      </Paper>
    </div>
  );
});
// Type and required properties
DocumentUploadMolecule.propTypes = {};
// Default properties
DocumentUploadMolecule.defaultProps = {};

export default DocumentUploadMolecule;
