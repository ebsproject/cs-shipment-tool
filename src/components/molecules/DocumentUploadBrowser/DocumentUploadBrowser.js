import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import { Core } from "@ebs/styleguide";
const { Box, Typography } = Core;
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { EbsGrid } from "@ebs/components";
import { getShipmentsData } from "store/modules/ShipmentsReducer";
import ToolBarButtonImport from "components/atom/ShipmentItemBrowser/ShipmentItemBrowserButtonImport";
import RowButtonDelete from "components/atom/ShipmentDocumentBrowser/ShipmentDocumentBrowserButtonDelete";
import RowButtonDownload from "components/atom/ShipmentDocumentBrowser/ShipmentDocumentBrowserButtonDownload";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DocumentUploadBrowserMolecule = React.forwardRef(({}, ref) => {
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const dispatch = useDispatch();
  const { shipmentDbId, shipments, isFileUploadSuccess } = useSelector(
    ({ shipmentsReducer }) => shipmentsReducer
  );
  const { getState } = useStore();

  const columns = [
    {
      Header: "id",
      accessor: "shipmentFileDbId",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "path",
      accessor: "filePath",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="File Name" />
        </Typography>
      ),
      csvHeader: "File Name",
      accessor: "fileName",
      width: 350,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Uploaded By" />
        </Typography>
      ),
      csvHeader: "Uploaded By",
      accessor: "creator",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Date" />
        </Typography>
      ),
      csvHeader: "Date Uploaded",
      accessor: "creationTimestamp",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="File Type" />
        </Typography>
      ),
      csvHeader: "File Type",
      accessor: "fileType",
      hidden: true,
      width: 100,
    },
  ];

  async function fetch({ page, sort, filters }) {
    var bodyFormData = new FormData();
    bodyFormData.append("shipmentDbId", JSON.stringify(shipmentDbId));
    var sortData = "fileName:desc";

    if (filters.length > 0) {
      filters.forEach(function (item, index) {
        bodyFormData.append(item.col, item.val);
      });
    }

    if (sort.length > 0) {
      if (sort[0].mod == "DES") {
        sortData = sort[0].col + ":DESC";
      } else {
        sortData = sort[0].col + ":ASC";
      }
    }

    return new Promise((resolve, reject) => {
      try {
        cbClient
          .post(
            "shipment-files-search?page=" +
              page.number +
              "&limit=" +
              page.size +
              "&sort=" +
              sortData,
            bodyFormData
          )
          .then((response) => {
            resolve({
              pages: response.data.metadata.pagination.totalPages,
              data: response.data.result.data,
              elements: response.data.metadata.pagination.totalCount,
            });
            refresh();
          })
          .catch(({ message }) => {
            reject(message);
          });
      } catch (error) {
        console.log(error);
      }
    });
  }

  const toolbarActions = (selection, refresh) => {
    return (
      <Box display="flex" justifyContent="center">
        <Box>{<ToolBarButtonImport refresh={refresh} />}</Box>
      </Box>
    );
  };

  const rowActions = (rowData, refresh) => {
    let isSubmitDeleteVisible = false;

    if (rowData.status == "DRAFT") {
      isSubmitDeleteVisible = true;
    }

    return (
      <Box display="flex" flexDirection="row">
        <Box>
          <RowButtonDownload rowData={rowData} refresh={refresh} />
        </Box>
        <Box>
          <RowButtonDelete
            shipmentFileDbId={rowData.shipmentFileDbId}
            refresh={refresh}
          />
        </Box>
      </Box>
    );
  };

  return (
    /* 
     @prop data-testid: Id to use inside DocumentUploadBrowser.test.js file.
     */
    <div>
      <EbsGrid
        id="DocumentUploadBrowserId"
        toolbar={false}
        columns={columns}
        title={
          <Typography variant="h8" color="primary">
            <FormattedMessage id="none" defaultMessage="Uploaded Files" />
          </Typography>
        }
        fetch={fetch}
        rowactions={rowActions}
        height="65vh"
        globalFilter={false}
      />
    </div>
  );
});
// Type and required properties
DocumentUploadBrowserMolecule.propTypes = {};
// Default properties
DocumentUploadBrowserMolecule.defaultProps = {};

export default DocumentUploadBrowserMolecule;
