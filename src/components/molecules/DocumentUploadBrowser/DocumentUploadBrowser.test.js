  import DocumentUploadBrowser from './DocumentUploadBrowser';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('DocumentUploadBrowser is in the DOM', () => {
  render(<DocumentUploadBrowser></DocumentUploadBrowser>)
  expect(screen.getByTestId('DocumentUploadBrowserTestId')).toBeInTheDocument();
})
