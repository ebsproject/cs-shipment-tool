import { Core } from "@ebs/styleguide";
import { memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { hideMessage } from "store/modules/message";
const { Icon, IconButton, Snackbar, SnackbarContent, Typography, Alert } = Core;

const variantIcon = {
  success: "check_circle",
  warning: "warning",
  error: "error_outline",
  info: "info",
};

function FuseMessage(props) {
  const dispatch = useDispatch();
  const state = useSelector((store) => store.message.state);
  const options = useSelector((store) => store.message.options);
  const variants = {
    success: "text-white bg-green-600",
    error: "text-white bg-red-700",
    info: "text-white bg-blue-600",
    warning: "text-white bg-yellow-700",
  };

  return (
    <Snackbar
      {...options}
      open={state}
      onClose={() => dispatch(hideMessage())}
      ContentProps={{
        variant: "body2",
        headlineMapping: {
          body1: "div",
          body2: "div",
        },
      }}
    >
      <Alert
        onClose={() => dispatch(hideMessage())}
        severity={options.variant}
        variant="filled"
        sx={{ width: '100%' }}
      >
        <div className="flex items-center">
            <Typography className="mx-8">{options.message}</Typography>
          </div>
      </Alert>
    </Snackbar>
  );
}

export default memo(FuseMessage);
