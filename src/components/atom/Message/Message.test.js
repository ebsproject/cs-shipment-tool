  import Message from './Message';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Message is in the DOM', () => {
  render(<Message></Message>)
  expect(screen.getByTestId('MessageTestId')).toBeInTheDocument();
})
