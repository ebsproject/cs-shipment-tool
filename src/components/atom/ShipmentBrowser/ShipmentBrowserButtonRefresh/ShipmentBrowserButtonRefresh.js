import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
// CORE COMPONENTS
const { Icon, Dialog, IconButton, Tooltip, Button, Typography } = Core;
const { Refresh } = Icons;




//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonRefreshAtom = React.forwardRef(
  ({ rowData, refresh }, ref) => {

    const handleClickRefresh = () => {
      refresh();
    };
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonRefresh.test.js file.
     */
      <Tooltip title={<FormattedMessage id="none" defaultMessage="Refresh" />}>
        <IconButton
          color="primary"
          // className={classes.iconButton}
          className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
          onClick={handleClickRefresh}
          aria-controls="new-domain-menu"
          aria-haspopup="true"
          title="Refresh"
        >
          <Refresh />
        </IconButton>
      </Tooltip>
      // <Button
      //   variant="contained"
      //   aria-label="refresh-shipment"
      //   color="primary"
      //   startIcon={<Refresh className="fill-current text-white" />}
      //   onClick={handleClickRefresh}
      //   // disabled={rowSelected ? false : true}
      //   className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
      // >
      //   <Typography className="text-white">
      //     <FormattedMessage id="none" defaultMessage="Refresh" />
      //   </Typography>
      // </Button>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonRefreshAtom.propTypes = {};
// Default properties
ShipmentBrowserButtonRefreshAtom.defaultProps = {};

export default ShipmentBrowserButtonRefreshAtom;
