  import ShipmentBrowserButtonRefresh from './ShipmentBrowserButtonRefresh';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonRefresh is in the DOM', () => {
  render(<ShipmentBrowserButtonRefresh></ShipmentBrowserButtonRefresh>)
  expect(screen.getByTestId('ShipmentBrowserButtonRefreshTestId')).toBeInTheDocument();
})
