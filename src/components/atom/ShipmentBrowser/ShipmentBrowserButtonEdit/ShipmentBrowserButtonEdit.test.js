  import ShipmentBrowserButtonEdit from './ShipmentBrowserButtonEdit';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonEdit is in the DOM', () => {
  render(<ShipmentBrowserButtonEdit></ShipmentBrowserButtonEdit>)
  expect(screen.getByTestId('ShipmentBrowserButtonEditTestId')).toBeInTheDocument();
})
