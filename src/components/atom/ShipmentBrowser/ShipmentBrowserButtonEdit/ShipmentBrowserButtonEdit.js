import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { cbClient } from "utils/axios";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
const { Edit } = Icons;
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch, useStore } from "react-redux";
import {
  setShipmentDbId,
  setMode,
  findShipment,
  setShipment,
  setFormEditable,
} from "store/modules/ShipmentsReducer";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonEditAtom = React.forwardRef(
  ({ rowData, refresh, isFormEditable }, ref) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { getState } = useStore();

    const handleClickOpen = () => {
      findShipment(rowData.shipmentDbId)(dispatch, getState);
      dispatch(setShipmentDbId(rowData.shipmentDbId));
      dispatch(setMode("put"));
      dispatch(setFormEditable(isFormEditable));
      history.push("shipment-create");
    };
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonEdit.test.js file.
     */
      <Tooltip
        title={<FormattedMessage id="none" defaultMessage="Modify Shipment" />}
      >
        <IconButton
          onClick={handleClickOpen}
          aria-label="modify-shipment"
          color="primary"
          data-testid={"PutButton"}
          title="Update Shipment"
          // to={{
          //   // pathname: `/sh/shipment-create/${rowData.id}`,
          //   pathname: `/ba/shipment-create`,
          // }}
        >
          <Edit />
        </IconButton>
      </Tooltip>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonEditAtom.propTypes = {};
// Default properties
ShipmentBrowserButtonEditAtom.defaultProps = {};

export default ShipmentBrowserButtonEditAtom;
