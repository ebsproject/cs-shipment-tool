import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
// CORE COMPONENTS
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
  DialogActions,
} = Core;
const { Delete } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonDeleteAtom = React.forwardRef(
  ({ refresh, shipmentDbId }, ref) => {
    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const { getState } = useStore();
    const dispatch = useDispatch();

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);

    const submit = () => {
      try {
        cbClient
          .delete(`/shipments/${shipmentDbId}`)
          .then(function (response) {
            setSuccess(true);
            dispatch(
              showMessage({
                message: "Shipment Deleted sucessfully",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
            return;
          })
          .catch(function (error) {
            console.log(error);
          });
      } catch ({ message }) {
      } finally {
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonDelete.test.js file.
     */
      <div ref={ref} data-testid={"DeleteShipmentTestId"}>
        <Tooltip
          title={
            <FormattedMessage id="none" defaultMessage="Delete Shipment" />
          }
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="delete-shipment"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={
            <FormattedMessage id="none" defaultMessage="Delete Shipment" />
          }
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to delete this shipment permanently?"
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Delete" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonDeleteAtom.propTypes = {
  refresh: PropTypes.func.isRequired,
  shipmentDbId: PropTypes.number.isRequired,
};
// Default properties
ShipmentBrowserButtonDeleteAtom.defaultProps = {};

export default ShipmentBrowserButtonDeleteAtom;
