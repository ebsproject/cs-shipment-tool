  import ShipmentBrowserButtonDelete from './ShipmentBrowserButtonDelete';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonDelete is in the DOM', () => {
  render(<ShipmentBrowserButtonDelete></ShipmentBrowserButtonDelete>)
  expect(screen.getByTestId('ShipmentBrowserButtonDeleteTestId')).toBeInTheDocument();
})
