import React, {
  useState,
  useEffect,
  memo,
  Fragment,
  useCallback,
  useRef,
} from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { EbsDialog, Core, Icons, Lab, Styles } from "@ebs/styleguide";
import { useSelector, useDispatch, useStore } from "react-redux";
// CORE COMPONENTS
import { useForm, Controller } from "react-hook-form";
const { Autocomplete } = Lab;
import { cbClient, poClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { CSVLink, CSVDownload } from "react-csv";
const { PrintRounded } = Icons;

const {
  Button,
  Box,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  CircularProgress,
  Grid,
} = Core;


//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonPrintoutAtom = React.forwardRef(
  ({ refresh, selection, dataItem }, ref) => {

    const [open, setOpen] = useState(false);
    const [templateName, setTemplateName] = useState("");
    const [formatName, setFormatName] = useState("");
    const [shipmentName, setShipmentName] = useState("");
    const [shipmentDbId, setShipmentDbId] = useState("");
    const [fileName, setFileName] = useState("");
    const [printoutTemplateList, setPrintoutTemplateList] = useState({});
    const [templateFormats, setTemplateFormats] = useState({});
    const buttonImport = useRef();
    const [paramReport1, setParamReport1] = useState(false);

    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm();
    const dispatch = useDispatch();
    const { getState } = useStore();
    // const { shipmentDbId, mode, shipment, loading } = useSelector(
    //   ({ shipmentsReducer }) => shipmentsReducer
    // );
    // const { person } = useSelector(({ personReducer }) => personReducer);
    const [importStart, setImportStart] = React.useState(false);
    const [listSearchStart, setListSearchStart] = React.useState(false);
    const [transactionData, setTransactionData] = useState([]);
    const csvLink = useRef();

    // useEffect(() => {
    //   setShipmentId(shipmentDbId);
    // }, [open]);

    // useEffect(() => {
    //   var bodyFormData = new FormData();
    //   bodyFormData.append("type", "package");

    //   cbClient
    //     .post("lists-search?sort=creationTimestamp:DESC", bodyFormData)
    //     .then((response) => {
    //       setShipmentList(response.data.result.data);
    //       setListSearchStart(true);
    //     })
    //     .catch(({ message }) => {});
    // }, [open]);

    useEffect(() => {
      poClient
        .get(`api/report/getFormats`)
        .then((response) => {
          setTemplateFormats(response.data);
          setListSearchStart(true);
        })
        .catch(({ message }) => {});
    }, [printoutTemplateList]);

    useEffect(() => {
      poClient
        .get(`api/template/product/19`)
        .then((response) => {
          setPrintoutTemplateList(response.data.result.data);
          setListSearchStart(true);
        })
        .catch(({ message }) => {});
    }, [open]);

    const handleClick = (newValue) => {
      setNewValue(newValue.target.value);
    };

    // const handleSubmitClick = () => {
    //   console.log("SHIPMENT ID", selection[0].shipmentDbId);
    //   console.log("template", templateName);
    //   console.log("templateFormat", formatName);
    // };

    const actionButtonDownload = async () => {
      if (templateName == "" || formatName == "") {
        dispatch(
          showMessage({
            message: "Missing Input",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        return;
      }
      try {
        const formData = new FormData();
        setImportStart(true);

        formData.append("shipmentDbId", shipmentDbId);
        formData.append("template", templateName);
        formData.append("format", formatName);
        formData.append("filename", fileName);
        var filename = shipmentName + "_" + templateName + "." + formatName;
        var typeFormat = "";

        if (formatName == "csv") {
          typeFormat = "text/plain";
        } else if (formatName == "docx") {
          typeFormat =
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        } else if (formatName == "pdf") {
          typeFormat = "application/pdf";
        } else if (formatName == "xls" || formatName == "xlsx") {
          typeFormat = "application/vnd.ms-excel";
        } else if (formatName == "html") {
          typeFormat = "text/html";
        } else if (formatName == "txt") {
          typeFormat = "text/plain";
        }

        poClient
          .get(
            // `/api/FileManager/downloadDocument?file_name=${rowData.filePath}`,
            // `api/report/export?name=irri harvest tag&shipmentDbId=5234&format=csv&fileName=test.csv`,
            `api/report/export?name=${templateName}&format=${formatName}&parameter[shipmentDbId]=${shipmentDbId}&filename=${filename}`,
            {
              responseType: "blob", //blob
            }
          )
          .then((response) => {
            let link = document.createElement("a");
            link.download = filename;
            let blob = new Blob([response.data], { type: typeFormat });
            link.href = URL.createObjectURL(blob);
            link.click();
            URL.revokeObjectURL(link.href);
            setImportStart(false);
          })

          .catch(function (error) {
            dispatch(
              showMessage({
                message: error.toString(),
                variant: "error",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
          });
      } catch ({ message }) {
      } finally {
      }
    };

    function handleClose() {
      setOpen(false);
      refresh();
    }

    const handleClickOpen = () => {
      console.log("Selection", selection);
      reset();

      if (selection.length === 0) {
        console.log("length 0");
        dispatch(
          showMessage({
            message: "No Shipment Selected",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } else {
        setShipmentName(selection[0].shipmentName);
        setShipmentDbId(selection[0].shipmentDbId);
        console.log(selection[0].shipmentDbId);
        setOpen(true);
      }
    };

    const onSubmit = (data) => {
      event.preventDefault();
      // console.log("Onsubmit: ", data);
      // console.log("SHIPMENT ID", selection[0].id);

      // console.log("Template", templateName);
      // console.log("Template Format", formatName);
      // setImportStart(true);
      // buttonImport.current.enabled = false;
      // const bodyFormData = new FormData();
      // bodyFormData.append("listDbId", JSON.stringify(data.printoutTemplateList));

      // cbClient
      //   .post("shipments/" + `${shipmentId}` + "/import-list", bodyFormData)
      //   .then((response) => {
      //     buttonImport.current.enabled = true;
      //     setImportStart(false);
      //     setOpen(false);
      //     refresh();
      //     dispatch(
      //       showMessag({
      //         message: "Sucessfully importing the packages",
      //         variant: "success",
      //         anchorOrigin: {
      //           vertical: "top",
      //           horizontal: "right",
      //         },
      //       })
      //     );
      //     return;
      //   })
      //   .catch(({ message }) => {});
    };
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonPrintout.test.js file.
     */
      <div>
        <Tooltip
          title={
            <FormattedMessage id="none" defaultMessage="Download Report" />
          }
        >
          <IconButton
            // className={classes.button}
            onClick={handleClickOpen}
            aria-controls="new-domain-menu"
            aria-haspopup="true"
            // color="inherit"
            color="primary"
            // className={classes.iconButton}
            className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
            // component={Link}
            title="Print Reports"
          >
            <PrintRounded />
          </IconButton>
        </Tooltip>

        <EbsDialog
          open={open}
          handleClose={handleClose}
          title={
            <FormattedMessage
              id="none"
              defaultMessage="Select Report to Download"
            />
          }
        >
          <form onSubmit={handleSubmit(onSubmit)}>
            <DialogContent
              dividers
              className="grid grid-cols-1 sm:grid-cols-2 gap-2 md:gap-6"
            >
              <Typography className="text-ebs text-bold">
                <FormattedMessage id="none" defaultMessage={shipmentName} />
              </Typography>
              <Grid
                container
                spacing={2}
                direction="row"
                // justifyContent="flex-end"
              >
                <Grid item xs={8}>
                  <Controller
                    name="printoutTemplateList"
                    control={control}
                    render={({ field }) => (
                      <Autocomplete
                        {...field}
                        id="printoutTemplateListId"
                        data-testid={"printoutTemplateList-Id"}
                        options={printoutTemplateList}
                        onChange={(e, options) => {
                          setValue(
                            "templateName",
                            options.name ? options.name : ""
                          );
                          setTemplateName(options.name);
                          if (options.name == "test2") {
                            // setParamReport1(true);
                          } else {
                            // setParamReport1(false);
                          }
                        }}
                        disabled={!listSearchStart}
                        defaultValue={printoutTemplateList?.id}
                        getOptionLabel={(option) =>
                          option.name ? option.name : ""
                        }
                        required
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            // required
                            // error={errors["printoutTemplateList"] ? true : false}
                            // helperText={
                            //   errors["printoutTemplateList"] && (
                            //     <Typography className="text-red-600 text-ebs">
                            //       <FormattedMessage
                            //         id="none"
                            //         defaultMessage="Please select"
                            //       />
                            //     </Typography>
                            //   )
                            // }
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Printout Template Name"
                              />
                            }
                          />
                        )}
                      />
                    )}
                    // defaultValue={""}
                    rules={{ required: true }}
                  />
                  {!listSearchStart && <CircularProgress />}
                  {/* {importStart && <CircularProgress />} */}
                </Grid>
                <Grid item xs={4} spacing={3}>
                  <Grid container spacing={2}>
                    {paramReport1 && (
                      <div>
                        <Controller
                          name="startDate"
                          control={control}
                          render={({ field }) => (
                            <TextField
                              {...field}
                              type="date"
                              fullWidth="true"
                              InputLabelProps={{
                                shrink: true,
                              }}
                              label={
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="Start Date"
                                />
                              }
                              variant="outlined"
                              data-testid={"startDate"}
                            />
                          )}
                          // defaultValue={""}
                        />

                        <Controller
                          name="endDate"
                          control={control}
                          render={({ field }) => (
                            <TextField
                              {...field}
                              type="date"
                              fullWidth="true"
                              InputLabelProps={{
                                shrink: true,
                              }}
                              label={
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="End Date"
                                />
                              }
                              variant="outlined"
                              data-testid={"endDate"}
                            />
                          )}
                          // defaultValue={""}
                        />
                      </div>
                    )}
                  </Grid>
                </Grid>
                <Grid item xs={5} spacing={5}>
                  <Controller
                    name="templateFormats"
                    control={control}
                    render={({ field }) => (
                      <Autocomplete
                        {...field}
                        id="templateFormatsId"
                        data-testid={"templateFormats-Id"}
                        options={templateFormats}
                        onChange={(e, options) => {
                          setValue("formatName", options);
                          setFormatName(options);
                        }}
                        // disabled={!listSearchStart}
                        defaultValue={templateFormats?.id}
                        getOptionLabel={(option) => (option ? option : "")}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            required
                            // error={errors["formatName"] ? true : false}
                            // helperText={
                            //   errors["formatName"] && (
                            //     <Typography className="text-red-600 text-ebs">
                            //       <FormattedMessage
                            //         id="none"
                            //         defaultMessage="Please select format"
                            //       />
                            //     </Typography>
                            //   )
                            // }
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Format"
                              />
                            }
                          />
                        )}
                      />
                    )}
                    // defaultValue={""}
                    rules={{ required: true }}
                  />
                </Grid>
              </Grid>
            </DialogContent>
            <div align="right">
              <Button onClick={handleClose} disabled={importStart}>
                <FormattedMessage id="cancelId" defaultMessage="Cancel" />
              </Button>

              <Button type="submit" onClick={actionButtonDownload}>
                <FormattedMessage id="submitId" defaultMessage="Download" />
              </Button>
              {importStart && <CircularProgress />}

              {/* <CSVLink
                data={transactionData}
                filename={"shipment.csv"}
                ref={csvLink}
                target="_blank"
              ></CSVLink> */}
            </div>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonPrintoutAtom.propTypes = {};
// Default properties
ShipmentBrowserButtonPrintoutAtom.defaultProps = {};

export default ShipmentBrowserButtonPrintoutAtom;
