  import ShipmentBrowserButtonPrintout from './ShipmentBrowserButtonPrintout';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonPrintout is in the DOM', () => {
  render(<ShipmentBrowserButtonPrintout></ShipmentBrowserButtonPrintout>)
  expect(screen.getByTestId('ShipmentBrowserButtonPrintoutTestId')).toBeInTheDocument();
})
