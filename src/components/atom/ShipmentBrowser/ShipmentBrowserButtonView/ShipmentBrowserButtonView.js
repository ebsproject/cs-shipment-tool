import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { cbClient } from "utils/axios";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
const { Visibility } = Icons;
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch, useStore } from "react-redux";
import {
  setShipmentDbId,
  setMode,
  findShipment,
  setShipment,
  setFormEditable,
  setShipmentProgram,
} from "store/modules/ShipmentsReducer";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonViewAtom = React.forwardRef(
  ({ rowData, refresh, isFormEditable }, ref) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { getState } = useStore();
    const handleClickOpen = () => {
      findShipment(rowData.shipmentDbId)(dispatch, getState);
      dispatch(setShipmentDbId(rowData.shipmentDbId));
      dispatch(setMode("view"));
      dispatch(setFormEditable(isFormEditable));
      dispatch(setShipmentProgram(rowData.programCode));

      history.push("shipment-create");
    };
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonView.test.js file.
     */
      <Tooltip
        title={<FormattedMessage id="none" defaultMessage="View Shipment" />}
      >
        <IconButton
          onClick={handleClickOpen}
          aria-label="view-shipment"
          color="primary"
          // component={Link}
          // title="Edit"
          // //className={classes.processingThemedButton}
          // to={{
          //   // pathname: `/shipments/${rowData.id}`,
          //   pathname: `/shipment/${rowData.id}`,
          //   state: {
          //     currentData: rowData,
          //   },
          // }}
        >
          <Visibility />
        </IconButton>
      </Tooltip>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonViewAtom.propTypes = {};
// Default properties
ShipmentBrowserButtonViewAtom.defaultProps = {};

export default ShipmentBrowserButtonViewAtom;
