  import ShipmentBrowserButtonView from './ShipmentBrowserButtonView';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonView is in the DOM', () => {
  render(<ShipmentBrowserButtonView></ShipmentBrowserButtonView>)
  expect(screen.getByTestId('ShipmentBrowserButtonViewTestId')).toBeInTheDocument();
})
