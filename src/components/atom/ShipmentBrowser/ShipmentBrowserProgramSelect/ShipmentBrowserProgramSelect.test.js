  import ShipmentBrowserProgramSelect from './ShipmentBrowserProgramSelect';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserProgramSelect is in the DOM', () => {
  render(<ShipmentBrowserProgramSelect></ShipmentBrowserProgramSelect>)
  expect(screen.getByTestId('ShipmentBrowserProgramSelectTestId')).toBeInTheDocument();
})
