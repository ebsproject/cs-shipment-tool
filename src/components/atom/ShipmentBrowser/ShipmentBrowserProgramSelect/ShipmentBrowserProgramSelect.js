import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import RecipientTypeList from "utils/other/RecipientTypeService";
import { useSelector, useDispatch, useStore } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { EbsDialog, Core, Icons, Lab } from "@ebs/styleguide";
import { setCurrentUserProgramId } from "store/modules/PersonReducer";
const { Autocomplete } = Lab;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserProgramSelectAtom = React.forwardRef(
  ({ refresh, programList }, ref) => {
    const dispatch = useDispatch();
    const { getState } = useStore();
    const { shipmentDbId, mode, shipment, loading, isFormEditable } =
      useSelector(({ shipmentsReducer }) => shipmentsReducer);
    const { person, personProgram, isSHU, currentUserProgramId } = useSelector(
      ({ personReducer }) => personReducer
    );
    const [programListData, setProgramListData] = useState([
      { id: "", label: "" },
    ]);
    const [currentSelectedProgram, setCurrentSelectedProgram] = useState([
      { id: "", label: "" },
    ]);

    const { Grid, TextField } = Core;

    //form-control
    const {
      control,
      register,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm({
      mode: "onChange",
    });

    useEffect(() => {
      if (programList !== null) {
        setProgramListData(programList);
        setCurrentSelectedProgram(programList[0]);
        setValue("programList", programList[0]);
      }
    }, [programList]);

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserProgramSelect.test.js file.
     */
      <div>
        <form data-testid="basicInfoForm">
          <Grid container>
            <Grid item>
              <Controller
                control={control}
                name="personProgram"
                render={({ field: { onChange, value } }) => (
                  <Autocomplete
                    onChange={(event, options) => {
                      setValue("personProgram", options.label);
                      setCurrentSelectedProgram(options);
                      onChange(options);
                      dispatch(setCurrentUserProgramId(options.id));
                      refresh();
                      return;
                    }}
                    value={currentSelectedProgram}
                    options={programListData}
                    autoHighlight
                    getOptionLabel={(item) => (item.label ? item.label : "")}
                    getOptionSelected={(option, value) =>
                      value === undefined ||
                      value === "" ||
                      option.label === value.label
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Program"
                        variant="outlined"
                        error={!!errors.item}
                        size="normal"
                        fullWidth
                        helperText={errors.item && "item required"}
                      />
                    )}
                  />
                )}
              />
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
);
// Type and required properties
ShipmentBrowserProgramSelectAtom.propTypes = {};
// Default properties
ShipmentBrowserProgramSelectAtom.defaultProps = {};

export default ShipmentBrowserProgramSelectAtom;
