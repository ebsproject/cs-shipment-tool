  import ShipmentBrowserButtonProcess from './ShipmentBrowserButtonProcess';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonProcess is in the DOM', () => {
  render(<ShipmentBrowserButtonProcess></ShipmentBrowserButtonProcess>)
  expect(screen.getByTestId('ShipmentBrowserButtonProcessTestId')).toBeInTheDocument();
})
