import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { cbClient } from "utils/axios";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
import { useHistory } from "react-router-dom";
const { Publish } =Icons;
import { useSelector, useDispatch, useStore } from "react-redux";
import {
  setShipmentDbId,
  setMode,
  findShipment,
  setShipment,
  setFormEditable,
  setShipmentProgram,
} from "store/modules/ShipmentsReducer";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonProcessAtom = React.forwardRef(
  ({ rowData, refresh, isFormEditable }, ref) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { getState } = useStore();

    const handleClickOpen = () => {
      findShipment(rowData.shipmentDbId)(dispatch, getState);
      dispatch(setShipmentDbId(rowData.shipmentDbId));
      dispatch(setMode("put"));
      dispatch(setFormEditable(isFormEditable));
      dispatch(setShipmentProgram(rowData.programCode));
      history.push("shipment-create");
    };
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonProcess.test.js file.
     */
      <Tooltip
        title={<FormattedMessage id="none" defaultMessage="Process Shipment" />}
      >
        <IconButton
          onClick={handleClickOpen}
          aria-label="process-shipment"
          color="primary"
          title="Process Shipment"
        >
          <Publish />
        </IconButton>
      </Tooltip>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonProcessAtom.propTypes = {};
// Default properties
ShipmentBrowserButtonProcessAtom.defaultProps = {};

export default ShipmentBrowserButtonProcessAtom;
