  import ShipmentBrowserButtonSubmit from './ShipmentBrowserButtonSubmit';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonSubmit is in the DOM', () => {
  render(<ShipmentBrowserButtonSubmit></ShipmentBrowserButtonSubmit>)
  expect(screen.getByTestId('ShipmentBrowserButtonSubmitTestId')).toBeInTheDocument();
})
