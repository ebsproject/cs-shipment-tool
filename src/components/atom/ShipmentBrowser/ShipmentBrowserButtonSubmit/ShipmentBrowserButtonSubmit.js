import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
// CORE COMPONENTS
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
  DialogActions,
} = Core;
const { Publish } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonSubmitAtom = React.forwardRef(
  ({ rowData, refresh, shipmentDbId }, ref) => {
    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const { getState } = useStore();
    const dispatch = useDispatch();
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);

    const submit = () => {
      if (rowData.totalEntries == 0) {
        dispatch(
          showMessage({
            message: "Cannot Submit Shipment - No available shipment items",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        handleClose();
        refresh();
      } else {
        // check if there is 0 volume in shipment Item
        try {
          var bodyFormData = new FormData();
          bodyFormData.append("shipmentDbId", JSON.stringify(shipmentDbId));

          bodyFormData.append("shipmentItemWeight", JSON.stringify(0));

          try {
            cbClient
              .post("shipment-items-search", bodyFormData)
              .then(function (response) {
                if (response.data.result.data.length == 0) {
                  cbClient
                    .put(`/shipments/${shipmentDbId}`, {
                      shipmentStatus: "SUBMITTED",
                    })
                    .then(function (response) {
                      if (rowData.materialType != "non-propagative") {
                        try {
                          var bodyFormData = new FormData();
                          bodyFormData.append(
                            "shipmentDbId",
                            JSON.stringify(shipmentDbId)
                          );

                          bodyFormData.append(
                            "mtaStatus",
                            JSON.stringify(null)
                          );

                          try {
                            cbClient
                              .post("shipment-items-search", bodyFormData)
                              .then(function (response) {
                                if (response.data.result.data.length == 0) {
                                  cbClient
                                    .put(`/shipments/${shipmentDbId}`, {
                                      shipmentStatus: "SUBMITTED",
                                    })
                                    .then(function (response) {
                                      setSuccess(true);
                                      dispatch(
                                        showMessage({
                                          message:
                                            "Shipment submitted sucessfully",
                                          variant: "success",
                                          anchorOrigin: {
                                            vertical: "top",
                                            horizontal: "right",
                                          },
                                        })
                                      );
                                      return;
                                    })
                                    .catch(function (error) {
                                      console.log(error);
                                    });
                                } else {
                                  dispatch(
                                    showMessage({
                                      message:
                                        "Cannot Submit Shipment - Missing MTA Status in Shipment Item List",
                                      variant: "error",
                                      anchorOrigin: {
                                        vertical: "top",
                                        horizontal: "right",
                                      },
                                    })
                                  );
                                  handleClose();
                                }
                              })
                              .catch(function (error) {
                                console.log(error);
                              });
                          } catch ({ message }) {
                          } finally {
                          }
                        } catch ({ message }) {
                        } finally {
                        }
                      } else {
                        cbClient
                          .put(`/shipments/${shipmentDbId}`, {
                            shipmentStatus: "SUBMITTED",
                          })
                          .then(function (response) {
                            setSuccess(true);
                            dispatch(
                              showMessage({
                                message: "Shipment submitted sucessfully",
                                variant: "success",
                                anchorOrigin: {
                                  vertical: "top",
                                  horizontal: "right",
                                },
                              })
                            );
                            handleClose();
                            refresh();
                            return;
                          })
                          .catch(function (error) {
                            console.log(error);
                          });

                        return;
                      }
                    })
                    .catch(function (error) {
                      console.log(error);
                    });
                } else {
                  dispatch(
                    showMessage({
                      message:
                        "Cannot Submit Shipment - Incomplete Package Volume in Shipment Item List",
                      variant: "error",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                  handleClose();
                }
              })
              .catch(function (error) {
                console.log(error);
              });
          } catch ({ message }) {
          } finally {
          }
        } catch ({ message }) {
        } finally {
        }

        // end
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonSubmit.test.js file.
     */

      <div ref={ref} data-testid={"SubmitShipmentTestId"}>
        <Tooltip
          title={
            <FormattedMessage id="none" defaultMessage="Submit Shipment" />
          }
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="submit-shipment"
            color="primary"
          >
            <Publish />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={
            <FormattedMessage id="none" defaultMessage="Submit Shipment" />
          }
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Are you sure you want to submit this shipment?"
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Yes" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonSubmitAtom.propTypes = {};
// Default properties
ShipmentBrowserButtonSubmitAtom.defaultProps = {};

export default ShipmentBrowserButtonSubmitAtom;
