  import ShipmentBrowserButtonMessage from './ShipmentBrowserButtonMessage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowserButtonMessage is in the DOM', () => {
  render(<ShipmentBrowserButtonMessage></ShipmentBrowserButtonMessage>)
  expect(screen.getByTestId('ShipmentBrowserButtonMessageTestId')).toBeInTheDocument();
})
