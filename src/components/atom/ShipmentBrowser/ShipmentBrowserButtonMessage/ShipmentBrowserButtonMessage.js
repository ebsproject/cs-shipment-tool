import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
// CORE COMPONENTS
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { useForm, Controller } from "react-hook-form";
const {
  Box,
  Grid,
  Paper,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  FormLabel,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  Radio,
  RadioGroup,
} = Core;

const { Message } = Icons;



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserButtonMessageAtom = React.forwardRef(
  ({ refresh, rowData }, ref) => {

    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const initValue = null;
    const { getState } = useStore();
    const dispatch = useDispatch();
    const { person } = useSelector(({ personReducer }) => personReducer);
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [hasNote, setHasNote] = useState(false);
    const [oldNotes, setOldNotes] = useState(rowData.notes);
    const [allowUpdate, setAllowUpdate] = useState(true);
    const [disable, setDisable] = useState(true);
    const [noteValue, setNoteValue] = useState("");
    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm();
    useEffect(() => {
      if (rowData.notes === null) {
        setHasNote(false);
        setOldNotes("");
      } else {
        setHasNote(true);
      }

      if (rowData.shipmentStatus === "DONE") {
        setAllowUpdate(false);
      }
    }, [setHasNote, rowData]);

    const onSubmit = (data) => {
      event.preventDefault();
      const dtd = new Date();
      const addedNotes =
        oldNotes + "\n(" + person.personName + ":" + dtd + "):\n" + noteValue;

      try {
        cbClient
          .put("shipments/" + `${rowData.shipmentDbId}`, {
            notes: addedNotes,
          })
          .then(function (response) {
            setSuccess(true);
            refresh();
            handleClose();
            dispatch(
              showMessage({
                message: "Successfully Save Notes",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
          })
          .catch(function (error) {
            console.log(error);
          });
      } catch ({ message }) {
      } finally {
      }

      setOpen(false);
      setSuccess(true);
      return;
    };
    function handleChange(event) {
      setDisable(event.target.value === "");
      setNoteValue(event.target.value);
    }

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonMessage.test.js file.
     */
      <div ref={ref} data-testid={"ShipmentNotesTestId"}>
        <Tooltip title={<FormattedMessage id="none" defaultMessage="Notes" />}>
          <IconButton
            onClick={handleClickOpen}
            aria-label="package-note"
            color="primary"
          >
            <Message />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="Notes" />}
        >
          <form
            onSubmit={handleSubmit(onSubmit)}
            data-testid="AddShipmentNotes"
          >
            <DialogContent dividers>
              <Grid container spacing={3}>
                {hasNote && (
                  <Grid item xs={12}>
                    <TextField
                      fullWidth="true"
                      variant="standard"
                      multiline
                      rows={4}
                      label={
                        <FormattedMessage id="none" defaultMessage="History" />
                      }
                      value={rowData.notes}
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                  </Grid>
                )}
                {allowUpdate && (
                  <Grid item xs={12}>
                    <Controller
                      name="newNote"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          // value={initValue}
                          // required
                          fullWidth={true}
                          multiline
                          rows={4}
                          onChange={handleChange}
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Add Notes"
                            />
                          }
                          data-testid={"newNote"}
                        />
                      )}
                    />
                  </Grid>
                )}
              </Grid>
            </DialogContent>
            {allowUpdate && (
              <DialogActions>
                <Button onClick={handleClose}>
                  <FormattedMessage id="none" defaultMessage="Cancel" />
                </Button>
                <Button type="submit" disabled={disable}>
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
            )}
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentBrowserButtonMessageAtom.propTypes = {};
// Default properties
ShipmentBrowserButtonMessageAtom.defaultProps = {};

export default ShipmentBrowserButtonMessageAtom;
