import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Core, Styles, Icons } from "@ebs/styleguide";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
// import * as Icons from "@material-ui/icons";
const { PostAdd } = Icons;
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";


import { useSelector, useDispatch, useStore } from "react-redux";
import {
  setMode,
  setShipment,
  setFormEditable,
  setShipmentDbId,
} from "store/modules/ShipmentsReducer";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentCreateButtonAtom = React.forwardRef(
  ({ refresh, isFormEditable }, ref) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const { getState } = useStore();

    // const handleOpen = (newValue) => {
    //   setNewValue(newValue.target.value);
    // };

    const handleClickOpen = () => {
      dispatch(setMode("post"));
      dispatch(setShipment([]));
      dispatch(setFormEditable(isFormEditable));
      dispatch(setShipmentDbId(0));
      history.push("shipment-create");
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentCreateButton.test.js file.
     */
      // <Tooltip title={<FormattedMessage id="none" defaultMessage="New" />}>
      //   <IconButton
      //     // className={classes.button}
      //     color="primary"
      //     className={classes.iconButton}
      //     onClick={handleClickOpen}
      //     aria-controls="new-domain-menu"
      //     aria-haspopup="true"
      //     // color="inherit"
      //     // color="primary"
      //     // component={Link}
      //     title="Create Shipment"
      //     //className={classes.processingThemedButton}
      //     // to={{
      //     //   // pathname: `/sh/shipment-create/${rowData.id}`,
      //     //   pathname: `/ba/shipment-create`,
      //     // }}
      //   >
      //     <PostAdd />
      //   </IconButton>
      // </Tooltip>
      <Button
        variant="contained"
        aria-label="add-shipment"
        color="primary"
        startIcon={<PostAdd className="fill-current text-white" />}
        onClick={handleClickOpen}
        // disabled={rowSelected ? false : true}
        className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
      >
        <Typography className="text-white">
          <FormattedMessage id="none" defaultMessage="Create Shipment" />
        </Typography>
      </Button>
    );
  }
);
// Type and required properties
ShipmentCreateButtonAtom.propTypes = {};
// Default properties
ShipmentCreateButtonAtom.defaultProps = {};

export default ShipmentCreateButtonAtom;
