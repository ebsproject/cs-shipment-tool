  import ShipmentCreateButton from './ShipmentCreateButton';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentCreateButton is in the DOM', () => {
  render(<ShipmentCreateButton></ShipmentCreateButton>)
  expect(screen.getByTestId('ShipmentCreateButtonTestId')).toBeInTheDocument();
})
