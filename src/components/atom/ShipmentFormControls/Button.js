import React from "react";
import { Core, Styles } from "@ebs/styleguide";
const { Button, MuiButton } = Core;




export default function Button(props) {
  const { text, size, color, variant, onClick, ...other } = props;

  return (
    <MuiButton
      variant={variant || "contained"}
      size={size || "large"}
      color={color || "primary"}
      onClick={onClick}
      {...other}
     // classes={{ root: classes.root, label: classes.label }}
    >
      {text}
    </MuiButton>
  );
}
