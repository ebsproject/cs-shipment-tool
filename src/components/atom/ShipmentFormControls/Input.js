import React from "react";
import { Core } from "@ebs/styleguide";
const { TextField } = Core;

export default function Input(props) {
  const { name, label, value, error = null, line = 1, onChange } = props;
  return (
    <TextField
      //   variant="outlined"
      label={label}
      name={name}
      value={value}
      onChange={onChange}
      {...(error && { error: true, helperText: error })}
    />
  );
}
