import React from "react";
import { Core } from "@ebs/styleguide";
const { TextField } = Core;

export default function DatePicker(props) {
  const { name, label, value, error = null, onChange } = props;
  return (
    <TextField
      //   variant="outlined"
      label={label}
      name={name}
      value={value}
      onChange={onChange}
      type="date"
      defaultValue="2017-05-24"
      InputLabelProps={{
        shrink: true,
      }}
      {...(error && { error: true, helperText: error })}
    />
  );
}
