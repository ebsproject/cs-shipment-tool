import React from "react";
import { Core } from "@ebs/styleguide";
const { TextField } = Core;

export default function InputMultiLine(props) {
  const { name, label, value, error = null, line = 1, onChange } = props;
  return (
    <TextField
      label={label}
      name={name}
      value={value}
      onChange={onChange}
      multiline
      rows={3}
      {...(error && { error: true, helperText: error })}
    />
  );
}
