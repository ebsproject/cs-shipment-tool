import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { TextField, Typography } = Core;
import { postShipmentName } from "store/modules/TestReducer";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const TextFieldAtom = React.forwardRef(({ value }, ref) => {
  const [newValue, setNewValue] = useState(value);

  const handleChange = (newValue) => {
    setNewValue(newValue.target.value);
  };

  return (
    /* 
     @prop data-testid: Id to use inside TextField.test.js file.
     */
    <TextField
      data-testid={"TextFieldTestId"}
      ref={ref}
      value={newValue}
      onChange={handleChange}
    />
  );
});
// Type and required properties
TextFieldAtom.propTypes = {
  value: PropTypes.any,
};
// Default properties
TextFieldAtom.defaultProps = {
  value: "",
};

export default TextFieldAtom;
