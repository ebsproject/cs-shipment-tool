  import TextField from './TextField';
import React from 'react'
import Grid from './Grid'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('TextField is in the DOM', () => {
  render(<TextField></TextField>)
  expect(screen.getByTestId('TextFieldTestId')).toBeInTheDocument();
})
