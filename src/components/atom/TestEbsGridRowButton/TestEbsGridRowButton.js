import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
const { Edit } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const TestEbsGridRowButtonAtom = React.forwardRef(({}, ref) => {
  return (
    /* 
     @prop data-testid: Id to use inside TestEbsGridRowButton.test.js file.
     */
    <Tooltip
      title={<FormattedMessage id="none" defaultMessage="View Shipment" />}
    >
      <IconButton
        // onClick={handleClickOpen}
        aria-label="view-shipment"
        color="primary"
      >
        <Edit />
      </IconButton>
    </Tooltip>
  );
});
// Type and required properties
TestEbsGridRowButtonAtom.propTypes = {};
// Default properties
TestEbsGridRowButtonAtom.defaultProps = {};

export default TestEbsGridRowButtonAtom;
