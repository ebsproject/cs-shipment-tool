  import TestEbsGridRowButton from './TestEbsGridRowButton';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('TestEbsGridRowButton is in the DOM', () => {
  render(<TestEbsGridRowButton></TestEbsGridRowButton>)
  expect(screen.getByTestId('TestEbsGridRowButtonTestId')).toBeInTheDocument();
})
