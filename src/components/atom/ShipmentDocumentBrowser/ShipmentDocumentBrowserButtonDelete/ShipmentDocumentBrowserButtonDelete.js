import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
// CORE COMPONENTS
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
  DialogActions,
} = Core;
const { Delete } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentDocumentBrowserButtonDeleteAtom = React.forwardRef(
  ({ refresh, shipmentFileDbId }, ref) => {
    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const { getState } = useStore();
    const dispatch = useDispatch();

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);

    const submit = () => {
      try {
        cbClient
          .delete(`/shipment-files/${shipmentFileDbId}`)
          .then(function (response) {
            setSuccess(true);
            dispatch(
              showMessage({
                message: "File Successfully Deleted",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
            return;
          })
          .catch(function (error) {
            console.log(error);
          });
      } catch (error) {
      } finally {
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentDocumentBrowserButtonDelete.test.js file.
     */
      <div ref={ref} data-testid={"DeleteShipmentFileUploadTestId"}>
        <Tooltip
          title={<FormattedMessage id="none" defaultMessage="Delete File" />}
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="delete-shipment"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="Delete File" />}
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to delete this file permanently?"
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Delete" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentDocumentBrowserButtonDeleteAtom.propTypes = {};
// Default properties
ShipmentDocumentBrowserButtonDeleteAtom.defaultProps = {};

export default ShipmentDocumentBrowserButtonDeleteAtom;
