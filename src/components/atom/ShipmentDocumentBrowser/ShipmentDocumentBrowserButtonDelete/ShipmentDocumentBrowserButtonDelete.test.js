  import ShipmentDocumentBrowserButtonDelete from './ShipmentDocumentBrowserButtonDelete';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentDocumentBrowserButtonDelete is in the DOM', () => {
  render(<ShipmentDocumentBrowserButtonDelete></ShipmentDocumentBrowserButtonDelete>)
  expect(screen.getByTestId('ShipmentDocumentBrowserButtonDeleteTestId')).toBeInTheDocument();
})
