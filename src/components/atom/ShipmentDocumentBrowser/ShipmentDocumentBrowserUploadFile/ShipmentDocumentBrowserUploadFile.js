import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Box, Button, Typography } = Core;
import { cbClient, poClient } from "utils/axios";
import { useDispatch, useSelector, useStore } from "react-redux";
import {
  createShipmentFileUploadMetadata,
  uploadShipmentFile,
  setShipmentFileUploadSuccess,
} from "store/modules/ShipmentsReducer";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentDocumentBrowserUploadFileAtom = React.forwardRef(
  ({ refresh }, ref) => {
    const dispatch = useDispatch();
    const { getState } = useStore();
    const {
      shipmentDbId,
      mode,
      shipment,
      loading,
      isFileUploadSuccess,
      isFormEditable,
    } = useSelector(({ shipmentsReducer }) => shipmentsReducer);
    const { person, personProgram } = useSelector(
      ({ personReducer }) => personReducer
    );
    const [selectedFiles, setSelectedFiles] = useState(null);
    const [currentFile, setCurrentFile] = useState(null);
    const inputRef = useRef();
    const [progress, setProgress] = useState(0);
    const [createUploadId, setCreateUploadId] = useState(0);

    const selectFile = (event) => {
      setSelectedFiles(event.target.files);
    };

    useEffect(() => {
      if (isFormEditable) {
        setSelectedFiles("");
        inputRef.current.value = null;
      }
      refresh();
    }, [createUploadId]);

    async function upload() {
      event.preventDefault();

      if (selectedFiles !== null) {
        let currentFile = selectedFiles[0];

        let newFileName = shipmentDbId + "_" + currentFile.name;
        setCurrentFile(currentFile);

        const uploadResult = await uploadShipmentFile({
          newFileName: newFileName,
          currentFile: currentFile,
          personProgram: personProgram.programCode,
        })(dispatch, getState);

        const fileType = currentFile.type.split(".");

        const idCreated = await createShipmentFileUploadMetadata({
          shipmentDbId: shipmentDbId.toString(),
          fileName: currentFile.name,
          fileType: fileType[fileType.length - 1],
          fileSize: currentFile.size.toString(),
          filePath: personProgram.programCode + "-" + newFileName,
          creatorId: person.personDbId,
          modifierId: person.personDbId,
        })(dispatch, getState);

        setCreateUploadId(idCreated);
        dispatch(setShipmentFileUploadSuccess());
        refresh();
      }
    }

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentDocumentBrowserUploadFile.test.js file.
     */

      <Box>
        <div>
          <label className="btn btn-default">
            <input ref={inputRef} type="file" onChange={selectFile} />
          </label>

          <Button
            variant="contained"
            color="primary"
            // className="btn btn-success"
            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
            disabled={!selectedFiles}
            size="medium"
            onClick={upload}
          >
            Upload
          </Button>
        </div>
      </Box>
    );
  }
);
// Type and required properties
ShipmentDocumentBrowserUploadFileAtom.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
ShipmentDocumentBrowserUploadFileAtom.defaultProps = {};

export default ShipmentDocumentBrowserUploadFileAtom;
