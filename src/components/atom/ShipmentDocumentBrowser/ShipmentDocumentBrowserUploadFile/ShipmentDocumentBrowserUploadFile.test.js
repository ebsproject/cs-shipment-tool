  import ShipmentDocumentBrowserUploadFile from './ShipmentDocumentBrowserUploadFile';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentDocumentBrowserUploadFile is in the DOM', () => {
  render(<ShipmentDocumentBrowserUploadFile></ShipmentDocumentBrowserUploadFile>)
  expect(screen.getByTestId('ShipmentDocumentBrowserUploadFileTestId')).toBeInTheDocument();
})
