import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { poClient } from "utils/axios";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch, useStore } from "react-redux";

// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
const { CloudDownload } = Icons;
import { showMessage } from "store/modules/message";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentDocumentBrowserButtonDownloadAtom = React.forwardRef(
  ({ rowData }, ref) => {
    const dispatch = useDispatch();

    const actionButtonDownload = async () => {
      try {
        const formData = new FormData();
        formData.append("file_name", rowData.filePath);
        poClient
          .get(
            `/api/FileManager/downloadDocument?file_name=${rowData.filePath}`,
            {
              responseType: "blob",
            }
          )
          .then(function (response) {
            // console.log("Status" + response.status);
            // const link = document.createElement("a");
            // link.href = response;
            // link.download = rowData.filePath;
            // link.click();

            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", rowData.filePath); //
            document.body.appendChild(link);
            link.click();
          })
          .catch(function (error) {
            dispatch(
              showMessage({
                message: error.toString(),
                variant: "error",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
          });
      } catch ({ message }) {
      } finally {
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentDocumentBrowserButtonDownload.test.js file.
     */
      <Tooltip
        title={
          <FormattedMessage id="none" defaultMessage="Download Document" />
        }
      >
        <IconButton
          onClick={actionButtonDownload}
          aria-label="download-shipment-document"
          color="primary"
        >
          <CloudDownload />
        </IconButton>
      </Tooltip>
    );
  }
);
// Type and required properties
ShipmentDocumentBrowserButtonDownloadAtom.propTypes = {};
// Default properties
ShipmentDocumentBrowserButtonDownloadAtom.defaultProps = {};

export default ShipmentDocumentBrowserButtonDownloadAtom;
