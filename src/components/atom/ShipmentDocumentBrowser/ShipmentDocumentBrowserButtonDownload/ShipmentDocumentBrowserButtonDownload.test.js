  import ShipmentDocumentBrowserButtonDownload from './ShipmentDocumentBrowserButtonDownload';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentDocumentBrowserButtonDownload is in the DOM', () => {
  render(<ShipmentDocumentBrowserButtonDownload></ShipmentDocumentBrowserButtonDownload>)
  expect(screen.getByTestId('ShipmentDocumentBrowserButtonDownloadTestId')).toBeInTheDocument();
})
