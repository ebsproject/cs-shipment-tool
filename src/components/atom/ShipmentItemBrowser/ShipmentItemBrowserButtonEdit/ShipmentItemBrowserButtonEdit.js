import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
// CORE COMPONENTS
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { useForm, Controller } from "react-hook-form";
const {
  Box,
  Grid,
  Paper,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  FormLabel,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  Radio,
  RadioGroup,
} = Core;
const { Edit } = Icons;



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonEditAtom = React.forwardRef(
  ({ refresh, rowData }, ref) => {

    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const initValue = null;
    const { getState } = useStore();
    const dispatch = useDispatch();

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm();

    useEffect(() => {
      if (success) {
        refresh();
        handleClose();
      }
    }, [success]);

    const onSubmit = (data) => {
      event.preventDefault();
      var bodyFormData = new FormData();
      bodyFormData.append("packageCode", data.newPackageCode.toString());
      try {
        cbClient
          .post(
            "shipment-items/" +
              `${rowData.shipmentItemDbId}` +
              "/replace-package",
            bodyFormData
          )
          .then(function (response) {
            setSuccess(true);
            dispatch(
              showMessage({
                message: "Sucessfully replaced package",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
            return;
          })
          .catch(function (error) {
            console.log(error);
          });
      } catch ({ message }) {
      } finally {
      }

      setOpen(false);
      setSuccess(true);
      return;
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserTextFieldNewPackage.test.js file.
     */
      <div ref={ref} data-testid={"ReplacePackageTestId"}>
        <Tooltip
          title={
            <FormattedMessage id="none" defaultMessage="Replace Package" />
          }
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="package-update"
            color="primary"
          >
            <Edit />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={
            <FormattedMessage id="none" defaultMessage="Replace Package" />
          }
        >
          <form
            onSubmit={handleSubmit(onSubmit)}
            data-testid="ReplacePackageItem"
          >
            <DialogContent dividers>
              <Grid container spacing={2}>
                <Grid>Selected Package</Grid>
                <Grid>
                  <TextField
                    label="Germplasm Designation"
                    value={rowData.germplasmDesignation}
                  />
                  <TextField
                    label="Package Label"
                    value={rowData.packageLabel}
                  />
                  <TextField label="Seed Code" value={rowData.seedCode} />
                </Grid>
                <Grid item xs={6}>
                  <Controller
                    name="newPackageCode"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        value={initValue}
                        fullWidth={true}
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="Enter package code for replacement"
                          />
                        }
                        data-testid={"newPackageCode"}
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button onClick={handleClose}>
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Replace" />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonEditAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonEditAtom.defaultProps = {};

export default ShipmentItemBrowserButtonEditAtom;
