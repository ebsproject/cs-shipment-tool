  import ShipmentItemBrowserButtonEdit from './ShipmentItemBrowserButtonEdit';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonEdit is in the DOM', () => {
  render(<ShipmentItemBrowserButtonEdit></ShipmentItemBrowserButtonEdit>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonEditTestId')).toBeInTheDocument();
})
