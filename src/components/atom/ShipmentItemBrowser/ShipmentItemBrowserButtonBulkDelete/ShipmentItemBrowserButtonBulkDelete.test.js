  import ShipmentItemBrowserButtonBulkDelete from './ShipmentItemBrowserButtonBulkDelete';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonBulkDelete is in the DOM', () => {
  render(<ShipmentItemBrowserButtonBulkDelete></ShipmentItemBrowserButtonBulkDelete>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonBulkDeleteTestId')).toBeInTheDocument();
})
