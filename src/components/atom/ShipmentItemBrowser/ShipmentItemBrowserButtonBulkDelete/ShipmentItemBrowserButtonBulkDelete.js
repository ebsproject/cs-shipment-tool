import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
// CORE COMPONENTS
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { useForm, Controller } from "react-hook-form";
const {
  Grid,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  FormLabel,
  FormControlLabel,
  Radio,
  RadioGroup,
} = Core;
const { Delete } = Icons;


//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonBulkDeleteAtom = React.forwardRef(
  ({ refresh, selection, dataItem }, ref) => {

    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const [selectionOption, setSelectionOption] = useState("Selected");
    const { getState } = useStore();
    const dispatch = useDispatch();
    const { shipmentDbId } = useSelector(
      ({ shipmentsReducer }) => shipmentsReducer
    );

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm({
      defaultValues: {},
    });

    useEffect(() => {
      if (success) {
        refresh();
        handleClose();
      }
    }, [success]);

    const onSubmit = (data) => {
      event.preventDefault();

      if (data.selectedItem == "Selected") {
        if (selection.length > 0) {
          selection.forEach(function (value, index) {
            cbClient
              .delete(`/shipment-items/${value.shipmentItemDbId}`)
              .then(function (response) {
                setOpen(false);
                setSuccess(true);
                refresh();
                dispatch(
                  showMessage({
                    message: "Packages successfully deleted",
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
              })
              .catch(function (error) {
                console.log(error);
              });
          });
        } else {
          dispatch(
            showMessage({
              message: "No item selected",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        }
      } else {
        if (dataItem.length > 0) {
          var bodyFormData = new FormData();
          bodyFormData.append("field", "shipmentItemWeight");
          bodyFormData.append("fieldValue", data.volume);

          cbClient
            .delete(
              `/shipments/${shipmentDbId}/bulk-delete-items`,
              bodyFormData
            )
            .then(function (response) {
              setOpen(false);
              setSuccess(true);
              refresh();
              dispatch(
                showMessage({
                  message: "Packages successfully deleted",
                  variant: "success",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
            })
            .catch(function (error) {
              console.log(error);
            });
        } else {
          dispatch(
            showMessage({
              message: "No items to delete",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        }
      }
    };

    function handleClickSelection(event) {
      if (event.target.value === "Selected") {
        setSelectionOption("Selected");
      } else {
        setSelectionOption("All");
      }
    }

    useEffect(() => {
      setValue("selectedItem", selectionOption);
    }, [selectionOption]);

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonBulkUpdate.test.js file.
     */
      <div ref={ref} data-testid={"BulkUpdateShipmentTestId"}>
        <Tooltip
          title={
            <FormattedMessage
              id="none"
              defaultMessage="Shipment Item Bulk Delete"
            />
          }
        >
          <IconButton
            // className={classes.button}
            onClick={handleClickOpen}
            aria-label="bulk-update"
            // color="inherit"
            color="primary"
            // className={classes.iconButton}
            className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="Bulk Delete" />}
        >
          <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkDeleteForm">
            <DialogContent dividers>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <div align="center">
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Apply to</FormLabel>
                      <RadioGroup
                        aria-label="selectedItem"
                        name="selectedItem"
                        value={selectionOption}
                        row={true}
                      >
                        <FormControlLabel
                          value="Selected"
                          control={<Radio onClick={handleClickSelection} />}
                          label="Selected Item"
                        />
                        <FormControlLabel
                          value="All"
                          control={<Radio onClick={handleClickSelection} />}
                          label="All Items"
                        />
                      </RadioGroup>
                    </FormControl>
                  </div>
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button onClick={handleClose}>
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Apply" />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonBulkDeleteAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonBulkDeleteAtom.defaultProps = {};

export default ShipmentItemBrowserButtonBulkDeleteAtom;
