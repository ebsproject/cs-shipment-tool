import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { cbClient } from "utils/axios";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
const { Cancel } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonFailedAtom = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const handleClickFailed = () => {
      try {
        cbClient
          .put(`/shipment-items/${rowData.shipmentItemDbId}`, {
            shipmentItemStatus: "failed",
          })
          .then(function (response) {
            refresh();
            return;
          })
          .catch(function (error) {
            console.log(error);
          });
      } catch ({ message }) {
      } finally {
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonFailed.test.js file.
     */
      <Tooltip
        title={<FormattedMessage id="none" defaultMessage="Set Item Failed" />}
      >
        <IconButton
          // onClick={handleClickOpen}
          aria-label="failed-item-shipment"
          color="primary"
          onClick={handleClickFailed}
        >
          <Cancel />
        </IconButton>
      </Tooltip>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonFailedAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonFailedAtom.defaultProps = {};

export default ShipmentItemBrowserButtonFailedAtom;
