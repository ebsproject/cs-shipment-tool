  import ShipmentItemBrowserButtonFailed from './ShipmentItemBrowserButtonFailed';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonFailed is in the DOM', () => {
  render(<ShipmentItemBrowserButtonFailed></ShipmentItemBrowserButtonFailed>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonFailedTestId')).toBeInTheDocument();
})
