  import ShipmentItemBrowserButtonImport from './ShipmentItemBrowserButtonImport';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonImport is in the DOM', () => {
  render(<ShipmentItemBrowserButtonImport></ShipmentItemBrowserButtonImport>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonImportTestId')).toBeInTheDocument();
})
