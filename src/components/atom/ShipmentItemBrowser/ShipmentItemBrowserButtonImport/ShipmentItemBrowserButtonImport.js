import React, {
  useState,
  useEffect,
  memo,
  Fragment,
  useCallback,
  useRef,
} from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { EbsDialog, Core, Icons, Lab, Styles } from "@ebs/styleguide";
import { useSelector, useDispatch, useStore } from "react-redux";
// CORE COMPONENTS
import { useForm, Controller } from "react-hook-form";
const { Autocomplete } = Lab;
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
const { PostAdd } = Icons;

const {
  Button,
  Box,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  CircularProgress,
} = Core;



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonImportAtom = React.forwardRef(
  ({ refresh }, ref) => {

    const [open, setOpen] = useState(false);
    const [shipmentId, setShipmentId] = useState();
    const [shipmentList, setShipmentList] = useState({});
    const buttonImport = useRef();

    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm();
    const dispatch = useDispatch();
    const { getState } = useStore();
    const { shipmentDbId, mode, shipment, loading } = useSelector(
      ({ shipmentsReducer }) => shipmentsReducer
    );
    const { person } = useSelector(({ personReducer }) => personReducer);
    const [importStart, setImportStart] = React.useState(false);
    const [listSearchStart, setListSearchStart] = React.useState(false);

    useEffect(() => {
      setShipmentId(shipmentDbId);
    }, [open]);

    useEffect(() => {
      var bodyFormData = new FormData();
      bodyFormData.append("type", "package");

      cbClient
        .post("lists-search?sort=creationTimestamp:DESC", bodyFormData)
        .then((response) => {
          setShipmentList(response.data.result.data);
          setListSearchStart(true);
        })
        .catch(({ message }) => {});
    }, [open]);

    const handleClick = (newValue) => {
      setNewValue(newValue.target.value);
    };

    function handleClose() {
      setOpen(false);
      refresh();
    }

    const handleClickOpen = () => {
      reset();

      if (shipmentList.length == 0) {
        dispatch(
          showMessage({
            message: "No Package list to import",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } else {
        setOpen(true);
      }
    };

    const onSubmit = (data) => {
      event.preventDefault();
      setImportStart(true);
      buttonImport.current.enabled = false;
      const bodyFormData = new FormData();
      bodyFormData.append("listDbId", JSON.stringify(data.shipmentListId));

      cbClient
        .post("shipments/" + `${shipmentId}` + "/import-list", bodyFormData)
        .then((response) => {
          buttonImport.current.enabled = true;
          setImportStart(false);
          setOpen(false);
          refresh();
          dispatch(
            showMessag({
              message: "Sucessfully importing the packages",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          return;
        })
        .catch(({ message }) => {});
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonImport.test.js file.
     */
      <div data-testid={"PackageListTestId"} ref={ref}>
        <Tooltip
          title={
            <FormattedMessage id="none" defaultMessage="Import Package List" />
          }
        >
          <IconButton
            // className={classes.button}
            onClick={handleClickOpen}
            aria-controls="new-domain-menu"
            aria-haspopup="true"
            // color="inherit"
            color="primary"
            // className={classes.iconButton}
            className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
            // component={Link}
            title="Import Package List"
          >
            <PostAdd />
          </IconButton>
        </Tooltip>

        <EbsDialog
          open={open}
          handleClose={handleClose}
          title={
            <FormattedMessage
              id="none"
              defaultMessage="Select Shipment Package List"
            />
          }
        >
          <form
            onSubmit={handleSubmit(onSubmit)}
            data-testid="packageListImportForm"
          >
            <DialogContent
              dividers
              className="grid grid-cols-1 sm:grid-cols-2 gap-2 md:gap-6"
            >
              <Controller
                name="shipmentListId"
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    id="shipmentListId"
                    data-testid={"shipmentList-Id"}
                    options={shipmentList}
                    onChange={(e, options) =>
                      setValue("shipmentListId", options.listDbId)
                    }
                    disabled={!listSearchStart}
                    defaultValue={shipmentList?.listDbId}
                    getOptionLabel={(option) => option.abbrev}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={errors["shipmentListId"] ? true : false}
                        helperText={
                          errors["shipmentListId"] && (
                            <Typography className="text-red-600 text-ebs">
                              <FormattedMessage
                                id="none"
                                defaultMessage="Please select"
                              />
                            </Typography>
                          )
                        }
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="List Name"
                          />
                        }
                      />
                    )}
                  />
                )}
                // defaultValue={shipment?.recipientType}
                rules={{ required: true }}
              />
              {!listSearchStart && <CircularProgress />}
              {importStart && <CircularProgress />}
            </DialogContent>
            <div align="right">
              <Button onClick={handleClose} disabled={importStart}>
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Button>
              <Button ref={buttonImport} type="submit" disabled={importStart}>
                <FormattedMessage id="none" defaultMessage="Import" />
              </Button>
            </div>
          </form>
        </EbsDialog>
      </div>
    );
  }
);

// Type and required properties
ShipmentItemBrowserButtonImportAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonImportAtom.defaultProps = {};

export default ShipmentItemBrowserButtonImportAtom;
