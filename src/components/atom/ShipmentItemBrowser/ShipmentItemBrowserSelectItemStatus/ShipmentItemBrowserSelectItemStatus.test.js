  import ShipmentItemBrowserSelectItemStatus from './ShipmentItemBrowserSelectItemStatus';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserSelectItemStatus is in the DOM', () => {
  render(<ShipmentItemBrowserSelectItemStatus></ShipmentItemBrowserSelectItemStatus>)
  expect(screen.getByTestId('ShipmentItemBrowserSelectItemStatusTestId')).toBeInTheDocument();
})
