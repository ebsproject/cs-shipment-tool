import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Button, Typography, Select, MenuItem } = Core;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserSelectItemStatusAtom = React.forwardRef(
  ({ value }, ref) => {
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserSelectItemStatus.test.js file.
     */
      <div>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          label="Item Status"
          // onChange={handleChange}
        >
          <MenuItem value="passed">Passed</MenuItem>
          <MenuItem value="failed">Failed</MenuItem>
        </Select>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserSelectItemStatusAtom.propTypes = {};
// Default properties
ShipmentItemBrowserSelectItemStatusAtom.defaultProps = {};

export default ShipmentItemBrowserSelectItemStatusAtom;
