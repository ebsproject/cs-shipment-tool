  import ShipmentItemBrowserButtonOrderItemNumber from './ShipmentItemBrowserButtonOrderItemNumber';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonOrderItemNumber is in the DOM', () => {
  render(<ShipmentItemBrowserButtonOrderItemNumber></ShipmentItemBrowserButtonOrderItemNumber>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonOrderItemNumberTestId')).toBeInTheDocument();
})
