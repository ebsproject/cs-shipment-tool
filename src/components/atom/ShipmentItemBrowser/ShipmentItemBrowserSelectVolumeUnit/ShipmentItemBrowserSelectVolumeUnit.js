import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Select, MenuItem, Typography } = Core;
import { cbClient } from "utils/axios";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserSelectVolumeUnitAtom = React.forwardRef(
  ({ value, refresh }, ref) => {
    const [currentValue, setCurrentValue] = useState(
      value.values["packageUnit"]
    );
    const [newValue, setNewValue] = useState(value.values["packageUnit"]);
    const [rowDataId, setRowDataId] = useState();

    useEffect(() => {
      setNewValue(value.values["packageUnit"]);
      setRowDataId(value.values["shipmentItemDbId"]);
      setCurrentValue(value.values["packageUnit"]);
    }, [value]);

    useEffect(() => {
      const delayDebounceFn = setTimeout(() => {
        try {
          if (newValue != undefined && newValue != currentValue) {
            cbClient
              .put(`/shipment-items/${rowDataId}`, {
                packageUnit: newValue,
              })
              .then(function (response) {
                refresh;
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        } catch ({ message }) {
        } finally {
        }
      }, 2000);

      return () => clearTimeout(delayDebounceFn);
    }, [newValue]);

    function handleChangeValue(e) {
      setNewValue(e.target.value);
    }

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserSelectVolumeUnit.test.js file.
     */
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={newValue}
        label="Volume Unit"
        onChange={handleChangeValue}
        variant="outlined"
      >
        <MenuItem value="g">g</MenuItem>
        <MenuItem value="kg">kg</MenuItem>
        <MenuItem value="seed">seed</MenuItem>
      </Select>
    );
  }
);
// Type and required properties
ShipmentItemBrowserSelectVolumeUnitAtom.propTypes = {};
// Default properties
ShipmentItemBrowserSelectVolumeUnitAtom.defaultProps = {};

export default ShipmentItemBrowserSelectVolumeUnitAtom;
