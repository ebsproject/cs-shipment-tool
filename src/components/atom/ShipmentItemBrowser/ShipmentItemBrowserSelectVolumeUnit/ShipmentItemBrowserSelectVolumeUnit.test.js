  import ShipmentItemBrowserSelectVolumeUnit from './ShipmentItemBrowserSelectVolumeUnit';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserSelectVolumeUnit is in the DOM', () => {
  render(<ShipmentItemBrowserSelectVolumeUnit></ShipmentItemBrowserSelectVolumeUnit>)
  expect(screen.getByTestId('ShipmentItemBrowserSelectVolumeUnitTestId')).toBeInTheDocument();
})
