import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
import { cbClient } from "utils/axios";
const { CheckCircle } = Icons;
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonPassedAtom = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const [itemStatus, setItemStatus] = useState(null);

    const handleClickPassed = () => {
      setItemStatus("passed");
      try {
        cbClient
          .put(`/shipment-items/${rowData.shipmentItemDbId}`, {
            shipmentItemStatus: "passed",
          })
          .then(function (response) {
            refresh();
            return;
          })
          .catch(function (error) {
            console.log(error);
          });
      } catch ({ message }) {
      } finally {
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonPassed.test.js file.
     */
      <Tooltip
        title={<FormattedMessage id="none" defaultMessage="Set Item Passed" />}
      >
        <IconButton
          // onClick={handleClickOpen}
          aria-label="passed-item-shipment"
          color="primary"
          onClick={handleClickPassed}
        >
          <CheckCircle />
        </IconButton>
      </Tooltip>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonPassedAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonPassedAtom.defaultProps = {};

export default ShipmentItemBrowserButtonPassedAtom;
