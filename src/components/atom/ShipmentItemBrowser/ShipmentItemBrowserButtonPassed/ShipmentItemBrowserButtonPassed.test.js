  import ShipmentItemBrowserButtonPassed from './ShipmentItemBrowserButtonPassed';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonPassed is in the DOM', () => {
  render(<ShipmentItemBrowserButtonPassed></ShipmentItemBrowserButtonPassed>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonPassedTestId')).toBeInTheDocument();
})
