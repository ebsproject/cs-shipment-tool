import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
// CORE COMPONENTS
const { Message } = Icons;
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { useForm, Controller } from "react-hook-form";
const {
  Box,
  Grid,
  Paper,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  FormLabel,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  Radio,
  RadioGroup,
} = Core;



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonMessageAtom = React.forwardRef(
  ({ refresh, rowData }, ref) => {

    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const initValue = null;
    const { getState } = useStore();
    const dispatch = useDispatch();
    const { person } = useSelector(({ personReducer }) => personReducer);
    const { isFormEditable } = useSelector(
      ({ shipmentsReducer }) => shipmentsReducer
    );

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [hasNote, setHasNote] = useState(false);
    const [oldNotes, setOldNotes] = useState(rowData.notes);
    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm();

    useEffect(() => {
      if (rowData.notes !== undefined) {
        setHasNote(true);
      } else {
        setHasNote(false);
      }
    }, []);

    useEffect(() => {
      if (success) {
        refresh();
        handleClose();
      }
    }, [success]);

    const onSubmit = (data) => {
      event.preventDefault();
      const dtd = new Date();
      const addedNotes =
        oldNotes +
        "\n(" +
        person.personName +
        ":" +
        dtd +
        "):\n" +
        data.newNote;
      try {
        cbClient
          .put("shipment-items/" + `${rowData.shipmentItemDbId}`, {
            notes: addedNotes,
          })
          .then(function (response) {
            setSuccess(true);
            dispatch(
              showMessage({
                message: "Successfully Save Notes",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
            refresh();
          })
          .catch(function (error) {
            console.log(error);
          });
      } catch ({ message }) {
      } finally {
      }

      setOpen(false);
      setSuccess(true);
      return;
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonMessage.test.js file.
     */
      <div ref={ref} data-testid={"PackageNotesTestId"}>
        <Tooltip title={<FormattedMessage id="none" defaultMessage="Notes" />}>
          <IconButton
            onClick={handleClickOpen}
            aria-label="package-note"
            color="primary"
          >
            <Message />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="Add Notes" />}
        >
          <form onSubmit={handleSubmit(onSubmit)} data-testid="AddPackageNotes">
            <DialogContent dividers>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  {hasNote && (
                    <TextField
                      fullWidth="true"
                      variant="standard"
                      multiline
                      rows={4}
                      disabled
                      label={
                        <FormattedMessage id="none" defaultMessage="Notes" />
                      }
                      value={rowData.notes}
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                  )}
                </Grid>

                <Grid item xs={12}>
                  <Controller
                    name="newNote"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        value={initValue}
                        // required
                        fullWidth={true}
                        multiline
                        rows={4}
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="Add Notes"
                          />
                        }
                        data-testid={"newNote"}
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button onClick={handleClose}>
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Save" />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonMessageAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonMessageAtom.defaultProps = {};

export default ShipmentItemBrowserButtonMessageAtom;
