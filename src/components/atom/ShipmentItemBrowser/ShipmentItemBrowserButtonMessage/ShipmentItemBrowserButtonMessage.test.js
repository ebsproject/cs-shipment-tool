  import ShipmentItemBrowserButtonMessage from './ShipmentItemBrowserButtonMessage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonMessage is in the DOM', () => {
  render(<ShipmentItemBrowserButtonMessage></ShipmentItemBrowserButtonMessage>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonMessageTestId')).toBeInTheDocument();
})
