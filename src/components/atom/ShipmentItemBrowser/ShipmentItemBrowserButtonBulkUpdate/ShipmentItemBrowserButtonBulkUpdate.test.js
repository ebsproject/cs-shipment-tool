  import ShipmentItemBrowserButtonBulkUpdate from './ShipmentItemBrowserButtonBulkUpdate';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonBulkUpdate is in the DOM', () => {
  render(<ShipmentItemBrowserButtonBulkUpdate></ShipmentItemBrowserButtonBulkUpdate>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonBulkUpdateTestId')).toBeInTheDocument();
})
