import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
// CORE COMPONENTS
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { useForm, Controller } from "react-hook-form";
const {
  Box,
  Grid,
  Paper,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  FormLabel,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  Radio,
  RadioGroup,
} = Core;

const { DynamicFeedOutlined } = Icons;



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonBulkUpdateAtom = React.forwardRef(
  ({ refresh, selection, dataItem }, ref) => {

    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const initValue = null;
    const [selectionOption, setSelectionOption] = useState("Selected");
    const { getState } = useStore();
    const dispatch = useDispatch();
    const { shipmentDbId } = useSelector(
      ({ shipmentsReducer }) => shipmentsReducer
    );

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm({
      defaultValues: {
        volume: null,
        packageCount: null,
        rsht: null,
      },
    });

    useEffect(() => {
      if (success) {
        reset();
        refresh();
        handleClose();
      }
    }, [success]);

    const onSubmit = (data) => {
      event.preventDefault();

      if (data.selectedItem == "Selected") {
        if (selection.length > 0) {
          selection.forEach(function (value, index) {
            if (data.volume) {
              cbClient
                .put(`/shipment-items/${value.shipmentItemDbId}`, {
                  shipmentItemWeight: data.volume,
                })
                .then(function (response) {
                  setOpen(false);
                  setSuccess(true);
                  refresh();
                  dispatch(
                    showMessage({
                      message: "Bulk updating successfully",
                      variant: "success",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                })
                .catch(function (error) {
                  console.log(error);
                });
            }

            if (data.packageCount) {
              cbClient
                .put(`/shipment-items/${value.shipmentItemDbId}`, {
                  packageCount: data.packageCount,
                })
                .then(function (response) {
                  setOpen(false);
                  setSuccess(true);
                  refresh();
                  dispatch(
                    showMessage({
                      message: "Bulk updating successfully",
                      variant: "success",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                })
                .catch(function (error) {
                  console.log(error);
                });
            }

            if (data.rsht) {
              cbClient
                .put(`/shipment-items/${value.shipmentItemDbId}`, {
                  testCode: data.rsht,
                })
                .then(function (response) {
                  setOpen(false);
                  setSuccess(true);
                  refresh();
                  dispatch(
                    showMessage({
                      message: "Bulk updating successfully",
                      variant: "success",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                })
                .catch(function (error) {
                  console.log(error);
                });
            }
            if (data.packageUnit) {
              cbClient
                .put(`/shipment-items/${value.shipmentItemDbId}`, {
                  packageUnit: data.packageUnit,
                })
                .then(function (response) {
                  setOpen(false);
                  setSuccess(true);
                  refresh();
                  dispatch(
                    showMessage({
                      message: "Bulk updating successfully",
                      variant: "success",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                })
                .catch(function (error) {
                  console.log(error);
                });
            }
          });
        } else {
          dispatch(
            showMessage({
              message: "No item selected",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        }
      } else {
        if (dataItem.length > 0) {
          if (data.volume) {
            var bodyFormData = new FormData();
            bodyFormData.append("field", "shipmentItemWeight");
            bodyFormData.append("fieldValue", data.volume);

            cbClient
              .put(`/shipments/${shipmentDbId}/bulk-update-items`, bodyFormData)
              .then(function (response) {
                setOpen(false);
                setSuccess(true);
                refresh();
                dispatch(
                  showMessage({
                    message: "Bulk updating successfully",
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
              })
              .catch(function (error) {
                console.log(error);
              });
          }

          if (data.packageCount) {
            var bodyFormData = new FormData();
            bodyFormData.append("field", "packageCount");
            bodyFormData.append("fieldValue", data.packageCount);
            cbClient
              .put(`/shipments/${shipmentDbId}/bulk-update-items`, bodyFormData)
              .then(function (response) {
                setOpen(false);
                setSuccess(true);
                refresh();
                dispatch(
                  showMessage({
                    message: "Bulk updating successfully",
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
              })
              .catch(function (error) {
                console.log(error);
              });
          }

          if (data.rsht) {
            console.log("bulk update rsht");
            var bodyFormData = new FormData();
            bodyFormData.append("field", "testCode");
            bodyFormData.append("fieldValue", data.rsht);
            cbClient
              .put(`/shipments/${shipmentDbId}/bulk-update-items`, {
                field: "testCode",
                fieldValue: data.rsht,
              })
              .then(function (response) {
                setOpen(false);
                setSuccess(true);
                refresh();
                dispatch(
                  showMessage({
                    message: "Bulk updating successfully",
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
              })
              .catch(function (error) {
                console.log(error);
              });
          }

          if (data.packageUnit) {
            var bodyFormData = new FormData();
            bodyFormData.append("field", "packageUnit");
            bodyFormData.append("fieldValue", data.packageUnit);
            cbClient
              .put(`/shipments/${shipmentDbId}/bulk-update-items`, bodyFormData)
              .then(function (response) {
                setOpen(false);
                setSuccess(true);
                refresh();
                dispatch(
                  showMessage({
                    message: "Bulk updating successfully",
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        } else {
          dispatch(
            showMessage({
              message: "No items to update",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        }
      }
    };

    function handleClickSelection(event) {
      if (event.target.value === "Selected") {
        setSelectionOption("Selected");
      } else {
        setSelectionOption("All");
      }
    }

    useEffect(() => {
      setValue("selectedItem", selectionOption);
    }, [selectionOption]);
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonBulkUpdate.test.js file.
     */
      <div ref={ref} data-testid={"BulkUpdateShipmentTestId"}>
        <Tooltip
          title={
            <FormattedMessage
              id="none"
              defaultMessage="Shipment Item Bulk Update"
            />
          }
        >
          <IconButton
            // className={classes.button}
            onClick={handleClickOpen}
            aria-label="bulk-update"
            // color="inherit"
            color="primary"
            // className={classes.iconButton}
            className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
          >
            <DynamicFeedOutlined />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="Bulk Update" />}
        >
          <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkUpdateForm">
            <DialogContent dividers>
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <Controller
                    name="volume"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        // value={initValue}
                        variant="outlined"
                        type="number"
                        // required
                        label={
                          <FormattedMessage id="none" defaultMessage="Volume" />
                        }
                        data-testid={"volume"}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={3}>
                  <Controller
                    name="packageUnit"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        // value={initValue}
                        variant="outlined"
                        // required
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="Package Unit"
                          />
                        }
                        data-testid={"packageUnitId"}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={3}>
                  <Controller
                    name="packageCount"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        // required
                        // value={initValue}
                        variant="outlined"
                        type="number"
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="Package Count"
                          />
                        }
                        data-testid={"packageCount"}
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={3}>
                  <Controller
                    name="rsht"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        // value={initValue}
                        variant="outlined"
                        // required
                        label={
                          <FormattedMessage id="none" defaultMessage="RSHT" />
                        }
                        data-testid={"rshtId"}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <div align="center">
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Apply to</FormLabel>
                      <RadioGroup
                        aria-label="selectedItem"
                        name="selectedItem"
                        value={selectionOption}
                        row={true}
                      >
                        <FormControlLabel
                          value="Selected"
                          control={<Radio onClick={handleClickSelection} />}
                          label="Selected Item"
                        />
                        <FormControlLabel
                          value="All"
                          control={<Radio onClick={handleClickSelection} />}
                          label="All Items"
                        />
                      </RadioGroup>
                    </FormControl>
                  </div>
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button onClick={handleClose}>
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Apply" />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonBulkUpdateAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonBulkUpdateAtom.defaultProps = {};

export default ShipmentItemBrowserButtonBulkUpdateAtom;
