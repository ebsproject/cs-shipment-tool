import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { TextField, Typography } = Core;
import { cbClient } from "utils/axios";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserTextFieldRshtNumberAtom = React.forwardRef(
  ({ value, refresh }, ref) => {
    const [testCode, setTestCode] = useState();
    const [currentValue, setCurrentValue] = useState(value.values["testCode"]);
    const [newValue, setNewValue] = useState(value.values["testCode"]);
    const [rowDataId, setRowDataId] = useState();

    useEffect(() => {
      setNewValue(value.values["testCode"]);
      setRowDataId(value.values["shipmentItemDbId"]);
      setCurrentValue(value.values["testCode"]);
    }, [value]);

    useEffect(() => {
      const delayDebounceFn = setTimeout(() => {
        if (newValue != undefined && newValue != currentValue) {
          try {
            cbClient
              .put(`/shipment-items/${rowDataId}`, {
                testCode: newValue,
              })
              .then(function (response) {
                return;
              })
              .catch(function (error) {
                console.log(error);
              });
          } catch ({ message }) {
          } finally {
          }
        }
      }, 2000);

      return () => clearTimeout(delayDebounceFn);
    }, [newValue]);

    function handleChangeValue(e) {
      setNewValue(e.target.value);
    }

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserTextFieldRshtNumber.test.js file.
     */
      <TextField
        data-testid={"TextFieldRshtNumberId"}
        ref={ref}
        value={newValue}
        onChange={handleChangeValue}
        type="text"
        variant="outlined"
      />
    );
  }
);
// Type and required properties
ShipmentItemBrowserTextFieldRshtNumberAtom.propTypes = {};
// Default properties
ShipmentItemBrowserTextFieldRshtNumberAtom.defaultProps = {};

export default ShipmentItemBrowserTextFieldRshtNumberAtom;
