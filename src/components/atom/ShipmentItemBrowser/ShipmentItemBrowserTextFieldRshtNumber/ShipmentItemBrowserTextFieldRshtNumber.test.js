  import ShipmentItemBrowserTextFieldRshtNumber from './ShipmentItemBrowserTextFieldRshtNumber';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserTextFieldRshtNumber is in the DOM', () => {
  render(<ShipmentItemBrowserTextFieldRshtNumber></ShipmentItemBrowserTextFieldRshtNumber>)
  expect(screen.getByTestId('ShipmentItemBrowserTextFieldRshtNumberTestId')).toBeInTheDocument();
})
