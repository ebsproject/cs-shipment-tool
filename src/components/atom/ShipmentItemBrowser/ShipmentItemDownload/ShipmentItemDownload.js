import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { poClient } from "utils/axios";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch, useStore } from "react-redux";
import { CSVLink } from "react-csv";

// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
} = Core;
const { CloudDownload } = Icons;
import { showMessage } from "store/modules/message";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemDownloadAtom = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const [transactionData, setTransactionData] = useState([]);
  const csvLink = useRef(); //

  const actionButtonDownload = async () => {
    try {
      const formData = new FormData();
      formData.append("shipmentDbId", 5234);
      formData.append("template", "irri_shipment_items");
      formData.append("format", "csv");
      formData.append("filename", "test.csv");

      poClient
        .get(
          // `/api/FileManager/downloadDocument?file_name=${rowData.filePath}`,
          // `api/report/export?name=irri harvest tag&shipmentDbId=5234&format=csv&fileName=test.csv`,
          `api/report/export?name=irri_shipment_items&format=csv&parameter[shipmentDbId]=5234&filename=test.csv`,
          {
            responseType: "blob",
          }
        )
        .then(
          function (response) {
            setTransactionData(response);
            csvLink.filename = "test.csv";
            csvLink.current.link.click();
          }

            // .then(function (response) {
            //   // console.log("Status" + response.status);
            //   // const link = document.createElement("a");
            //   // link.href = response;
            //   // link.download = rowData.filePath;
            //   // link.click();
            //   const url = window.URL.createObjectURL(new Blob([response.data]));
            //   const link = document.createElement("a");
            //   link.href = url;
            //   link.setAttribute(
            //     "download",
            //     "https://ebs-cb.irri.org/index.php/printouts/default/download-report?template=irri harvest tag&shipmentDbId=5329&format=csv&fileName=test.csv"
            //   ); //

            //   https: document.body.appendChild(link);
            //   link.click();

            // })
            .catch(function (error) {
              dispatch(
                showMessage({
                  message: error.toString(),
                  variant: "error",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
            })
        );
    } catch ({ message }) {
    } finally {
    }
  };

  return (
    /* 
     @prop data-testid: Id to use inside ShipmentDocumentBrowserButtonDownload.test.js file.
     */
    <div>
      <Tooltip title={<FormattedMessage id="none" defaultMessage="Download" />}>
        <IconButton
          onClick={actionButtonDownload}
          aria-label="download-shipment-document"
          color="primary"
          className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
        >
          <CloudDownload />
        </IconButton>
      </Tooltip>
      <CSVLink
        data={transactionData}
        filename="shipment.csv"
        className="hidden"
        ref={csvLink}
        target="_blank"
      />
    </div>
  );
});

// Type and required properties
ShipmentItemDownloadAtom.propTypes = {};
// Default properties
ShipmentItemDownloadAtom.defaultProps = {};

export default ShipmentItemDownloadAtom;
