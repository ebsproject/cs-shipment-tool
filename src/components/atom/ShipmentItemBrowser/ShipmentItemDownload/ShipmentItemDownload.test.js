  import ShipmentItemDownload from './ShipmentItemDownload';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemDownload is in the DOM', () => {
  render(<ShipmentItemDownload></ShipmentItemDownload>)
  expect(screen.getByTestId('ShipmentItemDownloadTestId')).toBeInTheDocument();
})
