  import ShipmentItemBrowserButtonMtaUpdate from './ShipmentItemBrowserButtonMtaUpdate';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonMtaUpdate is in the DOM', () => {
  render(<ShipmentItemBrowserButtonMtaUpdate></ShipmentItemBrowserButtonMtaUpdate>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonMtaUpdateTestId')).toBeInTheDocument();
})
