import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
// CORE COMPONENTS
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { useForm, Controller } from "react-hook-form";
const {
  Grid,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  FormLabel,
  FormControlLabel,
  Radio,
  RadioGroup,
} = Core;
const { CheckCircle } = Icons;



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonMtaUpdateAtom = React.forwardRef(
  ({ refresh, selection, dataItem }, ref) => {

    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const [selectionOption, setSelectionOption] = useState("Selected");
    const { getState } = useStore();
    const dispatch = useDispatch();
    const { shipmentDbId } = useSelector(
      ({ shipmentsReducer }) => shipmentsReducer
    );
    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm({
      defaultValues: {},
    });

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const onSubmit = async () => {
      await cbClient
        .post(`/shipments/${shipmentDbId}/update-list`)
        .then(function (response) {
          setOpen(false);
          setSuccess(true);
          refresh();
          dispatch(
            showMessage({
              message: "MTA data successfully updated",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        })
        .catch(function (error) {
          console.log(error);
        });
      setOpen(false);
      refresh();
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonMtaUpdate.test.js file.
     */

      <div
        ref={ref}
        data-testid={"ShipmentItemBrowserButtonMtaUpdateTestIdemNumberId"}
      >
        <Tooltip
          title={<FormattedMessage id="none" defaultMessage="Update MTA" />}
        >
          <IconButton
            // className={classes.button}
            onClick={handleClickOpen}
            aria-label="bulk-update"
            // color="inherit"
            color="primary"
            // className={classes.iconButton}
            className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
          >
            <CheckCircle />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="Confirmation" />}
        >
          <form onSubmit={handleSubmit(onSubmit)} data-testid="ReOrderForm">
            <DialogContent dividers>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <div align="center">
                    <FormControl component="fieldset">
                      <FormLabel component="legend">
                        Are you sure you want to update mta data
                      </FormLabel>
                    </FormControl>
                  </div>
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button onClick={handleClose}>
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Apply" />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonMtaUpdateAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonMtaUpdateAtom.defaultProps = {};

export default ShipmentItemBrowserButtonMtaUpdateAtom;
