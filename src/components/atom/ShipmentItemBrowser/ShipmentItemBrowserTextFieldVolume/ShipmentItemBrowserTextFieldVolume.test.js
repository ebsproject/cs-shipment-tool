  import ShipmentItemBrowserTextFieldVolume from './ShipmentItemBrowserTextFieldVolume';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserTextFieldVolume is in the DOM', () => {
  render(<ShipmentItemBrowserTextFieldVolume></ShipmentItemBrowserTextFieldVolume>)
  expect(screen.getByTestId('ShipmentItemBrowserTextFieldVolumeTestId')).toBeInTheDocument();
})
