import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Styles} from "@ebs/styleguide";
const { TextField, Typography } = Core;
import { cbClient } from "utils/axios";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserTextFieldVolumeAtom = React.forwardRef(
  ({ value, refresh }, ref) => {
    const [currentValue, setCurrentValue] = useState(
      value.values["shipmentItemWeight"]
    );
    const [newValue, setNewValue] = useState(
      value.values["shipmentItemWeight"]
    );
    const [rowDataId, setRowDataId] = useState();

    useEffect(() => {
      setNewValue(value.values["shipmentItemWeight"]);
      setCurrentValue(value.values["shipmentItemWeight"]);
      setRowDataId(value.values["shipmentItemDbId"]);
    }, [value]);

    useEffect(() => {
      const delayDebounceFn = setTimeout(() => {
        try {
          if (newValue != undefined && newValue != currentValue) {
            cbClient
              .put(`/shipment-items/${rowDataId}`, {
                shipmentItemWeight: newValue,
              })
              .then(function (response) {
                return;
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        } catch ({ message }) {
        } finally {
        }
      }, 2000);

      return () => clearTimeout(delayDebounceFn);
    }, [newValue]);

    function handleChangeValue(e) {
      setNewValue(e.target.value);
    }

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserTextFieldVolume.test.js file.
     */
      <TextField
        data-testid={"TextFieldVolumeId"}
        ref={ref}
        onChange={handleChangeValue}
        value={newValue}
        variant="outlined"
      />
    );
  }
);
// Type and required properties
ShipmentItemBrowserTextFieldVolumeAtom.propTypes = {};
// Default properties
ShipmentItemBrowserTextFieldVolumeAtom.defaultProps = {};

export default ShipmentItemBrowserTextFieldVolumeAtom;
