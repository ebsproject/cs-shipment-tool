import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
// CORE COMPONENTS
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
  DialogActions,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Grid,
} = Core;
const { Delete } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonDeleteAtom = React.forwardRef(
  ({ refresh, shipmentItemDbId }, ref) => {
    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const [deleteOption, setDeleteOption] = useState("markDelete");
    const { getState } = useStore();
    const dispatch = useDispatch();

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);

    function handleClickDeleteOption(event) {
      if (event.target.value === "markDelete") {
        setDeleteOption("markDelete");
      } else {
        setDeleteOption("delete");
      }
    }

    const submit = () => {
      try {
        if (deleteOption == "delete") {
          cbClient
            .delete(`/shipment-items/${shipmentItemDbId}`)
            .then(function (response) {
              setSuccess(true);
              dispatch(
                showMessage({
                  message: "Shipment Item Deleted sucessfully",
                  variant: "success",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
              return;
            })
            .catch(function (error) {
              console.log(error);
            });
        } else {
          cbClient
            .put(`/shipment-items/${shipmentItemDbId}`, {
              shipmentItemStatus: "cancel",
            })
            .then(function (response) {
              setSuccess(true);
              dispatch(
                showMessage({
                  message: "Shipment Item Deleted sucessfully",
                  variant: "success",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
              return;
            })
            .catch(function (error) {
              console.log(error);
            });
        }
      } catch ({ message }) {
      } finally {
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonDelete.test.js file.
     */
      <div ref={ref} data-testid={"DeleteShipmentItemTestId"}>
        <Tooltip
          title={
            <FormattedMessage id="none" defaultMessage="Delete Shipment Item" />
          }
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="delete-shipment-item"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={
            <FormattedMessage id="none" defaultMessage="Delete Shipment Item" />
          }
        >
          {/* <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to delete this shipment item permanently?"
              />
            </Typography>
          </DialogContent> */}
          <div align="center">
            <Grid container direction="row" spacing={2}>
              <Grid item xs={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Options</FormLabel>
                  <RadioGroup
                    arial-label="shipmentTypeLabel"
                    name="shipmentItemDeleteOption"
                    value={deleteOption}
                    // onChange={handleChange}
                  >
                    <FormControlLabel
                      value="markDelete"
                      control={<Radio onClick={handleClickDeleteOption} />}
                      label="Mark item only as cancel"
                    />
                    <FormControlLabel
                      value="delete"
                      control={<Radio onClick={handleClickDeleteOption} />}
                      label="Delete item permanently"
                    />
                  </RadioGroup>
                </FormControl>
              </Grid>
            </Grid>
          </div>

          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Delete" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonDeleteAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonDeleteAtom.defaultProps = {};

export default ShipmentItemBrowserButtonDeleteAtom;
