  import ShipmentItemBrowserButtonDelete from './ShipmentItemBrowserButtonDelete';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonDelete is in the DOM', () => {
  render(<ShipmentItemBrowserButtonDelete></ShipmentItemBrowserButtonDelete>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonDeleteTestId')).toBeInTheDocument();
})
