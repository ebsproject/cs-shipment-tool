import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons, Styles } from "@ebs/styleguide";
// CORE COMPONENTS
import { useSelector, useDispatch, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { useForm, Controller } from "react-hook-form";
import MlsAncestors from "components/molecules/MlsAncestorsBrowser";
const {
  Box,
  Grid,
  Paper,
  Button,
  IconButton,
  DialogActions,
  DialogContent,
  Tooltip,
  Typography,
} = Core;
const { ViewComfy } = Icons;


//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserButtonViewMlsAncestorsAtom = React.forwardRef(
  ({ refresh }, ref) => {

    const [open, setOpen] = useState(false);
    const [success, setSuccess] = useState(false);
    const [deleteOption, setDeleteOption] = useState("markDelete");
    const { getState } = useStore();
    const dispatch = useDispatch();

    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserButtonViewMlsAncestors.test.js file.
     */
      <div ref={ref} data-testid={"ViewMlsAncestorsTestId"}>
        <Tooltip
          title={
            <FormattedMessage id="none" defaultMessage="View MLS Ancestors" />
          }
        >
          <IconButton
            // className={classes.button}
            onClick={handleClickOpen}
            aria-label="view-mls"
            color="primary"
            // color="primary"
            // className={classes.iconButton}
            className="ebs-MuiButtonBase-root ebs-MuiButton-root bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
          >
            <ViewComfy />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          // maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="MLS Ancestors" />}
        >
          <Paper >
            <Grid container spacing={3}>
              <MlsAncestors />
            </Grid>
          </Paper>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ShipmentItemBrowserButtonViewMlsAncestorsAtom.propTypes = {};
// Default properties
ShipmentItemBrowserButtonViewMlsAncestorsAtom.defaultProps = {};

export default ShipmentItemBrowserButtonViewMlsAncestorsAtom;
