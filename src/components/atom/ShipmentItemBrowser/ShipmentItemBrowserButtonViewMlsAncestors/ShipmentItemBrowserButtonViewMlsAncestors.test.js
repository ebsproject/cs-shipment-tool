  import ShipmentItemBrowserButtonViewMlsAncestors from './ShipmentItemBrowserButtonViewMlsAncestors';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserButtonViewMlsAncestors is in the DOM', () => {
  render(<ShipmentItemBrowserButtonViewMlsAncestors></ShipmentItemBrowserButtonViewMlsAncestors>)
  expect(screen.getByTestId('ShipmentItemBrowserButtonViewMlsAncestorsTestId')).toBeInTheDocument();
})
