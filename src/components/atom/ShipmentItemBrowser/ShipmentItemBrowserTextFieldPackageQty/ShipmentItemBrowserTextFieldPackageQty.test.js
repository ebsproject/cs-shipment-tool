  import ShipmentItemBrowserTextFieldPackageQty from './ShipmentItemBrowserTextFieldPackageQty';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserTextFieldPackageQty is in the DOM', () => {
  render(<ShipmentItemBrowserTextFieldPackageQty></ShipmentItemBrowserTextFieldPackageQty>)
  expect(screen.getByTestId('ShipmentItemBrowserTextFieldPackageQtyTestId')).toBeInTheDocument();
})
