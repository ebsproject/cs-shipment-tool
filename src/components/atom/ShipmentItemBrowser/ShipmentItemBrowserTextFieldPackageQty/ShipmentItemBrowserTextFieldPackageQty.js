import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { TextField, Typography } = Core;
import { cbClient } from "utils/axios";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserTextFieldPackageQtyAtom = React.forwardRef(
  ({ value, refresh }, ref) => {
    const [currentValue, setCurrentValue] = useState(
      value.values["packageCount"]
    );
    const [newValue, setNewValue] = useState(value.values["packageCount"]);
    const [rowDataId, setRowDataId] = useState();

    useEffect(() => {
      setNewValue(value.values["packageCount"]);
      setRowDataId(value.values["shipmentItemDbId"]);
      setCurrentValue(value.values["packageCount"]);
    }, [value]);

    useEffect(() => {
      const delayDebounceFn = setTimeout(() => {
        // Send Axios request here
        if (newValue != undefined && newValue != currentValue) {
          try {
            cbClient
              .put(`/shipment-items/${rowDataId}`, {
                // .put(`/shipment-items/1`, {
                packageCount: newValue,
              })
              .then(function (response) {
                return;
              })
              .catch(function (error) {
                console.log(error);
              });
          } catch ({ message }) {
          } finally {
          }
        }
        //end axios request
      }, 2000);

      return () => clearTimeout(delayDebounceFn);
    }, [newValue]);

    function handleChangeValue(e) {
      setNewValue(e.target.value);
      // setCurrentValue(e.target.value);
    }

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserTextFieldPackageQty.test.js file.
     */
      <TextField
        data-testid={"TextFieldPackageQtyId"}
        ref={ref}
        value={newValue}
        autoComplete="off"
        onChange={handleChangeValue}
        variant="outlined"
      />
    );
  }
);
// Type and required properties
ShipmentItemBrowserTextFieldPackageQtyAtom.propTypes = {};
// Default properties
ShipmentItemBrowserTextFieldPackageQtyAtom.defaultProps = {};

export default ShipmentItemBrowserTextFieldPackageQtyAtom;
