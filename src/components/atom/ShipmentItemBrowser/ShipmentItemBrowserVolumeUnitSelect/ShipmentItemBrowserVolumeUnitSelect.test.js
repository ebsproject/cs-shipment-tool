  import ShipmentItemBrowserVolumeUnitSelect from './ShipmentItemBrowserVolumeUnitSelect';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentItemBrowserVolumeUnitSelect is in the DOM', () => {
  render(<ShipmentItemBrowserVolumeUnitSelect></ShipmentItemBrowserVolumeUnitSelect>)
  expect(screen.getByTestId('ShipmentItemBrowserVolumeUnitSelectTestId')).toBeInTheDocument();
})
