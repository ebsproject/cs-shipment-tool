import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Select, MenuItem, Typography } = Core;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ShipmentItemBrowserVolumeUnitSelectAtom = React.forwardRef(
  ({ value }, ref) => {
    const [newValue, setNewValue] = useState(value);

    const handleChange = (newValue) => {
      setNewValue(newValue.target.value);
    };
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserVolumeUnitSelect.test.js file.
     */
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        label="Volume Unit"
        onChange={handleChange}
      >
        <MenuItem value="g">g</MenuItem>
        <MenuItem value="kg">kg</MenuItem>
      </Select>
    );
  }
);
// Type and required properties
ShipmentItemBrowserVolumeUnitSelectAtom.propTypes = {};
// Default properties
ShipmentItemBrowserVolumeUnitSelectAtom.defaultProps = {};

export default ShipmentItemBrowserVolumeUnitSelectAtom;
