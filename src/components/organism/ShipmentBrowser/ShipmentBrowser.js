import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import { Core } from "@ebs/styleguide";
const { Box, Typography, LinearProgress, Chip } = Core;
import { useDispatch, useSelector, useStore } from "react-redux";
import { cbClient } from "utils/axios";
import { EbsGrid } from "@ebs/components";
import { getShipmentsData } from "store/modules/ShipmentsReducer";
import {
  setPerson,
  setPersonProgram,
  getPerson,
  setIsSHU,
  setCurrentUserProgramId,
  setPersonProgramList,
} from "store/modules/PersonReducer";
import RowButtonDelete from "components/atom/ShipmentBrowser/ShipmentBrowserButtonDelete";
import RowButtonMessage from "components/atom/ShipmentBrowser/ShipmentBrowserButtonMessage";
import RowButtonEdit from "components/atom/ShipmentBrowser/ShipmentBrowserButtonEdit";
import RowButtonView from "components/atom/ShipmentBrowser/ShipmentBrowserButtonView";
import RowButtonSubmit from "components/atom/ShipmentBrowser/ShipmentBrowserButtonSubmit";
import CreateButton from "components/atom/ShipmentBrowser/ShipmentCreateButton";
import PrintButton from "components/atom/ShipmentBrowser/ShipmentBrowserButtonPrintout";
import RefreshButton from "components/atom/ShipmentBrowser/ShipmentBrowserButtonRefresh";
import ProcessButton from "components/atom/ShipmentBrowser/ShipmentBrowserButtonProcess";
import ToolbarSelectProgram from "components/atom/ShipmentBrowser/ShipmentBrowserProgramSelect";
// import { getAuthState, getContext, getDomainContext } from "@ebs/cs";
import { getAuthState, getContext, getDomainContext } from "@ebs/layout";
import Cookies from "js-cookie";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ShipmentBrowserOrganism = React.forwardRef(({}, ref) => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);
  const [isLoadingPersonComplete, setIsLoadingPersonComplete] = useState(false);
  const dispatch = useDispatch();
  const { getState } = useStore();
  const gridRef = React.useRef();
  const { shipments } = useSelector(({ shipmentsReducer }) => shipmentsReducer);
  const {
    person : _person,
    personProgram,
    isSHU,
    currentUserProgramId,
    personProgramList,
  } = useSelector(({ personReducer }) => personReducer);

  const autState = getAuthState();
  const domainContext = getDomainContext();
  const context = getContext();
  const programList = [];
  const [programId, setProgramId] = useState(null);
  const [programIdsValue, setProgramIdsValue] = useState(null);

  useEffect(() => {
    const email = autState["http://wso2.org/claims/emailaddress"];

    try {
      cbClient
        .post(`/persons-search`, {
          email: email,
        })
        .then(function (response) {
          if (response.data.result.data[0] != null) {
            dispatch(setPerson(response.data.result.data[0]));
          } else {
          }
          return;
        })
        .catch(function (error) {
          console.log(error);
        });
    } catch ({ message }) {
    } finally {
    }
  }, []);

  useEffect(() => {
    try {
      let person = {..._person}
      if (person.personDbId !== undefined) {
        cbClient
          .get(`/persons/${person.personDbId}/programs`)
          .then(function (response) {
            let currentProgramCode = response.data.result.data[0].programCode;
            if (currentProgramCode != "SHU") {
              dispatch(setIsSHU(false));
            } else {
              dispatch(setIsSHU(true));
            }

          

            var programIds = "";
            response.data.result.data.forEach(function (item, index) {
              programIds = programIds + item.programDbId + "|";
              programList.push({
                id: item.programDbId,
                label: item.programCode,
              });
            });
            dispatch(setPersonProgramList(programList));
            setProgramIdsValue(programIds);

            dispatch(
              setCurrentUserProgramId(response.data.result.data[0].programDbId)
            );
            setProgramId(response.data.result.data[0].programDbId);
            dispatch(setPersonProgram(response.data.result.data[0]));
            setIsLoadingPersonComplete(true);

            return;
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    } catch ({ message }) {
    } finally {
    }
  }, [_person]);

  useEffect(() => {
    if (currentUserProgramId) {
      dispatch(setCurrentUserProgramId(currentUserProgramId));
      setProgramId(currentUserProgramId);
    }
  }, [currentUserProgramId]);

  async function fetch({ page, sort, filters }) {
    var bodyFormData = new FormData();
    var sortData = "shipmentDbId:desc";

    if (isSHU) {
      bodyFormData.append(
        "shipmentStatus",
        "SUBMITTED|PROCESSING|DONE|ON-HOLD"
      );
    } else {
      bodyFormData.append("programDbId", programIdsValue);
    }
    if (filters.length > 0) {
      filters.forEach(function (item, index) {
        bodyFormData.append(item.col, item.val);
      });
    }

    if (sort.length > 0) {
      if (sort[0].mod == "DES") {
        sortData = sort[0].col + ":DESC";
      } else {
        sortData = sort[0].col + ":ASC";
      }
    }

    return new Promise((resolve, reject) => {
      try {
        cbClient
          .post(
            "shipments-search?page=" +
              page.number +
              "&limit=" +
              page.size +
              "&sort=" +
              sortData,
            bodyFormData
          )
          .then((response) => {
            resolve({
              pages: response.data.metadata.pagination.totalPages,
              data: response.data.result.data,
              elements: response.data.metadata.pagination.totalCount,
            });
          })
          .catch(({ message }) => {
            reject(message);
          });
      } catch (error) {
        console.log("Error ", error);
      }
    });
  }

  function setColor(value) {
    if (value == "SUBMITTED") {
      return { backgroundColor: "#558B2F", color: "white" };
    } else if (value == "DONE") {
      return { backgroundColor: "#009688", color: "white" };
    } else if (value == "ON-HOLD") {
      return { backgroundColor: "red", color: "white" };
    } else if (value == "CANCELLED") {
      return { backgroundColor: "gray", color: "white" };
    } else if (value == "PROCESSING") {
      return { backgroundColor: "#0E93F0", color: "white" };
    }
  }

  // #558B2F #339FFF #0E93F0

  const columns = [
    {
      Header: "id",
      accessor: "shipmentDbId",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Status" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Chip size="medium" style={setColor(value)} label={value} />;
      },
      csvHeader: "Shipment Status",
      accessor: "shipmentStatus",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Sender Program" />
        </Typography>
      ),
      csvHeader: "Sender Program",
      accessor: "programCode",
      width: 150,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="SHU Ref No" />
        </Typography>
      ),
      csvHeader: "SHU Ref No.",
      accessor: "shuReferenceNumber",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Shipment Name" />
        </Typography>
      ),
      csvHeader: "Shipment Name",
      accessor: "shipmentName",
      width: 150,
    },
    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Shipment Code" />
    //     </Typography>
    //   ),
    //   csvHeader: "Shipment Code",
    //   accessor: "shipmentCode",
    //   width: 150,
    // },
    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Tracking Status" />
    //     </Typography>
    //   ),
    //   csvHeader: "Tracking Status",
    //   accessor: "shipmentTrackingStatus",
    //   width: 150,
    // },
    // {
    //   Header: (
    //     <Typography variant="h8" color="primary">
    //       <FormattedMessage
    //         id="none"
    //         defaultMessage="Shipment Transaction Type"
    //       />
    //     </Typography>
    //   ),
    //   csvHeader: "Shipment Transaction Type",
    //   accessor: "shipmentTransactionType",
    //   width: 150,
    // },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Shipment Type" />
        </Typography>
      ),
      csvHeader: "Shipment Type",
      accessor: "shipmentType",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Material Type" />
        </Typography>
      ),
      csvHeader: "Material Type",
      accessor: "materialType",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Purpose" />
        </Typography>
      ),
      csvHeader: "Shipment Purpose",
      accessor: "shipmentPurpose",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Total Items" />
        </Typography>
      ),
      csvHeader: "Total Items",
      accessor: "totalEntries",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Total Weight" />
        </Typography>
      ),
      csvHeader: "Total Package Weight",
      accessor: "totalItemWeight",
      width: 200,
    },

    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Total Count" />
        </Typography>
      ),
      csvHeader: "Total Package Count",
      accessor: "totalPackageCount",
      width: 150,
    },

    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Requestor Name" />
        </Typography>
      ),
      csvHeader: "Requestor Name",
      accessor: "requestorName",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Requestor Email" />
        </Typography>
      ),
      csvHeader: "Requestor Name",
      accessor: "requestorEmail",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Requestor Address" />
        </Typography>
      ),
      csvHeader: "Requestor Address",
      accessor: "requestorAddress",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Requestor Institution" />
        </Typography>
      ),
      csvHeader: "Requestor Institution",
      accessor: "requestorInstitution",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Requestor Country" />
        </Typography>
      ),
      csvHeader: "Requestor Country",
      accessor: "requestorCountry",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Sender Name" />
        </Typography>
      ),
      csvHeader: "Sender Name",
      accessor: "senderName",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Sender Email" />
        </Typography>
      ),
      csvHeader: "Sender Email",
      accessor: "senderEmail",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Sender Address" />
        </Typography>
      ),
      csvHeader: "Sender Address",
      accessor: "senderAddress",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Sender Institution" />
        </Typography>
      ),
      csvHeader: "Sender Institution",
      accessor: "senderInstitution",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Sender Country" />
        </Typography>
      ),
      csvHeader: "Sender Country",
      accessor: "senderCountry",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Recipient Name" />
        </Typography>
      ),
      csvHeader: "Recipient Name",
      accessor: "recipientName",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Recipient Email" />
        </Typography>
      ),
      csvHeader: "Recipient Email",
      accessor: "recipientEmail",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Recipient Address" />
        </Typography>
      ),
      csvHeader: "Recipient Address",
      accessor: "recipientAddress",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Recipient Institution" />
        </Typography>
      ),
      csvHeader: "Recipient Institution",
      accessor: "recipientInstitution",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Recipient Country" />
        </Typography>
      ),
      csvHeader: "Recipient Country",
      accessor: "recipientCountry",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Doc Signed Date" />
        </Typography>
      ),
      csvHeader: "Document Signed Date",
      accessor: "documentSignedDate",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Doc Generated Date" />
        </Typography>
      ),
      csvHeader: "Document Generated Date",
      accessor: "documentGeneratedDate",
      width: 200,
    },

    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Airbill" />
        </Typography>
      ),
      csvHeader: "Airbill Number",
      accessor: "airwayBillNumber",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Shipped Date" />
        </Typography>
      ),
      csvHeader: "Shipped Date",
      accessor: "shippedDate",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Processed By" />
        </Typography>
      ),
      csvHeader: "Processed By",
      accessor: "processorName",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Shipment Remarks" />
        </Typography>
      ),
      csvHeader: "Shipment Remarks",
      accessor: "shipmentRemarks",
      width: 300,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Dispatch Remarks" />
        </Typography>
      ),
      csvHeader: "Dispatch Remarks",
      accessor: "dispatchRemarks",
      width: 300,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage
            id="none"
            defaultMessage="Acknowledgement Remarks"
          />
        </Typography>
      ),
      csvHeader: "Acknowledgement Remarks",
      accessor: "acknowledgementRemarks",
      width: 300,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Notes" />
        </Typography>
      ),
      csvHeader: "Notes",
      accessor: "notes",
      hidden: true,
    },
  ];

  const toolbarActions = (selection, refresh) => {
    return (
      <Box display="flex" justifyContent="center">
        <Box>{<CreateButton refresh={refresh} isFormEditable={true} />}</Box>
        <Box>&nbsp;</Box>
        <Box>&nbsp;</Box>
        <Box>
          {
            <PrintButton
              refresh={refresh}
              selection={selection}
              dataItem={data}
            />
          }
        </Box>
        <Box>&nbsp;</Box>
        {/* <Box>{<RefreshButton refresh={refresh} />}</Box> */}
      </Box>
    );
  };

  const rowActions = (rowData, refresh) => {
    let isSubmit = false;
    let isEdit = false;
    let isProcess = false;
    let isDelete = false;
    let isView = true;
    let isMessage = true;

    if (rowData.shipmentStatus === "Draft") {
      isSubmit = true;
      isEdit = true;
      isDelete = true;
    }

    if (rowData.shipmentStatus === "CREATED") {
      isSubmit = true;
      isEdit = true;
      isDelete = true;
    }

    if (
      rowData.shipmentStatus === "SUBMITTED" ||
      rowData.shipmentStatus === "CANCELLED"
    ) {
      isEdit = false;
      isSubmit = false;
    }

    if (
      (rowData.shipmentStatus === "PROCESSING" ||
        rowData.shipmentStatus === "SUBMITTED") &&
      isSHU
    ) {
      isEdit = false;
      isProcess = true;
    }

    if (
      rowData.shipmentStatus === "DONE" ||
      rowData.shipmentStatus === "CANCELLED"
    ) {
      isEdit = false;
      isProcess = false;
    }

    if (isSHU) {
      isDelete = false;
      isEdit = false;
    }

    if (rowData.shipmentStatus === "ON-HOLD" && !isSHU) {
      isEdit = true;
      isSubmit = true;
    }

    if (rowData.shipmentStatus == "DONE" && rowData.notes === null) {
      isMessage = false;
    }

    return (
      <Box display="flex" flexDirection="row">
        {isMessage && (
          <Box>
            <RowButtonMessage rowData={rowData} refresh={refresh} />
          </Box>
        )}
        <Box>
          <RowButtonView
            rowData={rowData}
            refresh={refresh}
            isFormEditable={false}
          />
        </Box>
        {isEdit && (
          <Box>
            <RowButtonEdit
              rowData={rowData}
              refresh={refresh}
              isFormEditable={true}
            />
          </Box>
        )}
        {isDelete && (
          <Box>
            <RowButtonDelete
              shipmentDbId={rowData.shipmentDbId}
              refresh={refresh}
            />
          </Box>
        )}
        {isSubmit && (
          <Box>
            <RowButtonSubmit
              rowData={rowData}
              refresh={refresh}
              shipmentDbId={rowData.shipmentDbId}
            />
          </Box>
        )}

        {isProcess && (
          <Box>
            <ProcessButton
              rowData={rowData}
              refresh={refresh}
              isFormEditable={true}
            />
          </Box>
        )}
      </Box>
    );
  };
  if (isLoadingPersonComplete) {
    return (
      /* 
        @prop data-testid: Id to use inside shipmentbrowser.test.js file.
        */
      <div>
        <EbsGrid
          id="ShipmentBrowserId"
          toolbar={true}
          columns={columns}
          title={
            <Typography variant="h5" color="primary">
              <FormattedMessage id="none" defaultMessage="Shipment List" />
            </Typography>
          }
          csvfilename="shipment_list"
          fetch={fetch}
          select="single"
          toolbaractions={toolbarActions}
          rowactions={rowActions}
          height="65vh"
          raWidth={200}
          globalFilter={false}
        />
      </div>
    );
  } else {
    return <LinearProgress />;
  }
});
// Type and required properties
ShipmentBrowserOrganism.propTypes = {};
// Default properties

export default ShipmentBrowserOrganism;
