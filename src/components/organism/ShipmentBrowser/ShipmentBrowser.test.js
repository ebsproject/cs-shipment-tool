  import ShipmentBrowser from './ShipmentBrowser';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentBrowser is in the DOM', () => {
  render(<ShipmentBrowser></ShipmentBrowser>)
  expect(screen.getByTestId('ShipmentBrowserTestId')).toBeInTheDocument();
})
