  import TestEbsGrid from './TestEbsGrid';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('TestEbsGrid is in the DOM', () => {
  render(<TestEbsGrid></TestEbsGrid>)
  expect(screen.getByTestId('TestEbsGridTestId')).toBeInTheDocument();
})
