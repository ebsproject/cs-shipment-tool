import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import { Core } from "@ebs/styleguide";
const { Box, Typography } = Core;
//import EbsGrid Row Button
import { EbsGrid } from "@ebs/components";
import TestEbsGridRowButton from "components/atom/TestEbsGridRowButton";
import { useDispatch, useSelector, useStore } from "react-redux";
import { getShipmentsData } from "store/modules/TestReducer";
import TestButton from "components/atom/TestButton";
import TextField from "components/atom/TextField";
import { cbClient } from "utils/axios";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TestEbsGridOrganism = React.forwardRef(({}, ref) => {
  const [data, setData] = useState(null);
  const dispatch = useDispatch();
  const { shipments } = useSelector(({ testReducer }) => testReducer);
  const { getState } = useStore();

  const columns = [
    {
      Header: "id",
      accessor: "id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h8" color="primary">
          <FormattedMessage id="none" defaultMessage="Status" />
        </Typography>
      ),
      csvHeader: "Status",
      accessor: "status",
      width: 250,
    },
  ];

  async function fetch({ page, sort, filters }) {
    return new Promise((resolve, reject) => {
      try {
        cbClient
          .get("shipment")
          .then((response) => {
            resolve({
              pages: 1,
              data: response.data.result.data,
              elements: response.data.result.data.length,
            });
          })
          .catch(({ message }) => {
            reject(message);
          });
      } catch (error) {
        console.log(error);
      }
    });
  }

  const toolbarActions = (selection, refresh) => {
    return (
      <Box display="flex" justifyContent="center">
        <Box>{/* <ToolbarActionAdd refresh={refresh} /> */}</Box>
        <Box>
          {/* <ToolbarActionDelete selection={selection} refresh={refresh} /> */}
        </Box>
        {/* <Box>
          <ToolbarActionPrint selection={selection} refresh={refresh} />
        </Box> */}
      </Box>
    );
  };

  const rowActions = (rowData, refresh) => {
    return (
      <Box>
        <Box>
          {<TestEbsGridRowButton rowData={rowData} refresh={refresh} />}
        </Box>
      </Box>
    );
  };

  return (
    /* 
     @prop data-testid: Id to use inside testebsgrid.test.js file.
     */
    <div>
      <TestButton />
      <EbsGrid
        id="TestEbsGridId"
        toolbar={true}
        columns={columns}
        title={
          <Typography variant="h8" color="primary">
            <FormattedMessage id="none" defaultMessage="Shipment Transaction" />
          </Typography>
        }
        fetch={fetch}
        // toolbaractions={toolbarActions}
        select="multi"
        rowactions={rowActions}
        height="65vh"
      />
    </div>
  );
});
// Type and required properties
TestEbsGridOrganism.propTypes = {};
// Default properties
TestEbsGridOrganism.defaultProps = {};

export default TestEbsGridOrganism;
