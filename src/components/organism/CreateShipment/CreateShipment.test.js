  import CreateShipment from './CreateShipment';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('CreateShipment is in the DOM', () => {
  render(<CreateShipment></CreateShipment>)
  expect(screen.getByTestId('CreateShipmentTestId')).toBeInTheDocument();
})
