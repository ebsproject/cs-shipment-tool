  import ProcessShipment from './ProcessShipment';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ProcessShipment is in the DOM', () => {
  render(<ProcessShipment></ProcessShipment>)
  expect(screen.getByTestId('ProcessShipmentTestId')).toBeInTheDocument();
})
