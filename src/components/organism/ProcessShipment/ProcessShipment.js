import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import { Core, Styles } from "@ebs/styleguide";
const { Typography, Box, AppBar, Tabs, Tab } = Core;
const { useTheme } = Styles;
import BasicInfoForm from "components/molecules/BasicInfoForm";
import ShipmentItemBrowser from "components/molecules/ShipmentItemBrowser";
import ProcessForm from "components/molecules/ProcessForm";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`horizontal-tabpanel-${index}`}
      aria-labelledby={`-tab-${index}`}
      {...other}
      style={{ width: "100%", overflow: "auto" }}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}


//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ProcessShipmentOrganism = React.forwardRef(({}, ref) => {
  const theme = useTheme();

  const classes = {
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      display: "flex",
      maxHeight: "85vh",
    },
    tabs: {
      borderRight: `1px solid ${theme.palette.divider}`,
    },
  };
  
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    /* 
     @prop data-testid: Id to use inside processshipment.test.js file.
     */

    <div>
      <ProcessForm />
      <Tabs
        orientation="horizontal"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Create Shipment"
        className={classes.tabs}
        // variant="fullWidth"
      >
        <Tab label="Basic Info" {...a11yProps(0)} />
        <Tab label="Entries" {...a11yProps(1)} />
        <Tab label="Documents" {...a11yProps(2)} />
      </Tabs>

      <TabPanel value={value} index={0}>
        <BasicInfoForm />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <ShipmentItemBrowser />
      </TabPanel>
      <TabPanel value={value} index={2}>
        {/* <BasicInfoForm /> */}
      </TabPanel>
    </div>
  );
});
// Type and required properties
ProcessShipmentOrganism.propTypes = {};
// Default properties
ProcessShipmentOrganism.defaultProps = {};

export default ProcessShipmentOrganism;
