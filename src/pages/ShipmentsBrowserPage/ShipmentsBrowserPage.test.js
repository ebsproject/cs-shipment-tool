  import ShipmentsBrowserPage from './ShipmentsBrowserPage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentsBrowserPage is in the DOM', () => {
  render(<ShipmentsBrowserPage></ShipmentsBrowserPage>)
  expect(screen.getByTestId('ShipmentsBrowserPageTestId')).toBeInTheDocument();
})
