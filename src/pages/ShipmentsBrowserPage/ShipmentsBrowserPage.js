import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
// import { Typography } from "@material-ui/core";
import ShipmentBrowser from "components/organism/ShipmentBrowser";
import ShipmentItemBrowser from "components/molecules/ShipmentItemBrowser";
import MlsAncestorBrowser from "components/molecules/MlsAncestorsBrowser";

/*
  @param { }: page props,
*/
export default function ShipmentsBrowserPageView({}) {
  /*
  @prop data-testid: Id to use inside shipmentsbrowserpage.test.js file.
 */
  return (
    <div data-testid={"ShipmentsBrowserPageTestId"}>
      <ShipmentBrowser />
    </div>
  );
}
// Type and required properties
ShipmentsBrowserPageView.propTypes = {};

