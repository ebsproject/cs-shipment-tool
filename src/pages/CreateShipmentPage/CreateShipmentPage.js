import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Styles } from "@ebs/styleguide";
const { Typography } = Core;

import CreateShipment from "components/organism/CreateShipment";
import { useSelector, useDispatch, useStore } from "react-redux";


/*
  @param { }: page props,
*/
export default function CreateShipmentPageView({}) {
  /*
  @prop data-testid: Id to use inside createshipmentpage.test.js file.
 */

  const dispatch = useDispatch();
  const { getState } = useStore();
  const [PageLabel, setPageLabel] = useState("Create Shipment");

  const { mode, isFormEditable } = useSelector(
    ({ shipmentsReducer }) => shipmentsReducer
  );
  const { person, personProgram, isSHU } = useSelector(
    ({ personReducer }) => personReducer
  );

  useEffect(() => {
    if (mode == "put") {
      if (isSHU) {
        setPageLabel("Processing Shipment");
      } else {
        setPageLabel("Modify Shipment");
      }
    } else if (mode == "post") {
      setPageLabel("Create Shipment");
    } else if (mode == "view") {
      setPageLabel("View Shipment");
    }
  }, []);

  return (
    <div data-testid={"CreateShipmentPageTestId"}>
      {/* <Typography>
        <span className={`${classes.mainLabel}`}>
          <FormattedMessage id={"some.id"} defaultMessage={"Create Shipment"} />
        </span>
      </Typography> */}
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage={PageLabel} />
      </Typography>
      <CreateShipment />
    </div>
  );
}
// Type and required properties
CreateShipmentPageView.propTypes = {};
// Default properties
CreateShipmentPageView.defaultProps = {};
