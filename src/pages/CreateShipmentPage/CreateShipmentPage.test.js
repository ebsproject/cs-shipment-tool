  import CreateShipmentPage from './CreateShipmentPage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('CreateShipmentPage is in the DOM', () => {
  render(<CreateShipmentPage></CreateShipmentPage>)
  expect(screen.getByTestId('CreateShipmentPageTestId')).toBeInTheDocument();
})
