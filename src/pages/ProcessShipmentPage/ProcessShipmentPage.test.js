  import ProcessShipmentPage from './ProcessShipmentPage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ProcessShipmentPage is in the DOM', () => {
  render(<ProcessShipmentPage></ProcessShipmentPage>)
  expect(screen.getByTestId('ProcessShipmentPageTestId')).toBeInTheDocument();
})
