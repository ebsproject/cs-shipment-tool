import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Styles } from "@ebs/styleguide";
const { Typography } = Core;
const { makeStyles } = Styles;
import ProcessShipment from "components/organism/ProcessShipment";

const useStyles = makeStyles((theme) => ({
  bold: {
    fontWeight: 600,
  },
  inlineStyle: {
    display: "inline-block",
  },
  labelStyle: {
    fontWeight: 600,
    marginRight: "30px",
  },
  mainLabel: {
    marginRight: "5px",
    fontWeight: 900,
    color: theme.palette.primary.dark,
  },
}));

/*
  @param { }: page props,
*/
export default function ProcessShipmentPageView({}) {
  const classes = useStyles();
  /*
  @prop data-testid: Id to use inside processshipmentpage.test.js file.
 */
  return (
    <div data-testid={"ProcessShipmentPageTestId"}>
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Process Shipment" />
      </Typography>
      <ProcessShipment />
    </div>
  );
}
// Type and required properties
ProcessShipmentPageView.propTypes = {};
// Default properties
ProcessShipmentPageView.defaultProps = {};
