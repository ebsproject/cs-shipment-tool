  import TestEbsGridPage from './TestEbsGridPage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('TestEbsGridPage is in the DOM', () => {
  render(<TestEbsGridPage></TestEbsGridPage>)
  expect(screen.getByTestId('TestEbsGridPageTestId')).toBeInTheDocument();
})
