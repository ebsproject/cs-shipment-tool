import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Typography } from "@material-ui/core";
import TestEbsGrid from "components/organism/TestEbsGrid";

/*
  @param { }: page props,
*/
export default function TestEbsGridPageView({}) {
  /*
  @prop data-testid: Id to use inside testebsgridpage.test.js file.
 */
  return (
    <div data-testid={"TestEbsGridPageTestId"}>
      {/* <Typography variant="h5">
        <FormattedMessage id="none" defaultMessage="Test EbsGrid" />
      </Typography> */}
      <TestEbsGrid />
    </div>
  );
}
// Type and required properties
TestEbsGridPageView.propTypes = {};
// Default properties
TestEbsGridPageView.defaultProps = {};
