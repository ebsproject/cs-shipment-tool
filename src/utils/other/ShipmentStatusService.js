class ShipmentStatusService {
  getShipmentStatus() {
    let recipientType = [];
    return (recipientType = [
      { id: "1", label: "CREATED" },
      { id: "2", label: "SUBMITTED" },
      { id: "3", label: "PROCESSING" },
      { id: "4", label: "DONE" },
      { id: "5", label: "ON-HOLD" },
      { id: "6", label: "CANCELLED" },
    ]);
  }
}

export default new ShipmentStatusService();
