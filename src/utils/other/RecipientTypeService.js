class RecipientTypeService {
  getRecipientType() {
    let recipientType = [];
    return (recipientType = [
      { id: "Department", label: "Department" },
      { id: "Gene Bank", label: "Gene Bank" },
      { id: "Governmental", label: "Governmental" },
      { id: "Individual", label: "Individual" },
      { id: "Institution", label: "Institution" },
      { id: "International Center", label: "International Center" },
      { id: "Int Agriculture Center", label: "Int Agriculture Center" },
      {
        id: "National Agriculture Center",
        label: "National Agriculture Center",
      },
      { id: "National Center", label: "National Center" },
      { id: "Non-Governmental", label: "Non-Governmental" },
      { id: "Private Company", label: "Private Company" },
      { id: "Regional Organization", label: "Regional Organization" },
      { id: "University", label: "University" },
      { id: "Other", label: "Other" },
      { id: "Unknown", label: "Unknown" },
    ]);
  }
}

export default new RecipientTypeService();
