import axios from "axios";
import {
  getTokenId,
  getDomainContext,
  getCoreSystemContext,
} from "@ebs/layout";

const { context, sgContext } = getDomainContext("cb"); //"cs", "sm", "ba", "cb"

const { printoutUri } = getCoreSystemContext();
/**
 * Note: tokens expire. since SM UI does not really have a login system,
 * this token needs to be constantly updated by getting a new token.
 * But when this gets integrated with the Core System, the token
 * will be then handled by its auth system and the localStorage
 * will now have a value.
 */
export const cbClient = axios.create({
  // baseURL: "https://cbapi.local/v3/",
  baseURL: sgContext,
  headers: {
    Accept: "application/json",
  },
});

cbClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem("id_token")}`;
  return config;
});

/**
 * Note: tokens expire. since SM UI does not really have a login system,
 * this token needs to be constantly updated by getting a new token.
 * But when this gets integrated with the Core System, the token
 * will be then handled by its auth system and the localStorage
 * will now have a value.
 */
export const poClient = axios.create({
  // baseURL: "https://ebs-csps.irri.org",
  baseURL: printoutUri,
  headers: {
    Accept: "application/json",
  },
});

poClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem("id_token")}`;
  return config;
});
