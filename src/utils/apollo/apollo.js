import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink,
  from,
} from '@apollo/client';

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_UIAPI_URI_GRAPHQL,
});

const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      ...headers,
      authorization: `Bearer ${localStorage.getItem('id_token')}`,
    },
  }));
  return forward(operation);
});

export const client = new ApolloClient({
  link: from([authLink, httpLink]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});
