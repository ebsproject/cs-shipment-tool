import React from "react";
import ReactDOM from "react-dom";
import singleSpaReact from "single-spa-react";
import _App from "./root.component";

const lifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: _App,
  errorBoundary(err, info, props) {
    // Customize the root error boundary for your microfrontend here.
    return null;
  },
});

export const App = _App;

export const { bootstrap, mount, unmount } = lifecycles;

// import React from "react";
// import ReactDOM from "react-dom";
// import singleSpaReact from "single-spa-react";
// import Root from "./root.component";
// import "./styles/index.css";

// const lifecycles = singleSpaReact({
//   React,
//   ReactDOM,
//   rootComponent: Root,
//   errorBoundary(err, info, props) {
//     // Customize the root error boundary for your microfrontend here.
//     return null;
//   },
// });

// export {
//   getDomainContext,
//   getContext,
//   getAuthState,
//   getTokenId,
// } from "utils/other/CrossMFE/CrossContext";

// export const { bootstrap, mount, unmount } = lifecycles;
