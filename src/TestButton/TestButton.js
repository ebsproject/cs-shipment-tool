import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Button, Typography } = Core;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const TestButtonAtom = React.forwardRef(({}, ref) => {
  return (
    /* 
     @prop data-testid: Id to use inside TestButton.test.js file.
     */
    <Button
      data-testid={"TestButtonTestId"}
      ref={ref}
      variant="contained"
      className="w-224 bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
      aria-label="Login"
    >
      <Typography variant="button">
        <FormattedMessage id="none" defaultMessage="My button" />
      </Typography>
    </Button>
  );
});
// Type and required properties
TestButtonAtom.propTypes = {};
// Default properties
TestButtonAtom.defaultProps = {};

export default TestButtonAtom;
