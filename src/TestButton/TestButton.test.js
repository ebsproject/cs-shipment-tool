  import TestButton from './TestButton';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('TestButton is in the DOM', () => {
  render(<TestButton></TestButton>)
  expect(screen.getByTestId('TestButtonTestId')).toBeInTheDocument();
})
