import { combineReducers } from "redux";
import shipmentsReducer from "store/modules/ShipmentsReducer";
import personReducer from "store/modules/PersonReducer";
import message from "store/modules/message";
import testReducer from "store/modules/TestReducer";

export default combineReducers({
  shipmentsReducer,
  personReducer,
  testReducer,
  message,
});
