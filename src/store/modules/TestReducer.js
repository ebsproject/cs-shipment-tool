import { testClient } from "utils/axios";

/*
 Initial state and properties
 */
export const initialState = {
  testShipments: null,
  testLoading: false,
};
/*
 Action types
 */

export const SHIPMENTS_FETCH = "[TEST] SHIPMENTS_FETCH";
export const GET_SHIPMENTS = "[TEST] GET_SHIPMENT";
export const GET_SHIPMENTS_SUCCESS = "[TEST] GET_SHIPMENT_SUCCESS";
export const SELECTED_SHIPMENT = "[TEST] SELECTED_SHIPMENT";
const POST_SHIPMENT_NAME = "[TEST] POST_SHIPMENT_NAME";

/*
 Arrow function for change state
 */

const setShipmentName = () => ({
  type: POST_SHIPMENT_NAME,
});

export const shipmentFetch = () => ({
  type: SHIPMENTS_FETCH,
});

export const getShipments = (shipments) => ({
  type: GET_SHIPMENTS,
  payload: shipments,
});

export const getShipmentsSuccess = (shipments) => ({
  type: GET_SHIPMENT_SUCCESS,
  payload: shipments,
});

export const selectedShipment = (shipment) => ({
  type: SELECTED_SHIPMENT,
  payload: shipment,
});

/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case POST_SHIPMENT_NAME:
      return {
        ...state,
        testLoading: true,
      };
    case SHIPMENTS_FETCH:
      return {
        ...state,
        testLoading: true,
      };

    case GET_SHIPMENTS:
      return {
        ...state,
        testLoading: false,
        testShipments: payload,
      };
    case GET_SHIPMENTS_SUCCESS:
      return {
        ...state,
        testLoading: false,
        testShipments: payload,
      };
    case SELECTED_SHIPMENT:
      return {
        ...state,
        testShipments: payload,
      };
    default:
      return state;
  }
}

/*Async function calling to api and store the new state data */

export const postShipmentName = (newName) => async (dispatch, getState) => {
  //
};

export const getShipmentsData = () => async (dispatch, getState) => {
  dispatch(shipmentFetch());

  // try {
  //   await testClient
  //     .get("products")
  //     .then((response) => {
  //       console.log("Shipment Response ", response.data);
  //       dispatch(getShipments(response.data));
  //     })
  //     .catch(({ message }) => {
  //       console.log("error", err);
  //     });
  // } catch (error) {
  //   console.log(error);
  // }

  // return new Promise((resolve, reject) => {
  //   testClient
  //     .get("products")
  //     .then((response) => {
  //       console.log("Shipment Response ", response.data);
  //       resolve(dispatch(getShipments(response.data)));
  //     })
  //     .catch(({ message }) => {
  //       console.log("error", err);
  //     });
  // });

  // localClient
  //   .get("getShipment")
  //   .then((response) => {
  //     console.log("Shipment Response ", response.data);
  //     dispatch(getShipments(response.data));
  //   })
  //   .catch(({ message }) => {
  //     console.log("error", err);
  //   });
};
