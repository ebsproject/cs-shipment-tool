import { useSelector } from "react-redux";
/*
 Initial state and properties
 */
export const initialState = {
  person: [],
  personProgram: [],
  isSHU: false,
  currentUserProgramId: null,
  userProgramList: [],
};
/*
 Action types
 */
export const SET_PERSON = "[PERSON] SET_PERSON]";
export const SET_PERSON_PROGRAM = "[PERSON] SET_PERSON_PROGRAM";
export const GET_PERSON = "[PERSON] GET_PERSON]";
export const SET_IS_SHU = "[PERSON] SET_IS_SHU";
export const SET_CURRENT_USER_PROGRAM_ID =
  "[PERSON] SET_CURRENT_USER_PROGRAM_ID";
export const SET_PERSON_PROGRAM_LIST = "[PERSON] SET_USER_PROGRAM_LIST";

/*
 Arrow function for change state
 */
export const setPerson = (payload) => ({
  type: SET_PERSON,
  payload: payload,
});

export const setPersonProgram = (payload) => ({
  type: SET_PERSON_PROGRAM,
  payload: payload,
});

export const setPersonProgramList = (payload) => ({
  type: SET_PERSON_PROGRAM_LIST,
  payload: payload,
});

export const setCurrentUserProgramId = (payload) => ({
  type: SET_CURRENT_USER_PROGRAM_ID,
  payload: payload,
});

export const getPerson = () => ({
  type: GET_PERSON,
});
export const setIsSHU = (isShu) => ({
  type: SET_IS_SHU,
  payload: isShu,
});

/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_PERSON:
      return {
        ...state,
        person: payload,
      };
    case GET_PERSON:
      return {
        ...state,
        person: payload,
      };
    case SET_PERSON_PROGRAM:
      return {
        ...state,
        personProgram: payload,
      };
    case SET_IS_SHU:
      return {
        ...state,
        isSHU: payload,
      };
    case SET_CURRENT_USER_PROGRAM_ID:
      return {
        ...state,
        currentUserProgramId: payload,
      };
    case SET_PERSON_PROGRAM_LIST:
      return {
        ...state,
        personProgramList: payload,
      };

    default:
      return state;
  }
}
