import { cbClient, poClient } from "utils/axios";
import { showMessage } from "store/modules/message";

/*
 Initial state and properties
 */
export const initialState = {
  shipmentDbId: 0,
  mode: "post",
  shipments: [],
  shipment: null,
  loading: false,
  transactionStatus: null,
  isFileUploadSuccess: false,
  shipmentProgram: null,
};
/*
 Action types for Shipments Browser
 */

const ERROR = "[SHIPMENT] ERROR";
export const SET_SHIPMENT_DB_ID = "[SHIPMENT] SET_SHIPMENT_DB_ID";
export const SET_MODE = "[SHIPMENT] SET_MODE";
export const SET_TRANSACTION_STATUS = "[SHIPMENT] SET_TRANSACTION_STATUS";
export const SET_SHIPMENT = "[SHIPMENT] SET_SHIPMENT";
export const GET_SHIPMENT = "[SHIPMENT] GET_SHIPMENT";
export const CREATE_SHIPMENT_START = "[SHIPMENT] CREATE_SHIPMENT_START";
export const CREATE_SHIPMENT_SUCCESS = "[SHIPMENT] CREATE_SHIPMENT_SUCCESS";
export const UPDATE_SHIPMENT_START = "[SHIPMENT] UPDATE_SHIPMENT_START";
export const UPDATE_SHIPMENT_SUCCESS = "[SHIPMENT] UPDATE_SHIPMENT_SUCCESS";
export const SET_SHIPMENT_FILEUPLOAD_START =
  "[SHIPMENT] SET_SHIPMENT_FILEUPLOAD_START";
export const SET_SHIPMENT_FILEUPLOAD_SUCCESS =
  "[SHIPMENT] SET_SHIPMENT_FILEUPLOAD_SUCCESS";
export const SET_FORM_EDITABLE = "[SHIPMENT] SET_FORM_EDITABLE";
export const SET_SHIPMENT_PROGRAM = "[SHIPMENT] SET_SHIPMENT_PROGRAM";

/*
 Action types for Shipment Record
 */

/*
 Arrow function for change state
 */

export const setShipmentProgram = (shipmentProgram) => ({
  type: SET_SHIPMENT_PROGRAM,
  payload: shipmentProgram,
});

export const setShipmentDbId = (shipmentDbId) => ({
  type: SET_SHIPMENT_DB_ID,
  payload: shipmentDbId,
});

export const setMode = (mode) => ({
  type: SET_MODE,
  payload: mode,
});

export const setFormEditable = (isFormEditable) => ({
  type: SET_FORM_EDITABLE,
  payload: isFormEditable,
});

export const setShipment = (shipment) => ({
  type: SET_SHIPMENT,
  payload: shipment,
});

export const setTransactionStatus = (status) => ({
  type: SET_TRANSACTION_STATUS,
  payload: status,
});

export const getShipment = () => ({
  type: GET_SHIPMENT,
});

const createShipmentStart = () => ({
  type: CREATE_SHIPMENT_START,
});

const createShipmentSuccess = () => ({
  type: CREATE_SHIPMENT_SUCCESS,
});

const updateShipmentStart = () => ({
  type: UPDATE_SHIPMENT_START,
});

const updateShipmentSuccess = () => ({
  type: UPDATE_SHIPMENT_SUCCESS,
});

const shipmentFailure = (message) => ({
  type: ERROR,
  payload: message,
});

export const setShipmentFileUploadStart = () => ({
  type: SET_SHIPMENT_FILEUPLOAD_START,
});
export const setShipmentFileUploadSuccess = () => ({
  type: SET_SHIPMENT_FILEUPLOAD_SUCCESS,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_SHIPMENT_DB_ID:
      return {
        ...state,
        shipmentDbId: payload,
      };
    case SET_MODE:
      return {
        ...state,
        mode: payload,
      };
    case SET_SHIPMENT:
      return {
        ...state,
        shipment: payload,
        loading: true,
      };
    case SET_TRANSACTION_STATUS:
      return {
        ...state,
        transactionStatus: payload,
      };
    case GET_SHIPMENT:
      return {
        ...state,
        loading: false,
        shipment: payload,
      };
    case CREATE_SHIPMENT_START:
      return {
        ...state,
        loading: true,
      };
    case CREATE_SHIPMENT_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
      };
    case UPDATE_SHIPMENT_START:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_SHIPMENT_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
      };
    case SET_SHIPMENT_FILEUPLOAD_START:
      return {
        ...state,
        loading: true,
      };
    case SET_SHIPMENT_FILEUPLOAD_SUCCESS:
      return {
        ...state,
        isFileUploadSuccess: true,
        loading: false,
      };
    case SET_FORM_EDITABLE:
      return {
        ...state,
        isFormEditable: payload,
      };
    case SET_SHIPMENT_PROGRAM:
      return {
        ...state,
        shipmentProgram: payload,
      };

    default:
      return state;
  }
}

export const findShipment = (shipmentDbId) => async (dispatch, getState) => {
  try {
    const { errors, data } = await cbClient
      .get(`/shipments/${shipmentDbId}`)
      .then(function (response) {
        dispatch(setShipment(response.data.result.data[0]));
      })
      .catch(function (error) {
        console.log(error);
      });
  } catch ({ message }) {
    // dispatch(shipmentFailure(message));
    // dispatch(
    //   showMessage({
    //     message: message,
    //     variant: "error",
    //     anchorOrigin: {
    //       vertical: "top",
    //       horizontal: "right",
    //     },
    //   })
    // );
  } finally {
    // dispatch(cleanContactSuccess());
  }
};

export const createShipment = (shipmentInput) => async (dispatch, getState) => {
  dispatch(createShipmentStart());
  console.log("Oncreate ", shipmentInput.requestorInstitution);
  try {
    const { errors, data } = await cbClient
      .post("/shipments", {
        records: [
          {
            programDbId: shipmentInput.programDbId,
            shipmentCode: shipmentInput.shipmentCode,
            shipmentName: shipmentInput.shipmentName,
            shuReferenceNumber: shipmentInput.shuReferenceNumber,
            shipmentStatus: "CREATED",
            shipmentTrackingStatus: "",
            shipmentItemStatus: "passed",
            shipmentTransactionType: "",
            shipmentType: shipmentInput.shipmentType,
            shipmentPurpose: shipmentInput.shipmentPurpose,
            entityId: 1,
            materialType: shipmentInput.materialType,
            materialSubtype: "",
            totalItemCount: 0,
            totalPackageCount: 0,
            totalPackageWeight: 0,
            packageUnit: "g",
            // documentGeneratedDate: "2022-11-28",
            // documentSignedDate: "2022-11-28",
            // authorizedSignatory: "",
            // shippedDate: "2022-11-28",
            // airwayBillNumber: "",
            // receivedDate: "2022-11-28",
            processorName: "",
            processorId: 1,
            requestorName: shipmentInput.requestorName,
            requestorEmail: shipmentInput.requestorEmail,
            requestorAddress: shipmentInput.requestorAddress,
            requestorInstitution: shipmentInput.requestorInstitution,
            requestorCountry: shipmentInput.requestorCountry,
            senderFacility: "",
            senderName: shipmentInput.senderName,
            senderEmail: shipmentInput.senderEmail,
            senderAddress: shipmentInput.senderAddress,
            senderInstitution: shipmentInput.senderInstitution,
            senderCountry: shipmentInput.senderCountry,
            recipientType: shipmentInput.recipientType,
            recipientName: shipmentInput.recipientName,
            recipientEmail: shipmentInput.recipientEmail,
            recipientAddress: shipmentInput.recipientAddress,
            recipientInstitution: shipmentInput.recipientInstitution,
            recipientFacility: "",
            recipientCountry: shipmentInput.recipientCountry,
            remarks: shipmentInput.remarks,
            creatorId: shipmentInput.creatorId,
            modifierId: shipmentInput.modifierId,
          },
        ],
      })
      .then(function (response) {
        // console.log("Successfully Created Shipment");
        dispatch(createShipmentSuccess());
        dispatch(setShipmentDbId(response.data.result.data[0].shipmentDbId));
        dispatch(setShipment(shipmentInput));
        dispatch(setMode("put"));
        dispatch(
          showMessage({
            message: "Shipment created successfully",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        return;
      })
      .catch(function (error) {
        dispatch(
          showMessage({
            message: error,
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      });
  } catch ({ error }) {
    // dispatch(shipmentFailure(message));
    // console.log("Message Creating ", error);
    // dispatch(
    //   showMessage({
    //     message: message,
    //     variant: "error",
    //     anchorOrigin: {
    //       vertical: "top",
    //       horizontal: "right",
    //     },
    //   })
    // );
  } finally {
    // dispatch(cleanContactSuccess());
  }
};

export const updateShipment =
  (shipmentInput, shipmentDbId) => async (dispatch, getState) => {
    dispatch(createShipmentStart());

    for (const key in shipmentInput) {
      // console.log(key);
      if (shipmentInput[key] === null) {
        delete shipmentInput[key];
      }
      if (key.includes("Date") && shipmentInput[key] === "") {
        delete shipmentInput[key];
      }
    }

    try {
      const { errors, data } = await cbClient
        .put(`/shipments/${shipmentDbId}`, JSON.stringify(shipmentInput), {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then(function (response) {
          dispatch(setShipmentDbId(response.data.result.data[0].shipmentDbId));
          dispatch(setMode("put"));
          dispatch(setShipment(shipmentInput));
          dispatch(
            showMessage({
              message: "Shipment successfully updated",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          // return;
        })
        .catch(function (error) {
          console.log(error);
        });
    } catch ({ message }) {
      // dispatch(shipmentFailure(message));
      // dispatch(
      //   showMessage({
      //     message: message,
      //     variant: "error",
      //     anchorOrigin: {
      //       vertical: "top",
      //       horizontal: "right",
      //     },
      //   })
      // );
    } finally {
    }
  };

export const createShipmentFileUploadMetadata =
  (fileMetadata) => async (dispatch, getState) => {
    dispatch(setShipmentFileUploadStart());
    var idCreated = 0;
    try {
      const { errors, data } = await cbClient
        .post("/shipment-files", {
          records: [
            {
              shipmentDbId: fileMetadata.shipmentDbId,
              fileName: fileMetadata.fileName,
              fileType: fileMetadata.fileType,
              fileSize: fileMetadata.fileSize,
              filePath: fileMetadata.filePath,
              creatorId: fileMetadata.creatorId,
            },
          ],
        })
        .then(function (response) {
          // console.log(
          //   "Successfully Created Shipment File Upload",
          //   response.data.result.data[0].shipmentFileDbId
          // );
          dispatch(setShipmentFileUploadSuccess());
          idCreated = response.data.result.data[0].shipmentFileDbId;
          // refresh();
        })
        .catch(function (error) {
          // console.log("Error Creating Shipment");
          // dispatch(
          //   showMessage({
          //     message: error,
          //     variant: "error",
          //     anchorOrigin: {
          //       vertical: "top",
          //       horizontal: "right",
          //     },
          //   })
          // );
        });
    } catch ({ message }) {
      // dispatch(shipmentFailure(message));
      // dispatch(
      //   showMessage({
      //     message: message,
      //     variant: "error",
      //     anchorOrigin: {
      //       vertical: "top",
      //       horizontal: "right",
      //     },
      //   })
      // );
    } finally {
      // dispatch(cleanContactSuccess());
    }
    return idCreated;
  };

export const uploadShipmentFile = (input) => async (dispatch, getState) => {
  // dispatch(setShipmentFileUploadStart());
  try {
    const formData = new FormData();
    formData.append("file", input.currentFile, input.newFileName);
    formData.append("program", input.personProgram);

    poClient
      .post(`/api/FileManager/UploadDocument`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then(function (response) {
        // setCurrentFile(null);
        // dispatch(setFileUploadSuccess);
      })
      .catch(function (error) {
        console.log("Error Uploading ", error);
      });
  } catch ({ message }) {
  } finally {
    return "success";
  }
};
