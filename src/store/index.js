
import { configureStore } from '@reduxjs/toolkit';
import { thunk } from 'redux-thunk';

import reducers from './reducers';


const store = configureStore({
    reducer: reducers,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: ['persist/PERSIST', 'persist/REHYDRATE'],
        },
      }).concat(thunk),
    devTools: process.env.NODE_ENV !== 'production',
  });

export default store;