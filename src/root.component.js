import React, { useState } from "react";
import { Provider } from "react-redux";
import { ApolloProvider } from "@apollo/client";
import { BrowserRouter, Route } from "react-router-dom";
import { EBSMaterialUIProvider } from "@ebs/styleguide";
// import { InstanceContext, InstanceProvider } from '@ebs/cs';
import { IntlProvider } from "react-intl";
import store from "./store";
import { client } from "utils/apollo";
import ShipmentsBrowserPage from "pages/ShipmentsBrowserPage";
import CreateShipmentPage from "pages/CreateShipmentPage";
// import ProcessShipmentPage from "pages/ProcessShipmentPage";
import Message from "components/atom/Message";
// Routes
import { BASEPATH } from "./routes";

export default function App() {
  const [locale, setLocale] = useState("en");
  const [messages, setMessages] = useState(null);

  return (
    <EBSMaterialUIProvider>
      <Provider store={store}>
        <ApolloProvider client={client}>
          <IntlProvider locale={locale} messages={messages} defaultLocale="en">
            <Message />
            <BrowserRouter>
              <Route
                path={BASEPATH}
                render={({ match }) => (
                  <>
                    <Route
                      exact
                      path={match.url + "/shipment"}
                      component={ShipmentsBrowserPage}
                    />
                    <Route
                      exact
                      path={match.url + "/shipment-create"}
                      component={CreateShipmentPage}
                    />
                    {/* <Route
                      exact
                      path={match.url + "/shipment-process"}
                      component={ProcessShipmentPage}
                    /> */}
                  </>
                )}
              />
            </BrowserRouter>
          </IntlProvider>
        </ApolloProvider>
      </Provider>
    </EBSMaterialUIProvider>
  );
}
